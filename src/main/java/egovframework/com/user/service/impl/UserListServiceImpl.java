package egovframework.com.user.service.impl;

import egovframework.com.user.service.UserListService;
import egovframework.com.user.service.vo.UserCriteria;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.com.user.service.vo.idCheckVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 시뮬레이션 리스트 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
@Service("userListService")
public class UserListServiceImpl extends EgovAbstractServiceImpl implements UserListService {

	@Resource(name = "userDAO")
	private UserDAO userDAO;
	@Override
	public void registerUser(UserListVO vo) throws Exception {
		 userDAO.registerUser(vo);
	}
	@Override
	public int idCheck(UserCriteria vo) throws Exception {
		return userDAO.idCheck(vo);
	}

	/**
	 *
	 * @param vo
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean userLogin(UserCriteria vo, HttpSession session, HttpServletRequest request) throws Exception {
		boolean result = userDAO.userLogin(vo);
		if(result == true){
//			session.setAttribute("userId",vo.getId());
			EgovSessionCookieUtil.setSessionAttribute(request, "userId", vo.getId());
		}
		return result;
	}

}

