package egovframework.com.user.service;

import egovframework.com.user.service.vo.UserCriteria;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.com.user.service.vo.idCheckVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public interface UserListService {


    public void registerUser(UserListVO vo) throws Exception;

    /**
     *
     * @return 결과를 1,0으로 반환
     * @throws Exception
     */
    public int idCheck(UserCriteria vo) throws Exception;

    /**
     *
     * @param vo
     * @return 아이디, 비밀번호가 일치하면 1로 반환
     * @throws Exception
     */
    public boolean userLogin(UserCriteria vo, HttpSession session, HttpServletRequest request) throws Exception;
}
