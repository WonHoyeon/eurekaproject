package egovframework.com.user.web;


import egovframework.com.dash.web.DexiDashBoardController;
import egovframework.com.user.service.UserListService;
import egovframework.com.user.service.vo.UserCriteria;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.com.utl.cas.service.EgovSessionCookieUtil;
import egovframework.com.utl.excel.JxlsExcelView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.UUID;

/**
 * 시뮬레이션 Controller
 *
 *    수정일         수정자         수정내용
 *    -------        -------     -------------------
 *    2019.03.12	고동현		최초생성
 * @author 고동현
 * @since 2019.03.12
 * @version 1.0
 */
@Controller
public class DexiUserController {

	@Resource(name = "userListService")
	UserListService userListService;


	@Resource(name = "jxlsExcelView")
	private JxlsExcelView jxlsExcelView;

	/**
	 * 로깅 처리
	 */
	private static final Logger log = LoggerFactory.getLogger(DexiDashBoardController.class);

//	▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
//	▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

	/**
	 * @description 회원가입 하기
	 */
	@RequestMapping(value = "user/register.do", method = RequestMethod.POST)
	public ModelAndView registerUser(@RequestBody UserListVO vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("jsonView");
		String one = UUID.randomUUID().toString().replace("-", "");
		vo.setPrivateId(one);

		try {
			userListService.registerUser(vo);

		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return modelAndView;
	}


	/**
	 * @description 아이디 중복
	 */
	@RequestMapping(value = "user/idCheck.do", method = RequestMethod.POST)
	public ModelAndView postIdCheck(@RequestBody UserCriteria vo, HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("jsonView");

		System.out.println(vo.getId() +"test");
		try {
			modelAndView.addObject("checkId", userListService.idCheck(vo));
		} catch (Exception e) {
			log.error(e.getMessage());
		}

		return modelAndView;
	}

	/**
	 * @description 유저 로그임
	 */
	@RequestMapping(value = "user/userLogin.do", method = RequestMethod.POST)
	public ModelAndView userLogin(@RequestBody UserCriteria vo, HttpSession session,  HttpServletRequest request, HttpServletResponse response) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("jsonView");
		boolean result = userListService.userLogin(vo,session,request);
		try {
			if(result){
				String userId = (String) EgovSessionCookieUtil.getSessionAttribute(request,"userId");
				System.out.println(userId+ "test");
				modelAndView.addObject("msg", "성공");
			}else{
				modelAndView.addObject("msg", "실패");
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}
		return modelAndView;
	}
}

