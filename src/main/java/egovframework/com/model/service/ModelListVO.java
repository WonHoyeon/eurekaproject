package egovframework.com.model.service;

public class ModelListVO {
    private String stenc3dInfoId;
    private String stenc3dId;
    private String stenc3dTp;
    private String stenc3dFileNm;
    private int modelScale;
    private int isacty;
    private String insuser;

    public String getStenc3dInfoId() {
        return stenc3dInfoId;
    }

    public void setStenc3dInfoId(String stenc3dInfoId) {
        this.stenc3dInfoId = stenc3dInfoId;
    }

    public String getStenc3dId() {
        return stenc3dId;
    }

    public void setStenc3dId(String stenc3dId) {
        this.stenc3dId = stenc3dId;
    }

    public String getStenc3dTp() {
        return stenc3dTp;
    }

    public void setStenc3dTp(String stenc3dTp) {
        this.stenc3dTp = stenc3dTp;
    }

    public String getStenc3dFileNm() {
        return stenc3dFileNm;
    }

    public void setStenc3dFileNm(String stenc3dFileNm) {
        this.stenc3dFileNm = stenc3dFileNm;
    }

    public int getModelScale() {
        return modelScale;
    }

    public void setModelScale(int modelScale) {
        this.modelScale = modelScale;
    }

    public int getIsacty() {
        return isacty;
    }

    public void setIsacty(int isacty) {
        this.isacty = isacty;
    }

    public String getInsuser() {
        return insuser;
    }

    public void setInsuser(String insuser) {
        this.insuser = insuser;
    }
}
