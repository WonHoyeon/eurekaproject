package egovframework.com.model.service.impl;

import egovframework.com.model.service.*;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 시뮬레이션 리스트 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
@Service("modelListService")
public class ModelListServiceImpl extends EgovAbstractServiceImpl implements ModelListService{

	@Resource(name="modelDAO")
	private ModelDAO modelDAO;


	/**
	 * 시뮬레이션 리스트를 반환한다.
	 * @param criteria
	 * @return
	 * @throws Exception
	 */
	public List<ModelListVO> select3DModelList(ModelListCriteria criteria) throws Exception{
		return modelDAO.select3DModelList(criteria);
	}
}