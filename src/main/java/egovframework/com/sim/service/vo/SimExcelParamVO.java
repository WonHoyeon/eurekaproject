package egovframework.com.sim.service.vo;

import java.util.ArrayList;

public class SimExcelParamVO {
    private String simulationid;
    private String loclid;
    private String sorttype;
    private String sortname;
    private String checkedkeysstring;
    private ArrayList<String> checkedkeys;

    public String getCheckedkeysstring() {
        return checkedkeysstring;
    }

    public void setCheckedkeysstring(String checkedkeysstring) {
        this.checkedkeysstring = checkedkeysstring;
    }


    public String getSorttype() {
        return sorttype;
    }

    public void setSorttype(String sorttype) {
        this.sorttype = sorttype;
    }

    public String getSortname() {
        return sortname;
    }

    public void setSortname(String sortname) {
        this.sortname = sortname;
    }

    public ArrayList<String> getCheckedkeys() {
        return checkedkeys;
    }

    public void setCheckedkeys(ArrayList<String> checkedkeys) {
        this.checkedkeys = checkedkeys;
    }

    public String getLoclid() {
        return loclid;
    }

    public void setLoclid(String loclid) {
        this.loclid = loclid;
    }

    public String getSimulationid() {
        return simulationid;
    }

    public void setSimulationid(String simulationid) {
        this.simulationid = simulationid;
    }

}

