package egovframework.com.sim.service.vo;

public class ProcessModelInfoVO {
    private String stencId;
    private String stenckey;
    private String stencNm;
    private int stencLenx;
    private int stencLeny;
    private int stencLenz;
    private int stencPosx;
    private int stencPosy;
    private int stencPosz;
    private int zIndex;
    private String mapId;
    private int rotAngle;
    private Float modelScale;
    private String stenc3dTp;
    private String stenc3did;
    private String stenc3dinfoid;
    private String stenc3dfile;
    private String fillColor;
    private String lineColor;
    private float opacity;

    public float getOpacity() {
        return opacity;
    }

    public void setOpacity(float opacity) {
        this.opacity = opacity;
    }

    public String getStenckey() {
        return stenckey;
    }

    public void setStenckey(String stenckey) {
        this.stenckey = stenckey;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void setFillColor(String fillColor) {
        this.fillColor = fillColor;
    }

    public String getLineColor() {
        return lineColor;
    }

    public void setLineColor(String lineColor) {
        this.lineColor = lineColor;
    }

    public String getStenc3did() {
        return stenc3did;
    }

    public void setStenc3did(String stenc3did) {
        this.stenc3did = stenc3did;
    }

    public String getStenc3dinfoid() {
        return stenc3dinfoid;
    }

    public void setStenc3dinfoid(String stenc3dinfoid) {
        this.stenc3dinfoid = stenc3dinfoid;
    }

    public Float getModelScale() {
        return modelScale;
    }

    public void setModelScale(Float modelScale) {
        this.modelScale = modelScale;
    }

    public String getStencId() {
        return stencId;
    }

    public void setStencId(String stencId) {
        this.stencId = stencId;
    }

    public String getStencNm() {
        return stencNm;
    }

    public void setStencNm(String stencNm) {
        this.stencNm = stencNm;
    }

    public int getStencLenx() {
        return stencLenx;
    }

    public void setStencLenx(int stencLenx) {
        this.stencLenx = stencLenx;
    }

    public int getStencLeny() {
        return stencLeny;
    }

    public void setStencLeny(int stencLeny) {
        this.stencLeny = stencLeny;
    }

    public int getStencLenz() {
        return stencLenz;
    }

    public void setStencLenz(int stencLenz) {
        this.stencLenz = stencLenz;
    }

    public int getStencPosx() {
        return stencPosx;
    }

    public void setStencPosx(int stencPosx) {
        this.stencPosx = stencPosx;
    }

    public int getStencPosy() {
        return stencPosy;
    }

    public void setStencPosy(int stencPosy) {
        this.stencPosy = stencPosy;
    }

    public int getStencPosz() {
        return stencPosz;
    }

    public void setStencPosz(int stencPosz) {
        this.stencPosz = stencPosz;
    }

    public int getzIndex() {
        return zIndex;
    }

    public void setzIndex(int zIndex) {
        this.zIndex = zIndex;
    }

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public int getRotAngle() {
        return rotAngle;
    }

    public void setRotAngle(int rotAngle) {
        this.rotAngle = rotAngle;
    }

    public String getStenc3dTp() {
        return stenc3dTp;
    }

    public void setStenc3dTp(String stenc3dTp) {
        this.stenc3dTp = stenc3dTp;
    }

    public String getStenc3dfile() { return stenc3dfile; }

    public void setStenc3dfile(String stenc3dfile) { this.stenc3dfile = stenc3dfile; }
}
