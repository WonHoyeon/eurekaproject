package egovframework.com.sim.service.vo;

public class SimulationListVo {

    private String simReqId;
    private String modelNm;
    private String simReqNm;
    private String insDt;
    private String userNm;
    private String simReqStNm;
    private String simReqTpNm;

    public String getSimReqId() {
        return simReqId;
    }

    public void setSimReqId(String simReqId) {
        this.simReqId = simReqId;
    }

    public String getModelNm() {
        return modelNm;
    }

    public void setModelNm(String modelNm) {
        this.modelNm = modelNm;
    }

    public String getSimReqNm() {
        return simReqNm;
    }

    public void setSimReqNm(String simReqNm) {
        this.simReqNm = simReqNm;
    }

    public String getInsDt() {
        return insDt;
    }

    public void setInsDt(String insDt) {
        this.insDt = insDt;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public String getSimReqStNm() {
        return simReqStNm;
    }

    public void setSimReqStNm(String simReqStNm) {
        this.simReqStNm = simReqStNm;
    }

    public String getSimReqTpNm() {
        return simReqTpNm;
    }

    public void setSimReqTpNm(String simReqTpNm) {
        this.simReqTpNm = simReqTpNm;
    }


}
