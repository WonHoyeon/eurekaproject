package egovframework.com.sim.service.vo;

public class FactoryModelAttrVO {
    private String stencId;
    private String stenckey;
    private String stencNm;
    private String attrid;
    private String attrkey;
    private String attrval;
    private String attrtxt;

    public String getStencId() {
        return stencId;
    }

    public void setStencId(String stencId) {
        this.stencId = stencId;
    }

    public String getStenckey() {
        return stenckey;
    }

    public void setStenckey(String stenckey) {
        this.stenckey = stenckey;
    }

    public String getStencNm() {
        return stencNm;
    }

    public void setStencNm(String stencNm) {
        this.stencNm = stencNm;
    }

    public String getAttrid() {
        return attrid;
    }

    public void setAttrid(String attrid) {
        this.attrid = attrid;
    }

    public String getAttrkey() {
        return attrkey;
    }

    public void setAttrkey(String attrkey) {
        this.attrkey = attrkey;
    }

    public String getAttrval() {
        return attrval;
    }

    public void setAttrval(String attrval) {
        this.attrval = attrval;
    }

    public String getAttrtxt() {
        return attrtxt;
    }

    public void setAttrtxt(String attrtxt) {
        this.attrtxt = attrtxt;
    }
}
