package egovframework.com.sim.service.vo;

public class TimeOutListVO {

    private String simReqId;
    private String simLoadingTimeHour;
    private int ngCountForHour;
    private int inputCountForHour;

    public String getSimReqId() {
        return simReqId;
    }

    public void setSimReqId(String simReqId) {
        this.simReqId = simReqId;
    }

    public String getSimLoadingTimeHour() {
        return simLoadingTimeHour;
    }

    public void setSimLoadingTimeHour(String simLoadingTimeHour) {
        this.simLoadingTimeHour = simLoadingTimeHour;
    }

    public int getNgCountForHour() {
        return ngCountForHour;
    }

    public void setNgCountForHour(int ngCountForHour) {
        this.ngCountForHour = ngCountForHour;
    }

    public int getInputCountForHour() {
        return inputCountForHour;
    }

    public void setInputCountForHour(int inputCountForHour) {
        this.inputCountForHour = inputCountForHour;
    }
}
