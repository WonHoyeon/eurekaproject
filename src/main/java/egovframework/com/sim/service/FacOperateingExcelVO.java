package egovframework.com.sim.service;

public class FacOperateingExcelVO {

    private String simReqId;// 시뮬레이션 아이디
    private String processId;// 프로세스 아이디
    private String processNm;// 프로세스 이름
    private int processSeq;// 프로세스 시퀀스
    private String working;
    private String failure;
    private String waiting;
    private String blocking;


    public String getSimReqId() {
        return simReqId;
    }

    public void setSimReqId(String simReqId) {
        this.simReqId = simReqId;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getProcessNm() {
        return processNm;
    }

    public void setProcessNm(String processNm) {
        this.processNm = processNm;
    }

    public int getProcessSeq() {
        return processSeq;
    }

    public void setProcessSeq(int processSeq) {
        this.processSeq = processSeq;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public String getWaiting() {
        return waiting;
    }

    public void setWaiting(String waiting) {
        this.waiting = waiting;
    }

    public String getBlocking() {
        return blocking;
    }

    public void setBlocking(String blocking) {
        this.blocking = blocking;
    }
}