package egovframework.com.sim.service.criteria;

public class ProcessModelFilePatchCriteria {
    private String stenc3did;

    public String getStenc3did() {
        return stenc3did;
    }

    public void setStenc3did(String stenc3did) {
        this.stenc3did = stenc3did;
    }
}
