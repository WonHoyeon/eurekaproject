package egovframework.com.sim.service.criteria;

public class ModelViewCriteria{
    private String modelid;

    public String getModelid() {
        return modelid;
    }

    public void setModelid(String modelid) {
        this.modelid = modelid;
    }

}
