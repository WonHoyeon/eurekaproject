package egovframework.com.sim.service.criteria;

public class SimulationCriteria {

    private String search;
    private String username;
    private String fromdate;
    private String todate;
    private String loclid;
    private String simulationtype;


    public String getSimulationtype() {
        return simulationtype;
    }

    public void setSimulationtype(String simulationtype) {
        this.simulationtype = simulationtype;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getLoclid() {
        return loclid;
    }

    public void setLoclid(String loclid) {
        this.loclid = loclid;
    }
}
