package egovframework.com.sim.service.criteria;

public class UserCriteria {
    private String search;
    private String username;

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
