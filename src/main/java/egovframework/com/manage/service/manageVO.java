package egovframework.com.manage.service;

import java.util.ArrayList;

public class manageVO {

    private int key;
    private int level;
    private String deptId;
    private String localId;
    private String deptNm;
    private String upDeptId;
    private String deptAtNm;
    private String deptAtCd;
    private ArrayList<String> userId;
    private String userLoginId;
    private String deptAtInherYn;
    private String userNm;
    private String deptCd;
    private String children;
    private ArrayList<String> checkedDept;

    public int getKey() { return key; }
    public void setKey(int key) { this.key = key; }

    public String getChildren() { return children; }
    public void setChildren(String children) { this.children = children; }

    public ArrayList<String> getCheckedDept() { return checkedDept; }
    public void setCheckedDept(ArrayList<String> checkedDept) { this.checkedDept = checkedDept; }


    public ArrayList<String> getCheckedCode() {
        return checkedCode;
    }

    public void setCheckedCode(ArrayList<String> checkedCode) {
        this.checkedCode = checkedCode;
    }

    private ArrayList<String> checkedCode;


    public String getDeptAtCd() { return deptAtCd; }

    public void setDeptAtCd(String deptAtCd) { this.deptAtCd = deptAtCd; }

    public String getDeptCd() { return deptCd; }

    public void setDeptCd(String deptCd) { this.deptCd = deptCd; }

    public ArrayList<String> getUserId() {
        return userId;
    }

    public void setUserId(ArrayList<String> userId) {
        this.userId = userId;
    }

    public String getUserLoginId() {
        return userLoginId;
    }

    public void setUserLoginId(String userLoginId) {
        this.userLoginId = userLoginId;
    }

    public String getDeptAtInherYn() {
        return deptAtInherYn;
    }

    public void setDeptAtInherYn(String deptAtInherYn) {
        this.deptAtInherYn = deptAtInherYn;
    }

    public String getUserNm() {
        return userNm;
    }

    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getDeptId() {
        return deptId;
    }

    public void setDeptId(String deptId) {
        this.deptId = deptId;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getDeptNm() {
        return deptNm;
    }

    public void setDeptNm(String deptNm) {
        this.deptNm = deptNm;
    }

    public String getUpDeptId() {
        return upDeptId;
    }

    public void setUpDeptId(String upDeptId) {
        this.upDeptId = upDeptId;
    }

    public String getDeptAtNm() {
        return deptAtNm;
    }

    public void setDeptAtNm(String deptAtNm) {
        this.deptAtNm = deptAtNm;
    }
}
