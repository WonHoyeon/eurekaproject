package egovframework.com.manage.service.impl;

import egovframework.com.code.service.SysCodeVO;
import egovframework.com.manage.service.ManagementListService;
import egovframework.com.manage.service.manageCriteria;
import egovframework.com.manage.service.manageVO;
import egovframework.com.sim.service.criteria.SimulationViewCriteria;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("managementListService")
public class ManagementListServiceImpl extends EgovAbstractServiceImpl implements ManagementListService {

    @Resource(name="managementDAO")
    private ManagementDAO managementDAO;

    @Override
    public List<manageVO> getManageList(SimulationViewCriteria criteria) throws Exception {
        return managementDAO.getManageList(criteria);
    }

    @Override
    public List<UserListVO> getManageUserList() throws Exception {
        return managementDAO.getManageUserList();
    }
    @Override
    public List<SysCodeVO> getUserCodeList() throws Exception {
        return managementDAO.getUserCodeList();
    }
    @Override
    public void addDeptInfo(manageVO vo) throws Exception {
        managementDAO.addDeptInfo1(vo);
        managementDAO.addDeptInfo2(vo);
    }
    @Override
    public void delSysCodeList(manageVO vo) throws Exception {
        managementDAO.delSysCodeList1(vo);
        managementDAO.delSysCodeList2(vo);
    }

    @Override
    public void delManageList(manageVO vo) throws Exception {
        managementDAO.delManageList(vo);
    }
    @Override
    public void DelDepNm(manageVO vo) throws Exception {
        managementDAO.DelDepNm(vo);
    }
    @Override
    public void addDeptNm(manageVO vo) throws Exception {
        managementDAO.addDeptNm(vo);
    }

    @Override
    public List<UserListVO> searchRole(manageCriteria vo) throws Exception {
        return managementDAO.searchRole(vo);
    }

    @Override
    public List<UserListVO> searchDeptList(manageCriteria vo) throws Exception {
        return managementDAO.searchDeptList(vo);
    }

    @Override
    public List<UserListVO> searchDeptMember(manageCriteria vo) throws Exception {
        return managementDAO.searchDeptMember(vo);
    }
    @Override
    public List<UserListVO> getManageOtherUser(manageCriteria vo) throws Exception {
        return managementDAO.getManageOtherUser(vo);
    }
    @Override
    public manageVO checkDeptNm(manageVO vo) throws Exception {
        return managementDAO.checkDeptNm(vo);
    }
}