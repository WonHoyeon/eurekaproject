package egovframework.com.login.service.impl;

import egovframework.com.login.service.LogService;
import egovframework.com.user.service.vo.UserListVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 시뮬레이션 리스트 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
@Service("logService")
public class LoginServiceImpl extends EgovAbstractServiceImpl implements LogService {

	@Resource(name="logDAO")
	private LoginDAO logDAO;


	@Override
	public UserListVO getPw(UserListVO vo) throws Exception {
		return  logDAO.getPw(vo);
	}

	@Override
	public UserListVO pwUpdate(UserListVO vo) throws Exception {
		return logDAO.pwUpdate(vo);
	}

	@Override
	public UserListVO checkPw(UserListVO vo) throws Exception {
		return logDAO.checkPw(vo);
	}
}