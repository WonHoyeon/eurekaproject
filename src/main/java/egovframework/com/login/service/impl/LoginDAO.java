package egovframework.com.login.service.impl;

import egovframework.com.cmm.service.impl.DexiComAbstractDAO;
import egovframework.com.user.service.vo.UserListVO;
import org.springframework.stereotype.Repository;

/**
 * 시뮬레이션 리스트 DAO 클래스를 정의한다.
 * @author 고동현
 * @since 2019.04.13
 * @version 1.0
 * @see
 *
 * <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 */
@Repository("logDAO")
public class LoginDAO extends DexiComAbstractDAO {

    public UserListVO getPw(UserListVO vo) throws Exception{
        return selectOne("logDAO.getPw", vo);
    }
    public UserListVO pwUpdate(UserListVO vo) throws Exception{
        return selectOne("logDAO.pwUpdate", vo);
    }
    public UserListVO checkPw(UserListVO vo) throws Exception{
        return selectOne("logDAO.checkPw", vo);
    }
}