package egovframework.com.login.service;

import egovframework.com.user.service.vo.UserListVO;

public interface LogService {

    public UserListVO getPw(UserListVO vo) throws Exception;
    public UserListVO pwUpdate(UserListVO vo) throws Exception;
    public UserListVO checkPw(UserListVO vo) throws Exception;
}
