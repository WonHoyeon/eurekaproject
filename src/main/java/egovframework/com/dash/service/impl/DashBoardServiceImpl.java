package egovframework.com.dash.service.impl;

import egovframework.com.dash.service.DashBoardService;
import egovframework.com.dash.service.criteria.DashBoard3DCriteria;
import egovframework.com.dash.service.criteria.DashBoardCriteria;
import egovframework.com.dash.service.criteria.KpiSearchCriteria;
import egovframework.com.dash.service.criteria.SimulationCriteria;
import egovframework.com.dash.service.vo.*;
import egovframework.com.sim.service.vo.ProcessMapInfoVO;
import egovframework.com.sim.service.vo.ProcessModelInfoVO;
import egovframework.rte.fdl.cmmn.EgovAbstractServiceImpl;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 시뮬레이션 리스트 클래스를 정의한다.
 *
 * @author 고동현
 * @version 1.0
 * @see <pre>
 * << 개정이력(Modification Information) >>
 *
 *   수정일      수정자           수정내용
 *  -------    --------    ---------------------------
 *   2019.04.13  고동현          최초 생성
 *
 * </pre>
 * @since 2019.04.13
 */

@Component
@Service("dashboardService")
public class DashBoardServiceImpl extends EgovAbstractServiceImpl implements DashBoardService {

    @Resource(name = "dashboardDAO")
    private DashboardDAO dashboardDAO;

    /**
     * @return 전체 공장들의 좌표출력 정보
     * @throws Exception
     */
    @Override
    public List<MapLocationVO> selectMapLocation() throws Exception {
        return dashboardDAO.selectMapLocation();
    }
//  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @return 공장 상태메세지 TIMELINE에 필요한 정보
     * @throws Exception
     */
    @Override
    public List<FactoryMessageVO> selectFactoryMessage(DashBoardCriteria criteria) throws Exception {
        return dashboardDAO.selectFactoryMessage(criteria);
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @Override
    public GlobalMapVO selectGlobalMap(DashBoardCriteria criteria) throws Exception {
        return dashboardDAO.selectGlobalMap(criteria);
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @Override
    public FactoryMapVO selectFactorylMap(DashBoardCriteria criteria) throws Exception {
        return dashboardDAO.selectFactorylMap(criteria);
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @param criteria ModelId, level
     * @return 공장, 빌딩 등 3D 정보
     * @throws Exception
     */
    @Override
    public List<Monitor3DVO> getFactory3DInfo(DashBoard3DCriteria criteria) throws Exception {
        return dashboardDAO.getFactory3DInfo(criteria);
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @return 해당 3D map, model, animation 정보호출 반환
     * @throws Exception
     */
    public List<ProcessMapInfoVO> getSimulation(SimulationCriteria criteria) throws Exception {
        return dashboardDAO.getSimulation(criteria);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @return 해당 시뮬레이션 모델정보 반환
     * @throws Exception
     */
    @Override
    public List<ProcessModelInfoVO> getModel(SimulationCriteria criteria) throws Exception {
        return dashboardDAO.getModel(criteria);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     *
     * @return  KPI Master List
     * @throws Exception
     */
    @Override
    public List<KpiMasterVO> getKpiMaster() throws Exception {
        return dashboardDAO.getKpiMaster();
    }

    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return  KPI Master insert
     * @throws Exception
     */
    @Override
    public void insertKpiMaster(KpiMasterVO vo) throws Exception {
         dashboardDAO.insertKpiMaster(vo);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return  KPI Master Delete
     * @throws Exception
     */
    @Override
    public void deleteKpiMaster(DeleteDataVO vo) throws Exception {
         dashboardDAO.deleteKpiMaster(vo);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return  KPI Master Update
     * @throws Exception
     */
    @Override
    public int updateKpiMaster(KpiMasterVO vo) throws Exception {
       return dashboardDAO.updateKpiMaster(vo);
    }

    @Override
    public List<KpiRecordVO> getKpiRecord(SimulationCriteria vo) throws Exception {
        return dashboardDAO.getKpiRecord(vo);
    }

    @Override
    public List<KpiSearchVO> getSearch(KpiSearchCriteria vo) throws Exception {
        return dashboardDAO.getSearch(vo);
    }
    //  ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    /**
     *
     * @return  KPI Record Update
     * @throws Exception
     */
    @Override
    public void updateKpiRecord(KpiRecordVO vo) throws Exception {
        dashboardDAO.updateKpiRecord(vo);
    }

}