package egovframework.com.dash.service.vo;

public class KpiMasterVO {
   private String kpiId;
   private String kpiName;
   private String uiType;
   private String unit;
   private String description;
   private String scope;
   private String formula;
   private String unitOfMeasure;
   private String range;
   private String trend;
   private String timing;
   private String audience;
   private String prodMeth;
   private String notes;
   private String mainCate;
   private String midCate;
   private String subCate;
   private String preference;
   private String diagramFile;
   private String day;
   private String time;
   private String writer;


    public String getKpiId() {
        return kpiId;
    }

    public void setKpiId(String kpiId) {
        this.kpiId = kpiId;
    }

    public String getKpiName() {
        return kpiName;
    }

    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    public String getUiType() {
        return uiType;
    }

    public void setUiType(String uiType) {
        this.uiType = uiType;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    public void setUnitOfMeasure(String unitOfMeasure) {
        this.unitOfMeasure = unitOfMeasure;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getTrend() {
        return trend;
    }

    public void setTrend(String trend) {
        this.trend = trend;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getProdMeth() {
        return prodMeth;
    }

    public void setProdMeth(String prodMeth) {
        this.prodMeth = prodMeth;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getMainCate() {
        return mainCate;
    }

    public void setMainCate(String mainCate) {
        this.mainCate = mainCate;
    }

    public String getMidCate() {
        return midCate;
    }

    public void setMidCate(String midCate) {
        this.midCate = midCate;
    }

    public String getSubCate() {
        return subCate;
    }

    public void setSubCate(String subCate) {
        this.subCate = subCate;
    }

    public String getPreference() {
        return preference;
    }

    public void setPreference(String preference) {
        this.preference = preference;
    }

    public String getDiagramFile() {
        return diagramFile;
    }

    public void setDiagramFile(String diagramFile) {
        this.diagramFile = diagramFile;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }
}
