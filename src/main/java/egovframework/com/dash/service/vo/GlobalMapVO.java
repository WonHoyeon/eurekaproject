package egovframework.com.dash.service.vo;

import java.util.List;

public class GlobalMapVO {
    private List<KpiInfoVO> kpiInfo;
    private List<GlobalKpiDataVO> globalKpiData;

    public List<KpiInfoVO> getKpiInfo() {
        return kpiInfo;
    }

    public void setKpiInfo(List<KpiInfoVO> kpiInfo) {
        this.kpiInfo = kpiInfo;
    }

    public List<GlobalKpiDataVO> getGlobalKpiData() {
        return globalKpiData;
    }

    public void setGlobalKpiData(List<GlobalKpiDataVO> globalKpiData) {
        this.globalKpiData = globalKpiData;
    }
}
