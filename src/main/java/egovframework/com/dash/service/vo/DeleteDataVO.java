package egovframework.com.dash.service.vo;

import java.util.ArrayList;

public class DeleteDataVO {
    private ArrayList<String> selectKpiId;

    public ArrayList<String> getSelectKpiId() {
        return selectKpiId;
    }

    public void setSelectKpiId(ArrayList<String> selectKpiId) {
        this.selectKpiId = selectKpiId;
    }
}
