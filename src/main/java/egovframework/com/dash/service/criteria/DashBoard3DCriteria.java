package egovframework.com.dash.service.criteria;

public class DashBoard3DCriteria {
    private String modelId;
    private int level;

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
