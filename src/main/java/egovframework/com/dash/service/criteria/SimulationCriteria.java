package egovframework.com.dash.service.criteria;

public class SimulationCriteria {
    private String mapId;
    private String day;

    public String getMapId() {
        return mapId;
    }

    public void setMapId(String mapId) {
        this.mapId = mapId;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
