package egovframework.com.code.service;

import java.util.ArrayList;

public class SysCodeVO {
    public int getSeqList() {
        return seqList;
    }

    public void setSeqList(int seqList) {
        this.seqList = seqList;
    }

    public int getSeqC() {
        return seqC;
    }

    public void setSeqC(int seqC) {
        this.seqC = seqC;
    }

    private int seqC;
    private int seqList;
    private String cdDv;
    private int isActv;
    private String cdDvNm;
    private String cdDvDescr;
    private String loclId;
    private String cdDvTp;
    private String cdId;
    private String cdNm;
    private int seq;
    private String cdDescr;
    private String cdSymb;
    private ArrayList<String> checkedCodeId;
    private ArrayList<String> checkedCode;

    public ArrayList<String> getCheckedCodeId() { return checkedCodeId; }
    public void setCheckedCodeId(ArrayList<String> checkedCodeId) { this.checkedCodeId = checkedCodeId; }

    public String getCdSymb() { return cdSymb; }
    public void setCdSymb(String cdSymb) { this.cdSymb = cdSymb; }

    public ArrayList getCheckedCode() { return checkedCode; }
    public void setCheckedCode(ArrayList checkedCode) { this.checkedCode = checkedCode; }

    public String getCdDescr() { return cdDescr; }
    public void setCdDescr(String cdDescr) { this.cdDescr = cdDescr; }

    public String getCdDvTp() { return cdDvTp; }
    public void setCdDvTp(String cdDvTp) { this.cdDvTp = cdDvTp; }

    public String getCdId() {
        return cdId;
    }
    public void setCdId(String cdId) {
        this.cdId = cdId;
    }

    public String getCdNm() {
        return cdNm;
    }
    public void setCdNm(String cdNm) {
        this.cdNm = cdNm;
    }

    public int getSeq() {
        return seq;
    }
    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getCdDv() {
        return cdDv;
    }
    public void setCdDv(String cdDv) {
        this.cdDv = cdDv;
    }

    public int getIsActv() {
        return isActv;
    }
    public void setIsActv(int isActv) {
        this.isActv = isActv;
    }

    public String getCdDvNm() {
        return cdDvNm;
    }
    public void setCdDvNm(String cdDvNm) {
        this.cdDvNm = cdDvNm;
    }

    public String getCdDvDescr() {
        return cdDvDescr;
    }
    public void setCdDvDescr(String cdDvDescr) {
        this.cdDvDescr = cdDvDescr;
    }

    public String getLoclId() {
        return loclId;
    }
    public void setLoclId(String loclId) {
        this.loclId = loclId;
    }
}
