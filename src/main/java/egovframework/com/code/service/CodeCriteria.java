package egovframework.com.code.service;

public class CodeCriteria {

    private String selectedValue;
    private String searchText;
    private String cdDv;

    public String getCdDvTp() {
        return cdDvTp;
    }

    public void setCdDvTp(String cdDvTp) {
        this.cdDvTp = cdDvTp;
    }

    private String cdDvTp;
    private String cdId;
    private String loclId;
    private int seqC;

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    private int seq;

    public int getSeqC() {
        return seqC;
    }

    public void setSeqC(int seqC) {
        this.seqC = seqC;
    }

    public String getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(String selectedValue) {
        this.selectedValue = selectedValue;
    }

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }

    public String getCdDv() {
        return cdDv;
    }

    public void setCdDv(String cdDv) {
        this.cdDv = cdDv;
    }

    public String getCdId() {
        return cdId;
    }

    public void setCdId(String cdId) {
        this.cdId = cdId;
    }

    public String getLoclId() {
        return loclId;
    }

    public void setLoclId(String loclId) {
        this.loclId = loclId;
    }
}
