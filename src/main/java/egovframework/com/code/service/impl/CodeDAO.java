package egovframework.com.code.service.impl;

import egovframework.com.cmm.service.impl.DexiComAbstractDAO;
import egovframework.com.code.service.CodeCriteria;
import egovframework.com.code.service.SysCodeVO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("codeDAO")
public class CodeDAO extends DexiComAbstractDAO {


    public List<SysCodeVO> getCodeDetailList() throws Exception{
        return selectList("codeDAO.getCodeDetailList");
    }
    public void delCodeDetail(SysCodeVO vo) throws Exception{
        delete("codeDAO.delCodeDetail", vo);
    }
    public void updateCodeDetail(SysCodeVO vo) throws Exception{
        update("codeDAO.updateCodeDetail", vo);
    }
    public void updateCode(SysCodeVO vo) throws Exception{
        update("codeDAO.updateCode", vo);
    }
    public void updateSysCode(SysCodeVO vo) throws Exception{
        update("codeDAO.updateSysCode", vo);
    }
    public void addSysCode(SysCodeVO vo) throws Exception{
        insert("codeDAO.addSysCode", vo);
    }
    public void addSysCodeList(SysCodeVO vo) throws Exception{
        insert("codeDAO.addSysCodeList", vo);
    }
    public void delSysCodeList(SysCodeVO vo) throws Exception {
        delete("codeDAO.delSysCodeList", vo);
    }
    public void addUserCode(SysCodeVO vo) throws Exception{
        insert("codeDAO.addUserCode", vo);
    }
    public void addUserCodeList(SysCodeVO vo) throws Exception{
        insert("codeDAO.addUserCodeList", vo);
    }
    public List<SysCodeVO> getSysCode() throws Exception{
        return selectList("codeDAO.getSysCode");
    }
    public List<SysCodeVO> getCodeList() throws Exception{
        return selectList("codeDAO.getCodeList");
    }
    public List<SysCodeVO> getUserCodeList() throws Exception{
        return selectList("codeDAO.getUserCodeList");
    }
    public List<SysCodeVO> getSysCodeList() throws Exception{
        return selectList("codeDAO.getSysCodeList");
    }
    public List<SysCodeVO> getSysCodeDetail(SysCodeVO vo) throws Exception{
        return selectList("codeDAO.getSysCodeDetail", vo);
    }
    public List<SysCodeVO> getSysCodeDetail2(SysCodeVO vo) throws Exception{
        return selectList("codeDAO.getSysCodeDetail2", vo);
    }
    public void delUserCodeList(SysCodeVO vo) throws Exception{
        delete("codeDAO.delUserCodeList", vo);
    }
    public void delUserCode(SysCodeVO vo) throws Exception{
        delete("codeDAO.delUserCode", vo);
    }
    public List<SysCodeVO> searchDetail(CodeCriteria vo) throws Exception{
        return selectList("codeDAO.searchDetail", vo);
    }
    public List<SysCodeVO> searchCode(CodeCriteria vo) throws Exception{
        return selectList("codeDAO.searchCode", vo);
    }
    public List<SysCodeVO> searchSysCode(CodeCriteria vo) throws Exception{
        return selectList("codeDAO.searchSysCode", vo);
    }
    public List<SysCodeVO> searchSysDetail(CodeCriteria vo) throws Exception{
        return selectList("codeDAO.searchSysDetail", vo);
    }
    public List<SysCodeVO> getSequence(SysCodeVO vo) throws Exception{
        return selectList("codeDAO.getSequence", vo);
    }
    public List<SysCodeVO> maxSequence(SysCodeVO vo) throws Exception{
        return selectList("codeDAO.maxSequence", vo);
    }
    public List<SysCodeVO> thisSequence(SysCodeVO vo) throws Exception{
        return selectList("codeDAO.thisSequence", vo);
    }
    public SysCodeVO checkCdDv(SysCodeVO vo) throws Exception{
        return selectOne("codeDAO.checkCdDv", vo);
    }
    public SysCodeVO checkCdDv2(SysCodeVO vo) throws Exception{
        return selectOne("codeDAO.checkCdDv2", vo);
    }
}