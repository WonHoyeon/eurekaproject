package dexi.com.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JavaType;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;

public class CustomMappingJackson2HttpMessageConverter extends MappingJackson2HttpMessageConverter {
    @Override
    public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage) throws IOException, HttpMessageNotReadableException {
        JavaType javaType = getJavaType(type, contextClass);
        Object obj = readJavaType(javaType, inputMessage);

        JsonFactory jsonFactory = super.getObjectMapper().getJsonFactory().setCharacterEscapes(new HTMLCharacterEscapes());

        StringWriter output = new StringWriter();
        JsonGenerator jsonGenerator = jsonFactory.createJsonGenerator(output);
        jsonGenerator.writeObject(obj);

        String result = output.toString(); //cleanXSS();
        Object resultObj = super.getObjectMapper().readValue(result, javaType);
        return resultObj;
    }


    private Object readJavaType(JavaType javaType, HttpInputMessage inputMessage) {
        try {
            return super.getObjectMapper().readValue(inputMessage.getBody(), javaType);
        } catch (IOException ex) {
            throw new HttpMessageNotReadableException("Could not read JSON: "+ ex.getMessage(), ex);
        }
    }


    @Override
    protected void writeInternal(Object object, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        // json 출력시 Character 변경 초기화 ...
        // 이것을 null 로 안할경우에 json 출력시 따옴표도 replace 된다. (javascript단에서 Json 파싱시에 에러발생)
        super.getObjectMapper().getJsonFactory().setCharacterEscapes(null);
        String json = super.getObjectMapper().writeValueAsString(object);
        String result = json.toString();
        outputMessage.getBody().write(result.getBytes());
    }
}
