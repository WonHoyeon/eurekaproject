import React, {Component, Fragment} from 'react';
import {inject, observer} from "mobx-react";
import {Icon, Menu} from "antd";
import {Link} from "react-router-dom";
import {MenuItemList} from "../../typescript/IndexType";
import SubMenu from "antd/es/menu/SubMenu";


/**
 * @description 좌측메뉴의 메뉴리스트를 출력하는 클래스
 */
@inject('IndexStore')
@observer
class Left extends Component<any, any> {

    private setTree(menuItem: MenuItemList[]): any {
        let jsxResult: any = menuItem.map((value: MenuItemList) => {
            // 제일 마지막 자식 (더이상 자식이 없음.)
            if (value.child.length === 0) {
                return <Menu.Item key={value.id} style={{paddingLeft:0, paddingRight:0}}>
                    <Link to={value.url}><Icon type={value.icon}/>{value.text}</Link>
                </Menu.Item>;
            } else {
                let textTag: any = value.text;
                if (value.icon != undefined) {
                    // 안전관제 관리 | 작업자 안전관제 | 분석 레보트
                    textTag = <span><Icon type={value.icon}/><span>{value.text}</span></span>
                }
                return <SubMenu key={value.id} title={textTag} style={{paddingLeft:0, paddingRight:0}}>
                    {this.setTree(value.child)}
                </SubMenu>;
            }
        });
        return jsxResult;
    }


    render() {
        const {IndexStore} = this.props;
        let jsxResult: any = this.setTree(IndexStore.getLeftMenuList);
        return (
            <>
                <div style={{ textAlign: 'center', color: 'white', fontSize: 20, padding: 10 }}>
                             <img src={require('../../../store/logo.png')} width="100" height="50" />
                        </div>

                        <Menu
                            defaultSelectedKeys={['1']}
                            mode="inline"
                            theme="light">
                            {/* 1 Depth 처리 */}
                            {jsxResult}
                        </Menu>
            </>
        );
    };
}


export default Left;
