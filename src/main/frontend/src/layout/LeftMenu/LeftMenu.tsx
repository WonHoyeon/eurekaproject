import React, {Component, Fragment} from 'react';
import {inject, observer} from "mobx-react";
import autobind from "autobind-decorator";
import Media from "react-media";
import Left from "./Left";

@inject('IndexStore')
@observer
class LeftMenu extends Component<any, any> {

    @autobind
    private onClickMenu1(){
        this.props.IndexStore.setMenu1()
    }
    render() {

        return (
            <div className="col-3 col-2-3 col-3-3 col-4-3 col-5-3 col-6-3" id={"leftMenu"} style={{height:936, backgroundColor:"#001529", width:250}}>


                <Media queries={{
                    small: "(max-width: 650px)",
                    small1: "(min-width: 651px) and (max-width: 767px)",
                    medium: "(min-width: 768px) and (max-width: 1140px)",
                    large: "(min-width: 1140px)"
                }}>
                    {matches => (
                        <Fragment>
                            {matches.small && null}
                            {matches.small1 && null}
                            {matches.medium && <Left/>}
                            {matches.large && <Left/>}
                        </Fragment>
                    )}
                </Media>

                </div>
        );
    };
}


export default LeftMenu;
