import React, {Component} from 'react';
import 'antd/dist/antd.css';
import '../admin/layoutstyle.css';
import {Avatar} from "antd";
import DvrIcon from '@material-ui/icons/Dvr';
import {MenuFoldOutlined, MenuUnfoldOutlined,} from '@ant-design/icons';

class TopMenu extends Component<any, any> {

    /** @description  */
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };


    render() {

        return (


            <>eureka/Information

                {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                    className: 'trigger',
                    onClick: this.toggle,
                })}

                <div style={{ float: "right", marginRight: 15, marginLeft: 10 }}>
                    <Avatar style={{ backgroundColor: '#87d068' }} size={"small"}>Dexta</Avatar> Dexta
                            </div>


                <div style={{ float: "right", marginRight: 15 }}>
                    <DvrIcon fontSize={"default"} color={"action"}/>
                </div>
            </>
        );
    };
}


export default TopMenu;
