import React, {Component} from 'react';
import {Link, Route, Switch} from "react-router-dom";
import SimulationList from "../admin/simulation/layout/SimulationList/SimulationList";
import SimulationPage from "../admin/simulation/layout/SimulationPage";
import '../css/IndexPage.css';
import FactoryAll from "../admin/monitoring/layout/FactoryAll";
import {inject, observer} from "mobx-react";
import {Avatar, Button, Icon, Layout, Menu, Popover} from "antd";
import {MenuItemList} from "../typescript/IndexType";
import SubMenu from "antd/es/menu/SubMenu";
import DvrIcon from '@material-ui/icons/Dvr';
import {MenuFoldOutlined, MenuUnfoldOutlined,} from '@ant-design/icons';
import GlobalMap from "../admin/DashBoard/GlobalMap";
import LoginForm from "../admin/User/Component/LoginForm";

const {Header, Content, Footer, Sider} = Layout;

/**
 * IndexPage, 전체적인 레이아웃이 포함된 컴포넌트 페이지입니다.
 * ```
 * 좌측메뉴, 상단메뉴, 컨텐츠를 포함한 전체적 레이아웃이 포함되어있는 페이지입니다.
 * ```
 * mobx [[dashboard]] 와 [[monitor]] 을 상속한 클래스 (IndexPage.tsx)
 */
@inject('IndexStore')
@observer
class IndexPage extends Component<any, any> {
    componentDidMount() {
    }


    /** @description 죄측메뉴 collapsed(fold) */
    state = {
        collapsed: false,
    };

    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };


    private setTree(menuItem: MenuItemList[]): any {
        let jsxResult: any = menuItem.map((value: MenuItemList) => {
            // 제일 마지막 자식 (더이상 자식이 없음.)
            if (value.child.length === 0) {
                return <Menu.Item key={value.id} style={{paddingLeft: 0, paddingRight: 0}}><Link to={value.url}><Icon
                    type={value.icon}/>{value.text}</Link></Menu.Item>;
            } else {
                let textTag: any = value.text;
                if (value.icon != undefined) {
                    // 안전관제 관리 | 작업자 안전관제 | 분석 레보트
                    textTag = <span><Icon type={value.icon}/><span>{value.text}</span></span>
                }
                return <SubMenu key={value.id} title={textTag} style={{paddingLeft: 0, paddingRight: 0}}>
                    {this.setTree(value.child)}
                </SubMenu>;
            }
        });
        return jsxResult;
    }

    popupWindow = (evt: any) => {
        if (sessionStorage.getItem('loggedInfo') == "DEXI") {
            window.open('/eureka/Information', 'window_name', 'width=1000,height=700,location=no,status=no,scrollbars=yes', false);
        }
        return false;
    }

    logout = () => {
        sessionStorage.removeItem('loggedInfo');
        window.location.replace("/eureka/");
    }

    render() {
        const content = (
            <Menu mode="inline"
                  style={{color: 'red'}}>
                <Menu.Item key="1" onClick={this.logout}>
                    <span>Logout</span>
                </Menu.Item>
            </Menu>
        );
        const {IndexStore} = this.props;
        let jsxResult: any = this.setTree(IndexStore.getLeftMenuList);
        return (
            <Layout style={{height: '100vh', minWidth: 1280}}>
                <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                    <div style={{
                        textAlign: 'center',
                        backgroundColor: 'white',
                        fontSize: 20,
                        padding: 10,
                        borderRight: "solid"
                    }}>
                        <a href="/eureka/Main"><img src={require('../store/logo.png')} width="61" height="45"/></a>
                    </div>
                    <Menu
                        defaultSelectedKeys={['1']}
                        mode="inline"
                        theme="dark">
                        {/* 1 Depth 처리 */}
                        {jsxResult}
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background">
                        {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick: this.toggle,
                        })}

                        {sessionStorage.getItem('loggedInfo') !== null ? <span>
                            <div style={{float: "right", marginRight: 15, marginLeft: 10}}>
        <Popover content={content} placement="bottom" title={sessionStorage.getItem('loggedInfo') + '님'}>
                                <Avatar style={{backgroundColor: '#87d068'}}
                                        size={"small"}>{sessionStorage.getItem('loggedInfo')}</Avatar>
        </Popover>
                            </div>
                            <div style={{float: "right", margin: 7}}>
                                <a href="">
                                    {(sessionStorage.getItem('loggedInfo') == "DEXI") ?
                                        <DvrIcon fontSize={"default"} color={"action"}
                                                 onClick={this.popupWindow}/> : null}
                                </a>
                            </div>
                        </span> : null
                        }
                    </Header>
                    <Content
                        style={{
                            padding: 24,
                            minHeight: 280,
                        }}
                    >

                        <Switch>
                            <Route path="/Main" component={GlobalMap} exact={true}/>
                            <Route path="/Main/Simulation" component={SimulationList}/>
                            <Route path="/Main/Monitoring/:name" component={FactoryAll}/>
                            <Route path="/Main/SimulationPage" component={SimulationPage}/>
                        </Switch>

                    </Content>
                </Layout>
            </Layout>
        );
    };
}

export default IndexPage;
