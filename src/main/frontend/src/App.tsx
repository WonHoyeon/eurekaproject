import React, {Component} from 'react';
import IndexPage from "./layout/IndexPage";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import KpiIndex from "./admin/kpi/layout/KpiIndex";
import Login from "./admin/User/Component/Login";
import LoginForm from "./admin/User/Component/LoginForm";


class App extends Component<any, any> {


    render() {
        return (
            <div style={{backgroundColor: '#f0f2f5', height: '100vh'}}>
                <BrowserRouter basename={'eureka'}>

                    <Switch>
                        {sessionStorage.getItem("loggedInfo") !== null ?
                            <>
                                <Route path="/" component={IndexPage} exact={true}/>
                                < Route path="/Main" component={IndexPage}/>
                                <Route path="/Information" component={KpiIndex}/>
                                <Route path="/user" component={Login}/>
                            </>
                            :
                            <Route path="/" component={Login}/>
                        }
                    </Switch>

                </BrowserRouter>
            </div>
        );
    };
}


export default App;
