import React, {Component} from 'react';
import {IModelInfoPopup} from "../etc/IModelList";
import ModelView from "./ModelView";
import {Col, Modal, Row} from 'antd';

class ModelDetailView extends Component<IModelInfoPopup, any> {
    constructor(props:IModelInfoPopup) {
        super(props);
    }


    render() {
        const modalInfo : {numWidth:number, width:string , height:string} = {
            numWidth:800 ,
            width:"800px" ,
            height:"350px"
        };

        // @ts-ignore
        return (
            <div  >
                <Modal
                    title={this.props.modelInfo.stenc3dFileNm }
                    width={modalInfo.numWidth}
                    forceRender={true}
                    bodyStyle={{width:modalInfo.width , height:modalInfo.height }}
                    destroyOnClose={true}
                    visible={this.props.visible}
                    onOk={() => this.props.modalOKClickEvent()}
                    onCancel={() => this.props.modalOKClickEvent()}
                >
                    <Row gutter={24} >
                        <Col span={12} style={{height:"calc("+modalInfo.height+ " - 40px)" }}>
                            <ModelView positionInfo={this.props.modelInfo} />
                        </Col>
                        <Col span={12}>
                            <p>scale 정보 : {this.props.modelInfo.modelScale}</p>
                            <p>활성화 정보 :{this.props.modelInfo.isacty}</p>
                        </Col>
                    </Row>
                </Modal>
            </div>
        );
    }
}

export default ModelDetailView;