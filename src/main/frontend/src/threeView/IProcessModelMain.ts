export interface IProcessModelMain {
    modelInfo:Array<IProcessModelInfo>,
    mapInfo:Array<IProcessMapInfo>,
    processAnimationData?:Array<IProcessAnimationInfo>,
    backgroundColor?:number;
    clientSize ? : {width:number,height:number}
    // playType ? : IAnimationPlayType;
    processSuccessFailFun?:Function;
    SimulationStore:any;
    SetAdress?:any;
    visibleSet?:any;
    visibleGet?:any;
    closeSet?:any;
}


export interface IProcessAnimationInfo{
    processId: string,
    processType: string,
    productIdx: string,
    productSeq: string,
    simIndex: string,
    simLoadingTime: string,
    simProcessTime: string,
    simUnloadingTime: string,
    mapId:string,
    stencLenx: number,
    stencLeny: number,
    stencLenz: number,
    stencNm: string,
    stencPosx: number,
    stencPosy: number,
    stencPosz: number,
    status:string,
    startTime:number,
    waitingTime:number,
    endTime?:number
}

export interface IProcessModelInfo {
    stencId: string,
    stenckey?:string,
    stencNm: string,
    stencLenx: number,
    stencLeny: number,
    stencLenz: number,
    stencPosx: number,
    stencPosy: number,
    stencPosz: number,
    zIndex: number,
    mapId: string,
    modelScale:number,
    rotAngle: number,
    stenc3dTp: string,
    stenc3did: string,
    stenc3dinfoid: string,
    fillColor:number,
    lineColor:number,
    opacity?:number ,
    stenc3dfile?:string
}

export interface IFactoryModelAttrData{
    attrid: string,
    attrkey: string,
    attrtxt: string,
    attrval: string,
    stencId: string,
    stencNm: string,
    stenckey: string,
}


export interface IProcessMapInfo{
    mapId: string ,
    modelItemId: string ,
    mapRangeLenx: number,
    mapRangeLeny: number,
    mapRangeLenz: number,
    mapGridx: number,
    mapGridy: number,
    mapGridz: number,
}