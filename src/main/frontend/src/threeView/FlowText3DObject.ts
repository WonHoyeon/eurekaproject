import {Group, CanvasTexture, RepeatWrapping, PlaneBufferGeometry, MeshBasicMaterial, Mesh, Texture, TextureLoader} from 'three';
import Box3DObject from "./Box3DObject";
import Windows3DObject from "./Windows3DObject";

export default class FlowText3DObject{

    private _flowTextGroup:Group = new Group();
    private _textureList:CanvasTexture | null = null;
    private _flowMash:Mesh = new Mesh(undefined, undefined);

    constructor( x:number, y:number, z:number, textWidth:number , textHeight:number, text:string, rotateX?:number , rotateY ?:number){
        const textLength :number = 15;
        let ctx:CanvasRenderingContext2D | null = document.createElement("canvas").getContext("2d");
        if(ctx != null){
            ctx.canvas.width = 32;
            ctx.canvas.height = 32;
            ctx.translate(16, 16);
            ctx.fillStyle = "rgb(0,255,255)";
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";
            ctx.font = "30px sans-serif";
            //→⇒≫▶
            ctx.fillText(text , 0, 0);

            let wWidth:number = (textWidth > textHeight) ? textWidth : textHeight;
            let texture:CanvasTexture = new CanvasTexture(ctx.canvas);
            texture.wrapS = RepeatWrapping;
            texture.wrapT = RepeatWrapping;
            texture.repeat.x = (wWidth  / textLength  < 1) ? 1 : Math.round(wWidth / textLength);
            texture.repeat.y = 1;
            this._textureList = texture;

            let stripGeo:PlaneBufferGeometry = new PlaneBufferGeometry(textWidth, textHeight , 1 , 1);
            let stripMat:MeshBasicMaterial = new MeshBasicMaterial({
                map: texture,
                color:0xFFFFFF,
                opacity: 1,
                depthWrite: false,
                depthTest: false,
                transparent: true,
            });
            let stripMash:Mesh = new Mesh(stripGeo, stripMat);
            stripMash.rotation.x = (-0.5 * Math.PI);
            stripMash.position.set(0,0,0 );
            this._flowTextGroup.add(stripMash);
        }
        this._flowTextGroup.position.set( x , z , y );
    }

    public getGroup():Group{
        return this._flowTextGroup;
    }
    public getTexture():CanvasTexture | null{
        return this._textureList;
    }
    public getMesh():Mesh {
        return this._flowMash;
    }
}
