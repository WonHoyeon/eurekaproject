import {Texture} from 'three';

export interface IBasePoistion{
    x:number,
    y:number,
    z:number,
    width:number,
    setScissorSizeheight?:number,
    depth:number,
    height:number
}

export interface IBox3DObject extends IBasePoistion{
    edgesVisible:boolean,
    color:number,
    borderColor?:number,
    rotateX?: number ,
    rotateY?: number ,
    rotateZ?: number ,
    opacity?:number ,
    texture?:Texture  ,
    textInfo?:ITextObject
}

export interface ITextObject {
    text:string,
    width:number,
    height:number,
    fillStyle ?: {
        r:number, // default:00
        g:number, // default:00
        b:number, // default:00
    },
    textAlign?:CanvasTextAlign, // default:center
    textBaseline?:CanvasTextBaseline, // default:middle
    fontSize?:number ,// default:15
    fontface?:string ,//default:sans-serif
}

export interface IMarking3dObject{
    x:number,
    y:number,
    z:number,
    IconWidth:number,
    IconFrame:number,
    type:MarkingType,
}

export enum MarkingType{
    CHECK = "check",
    X = "x",
    ALERT = "alert",
}

export interface IFFU3dObject{
    x:number,
    y:number,
    z:number,
    ffuWidtdh:number,
    ffuHeight:number,
    ffuDepth:number,
    frameWidth?:number,
    rotationX?:number ,
    rotationY?:number,
    opacity?:number ,
    marking?:{
        visible?:boolean,
        type?:MarkingType,
    },
}

export interface ICylinder3DObject{
    x:number,
    y:number,
    z:number,
    radiusTop: number,
    radiusBottom: number,
    depth:number,
    edgesVisible:boolean,
    color?:number,
    borderColor?:number,
    rotateX?: number,
}

export interface IScissorLift3DObject extends IBasePoistion{
    IsRiding?:boolean,
    rotationY?:number,
    text?:string
}

export interface IBarricades3DObject {
    x:number,
    y:number,
    z:number,
    width:number,
    frameHeight:number,
    depth:number,
    edgesVisible:boolean,
    color:number,
    rotateX?:number,
    rotateY?: number,
    rotateZ?:number
}

export interface IXcrossFrame3DObject{

    x:number,
    y:number,
    z:number,
    width:number,
    frameHeight:number,
    depth:number,
    edgesVisible:boolean,
    color:number,
    rotateX?:number,
    rotateY?: number,
    rotateZ?:number
}



//
// export interface ISimulationResultViewTopMenu {
//     url: string,
//     text: string;
//     compoent:ComponentType<ISimulationResultContent>;
//     icon?: string;
//     style?: CSSProperties;
//     selection?: boolean;
// }
// export interface ISimulationResultContent{
//     EventFullScreen:Function,
//     visible?:boolean
// }


