import {Group, Texture, TextureLoader , RepeatWrapping} from 'three';
import Box3DObject from "./Box3DObject";
import Windows3DObject from "./Windows3DObject";
import Door3DObject from "./Door3DObject";

export default class PartitionDoor3DObject{

    private _partitionDoorGroup:Group = new Group();

    constructor( x:number, y:number, z:number, partitionDoorWidth:number , partitionDoorHeight:number, partitionDoorDepth:number  , doorVisible:boolean = false  , rotateY?:number , frameWidth:number = 3){

        let partitionDepth:number = (doorVisible == true) ? (partitionDoorDepth / 3) : partitionDoorDepth;
        let partitionZ:number = (doorVisible == true) ? (partitionDoorDepth / 3) : 0;

        let partition:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:partitionZ ,
            width:partitionDoorWidth,
            height:partitionDoorHeight,
            depth:partitionDepth,
            edgesVisible:true ,
            color:0xFFFFFF
        });
        this._partitionDoorGroup.add(partition.getGroup());


        if( doorVisible == true ){

            let doorFrameTopZ:number = (partitionDoorDepth  / 2) - (partitionDoorDepth / 3) - (frameWidth / 2);
            // 1. 문 상단
            let doorFrameTop:Box3DObject = new Box3DObject({
                x:0,
                y:0,
                z:doorFrameTopZ ,
                width:partitionDoorWidth,
                height:frameWidth,
                depth:frameWidth,
                edgesVisible:true ,
                color:0xc2af85
            });
            this._partitionDoorGroup.add(doorFrameTop.getGroup());

            let windowsFrameLeftX:number = -(partitionDoorWidth / 2) + (frameWidth / 2);
            let windowsFrameLeftY:number = 0;
            let windowsFrameLeftZ:number = doorFrameTopZ - (partitionDoorDepth / 3);
            // 2. 창문 좌측
            let doorFrameLeft:Box3DObject = new Box3DObject({
                x:windowsFrameLeftX,
                y:windowsFrameLeftY,
                z:windowsFrameLeftZ ,
                width:frameWidth,
                height:frameWidth,
                depth:(partitionDoorDepth / 3)  * 2 - frameWidth,
                edgesVisible:true ,
                color:0xc2af85
            });
            this._partitionDoorGroup.add(doorFrameLeft.getGroup());

            let windowsFrameRightX:number = (partitionDoorWidth / 2) - (frameWidth / 2);
            let windowsFrameRightY:number = 0;
            let windowsFrameRightZ:number = windowsFrameLeftZ;
            // 2. 창문 우측
            let windowsFrameRight:Box3DObject = new Box3DObject({
                x:windowsFrameRightX,
                y:windowsFrameRightY,
                z:windowsFrameRightZ ,
                width:frameWidth,
                height:frameWidth,
                depth:(partitionDoorDepth / 3)  * 2 - frameWidth,
                edgesVisible:true ,
                color:0xc2af85
            });
            this._partitionDoorGroup.add(windowsFrameRight.getGroup());

            // 1. 문
            let rotateY:number = 45;
            //doorWidth : 전체 삼각형 밑변
            let doorWidth:number = partitionDoorWidth - (frameWidth * 2);
            // doorOrgWidth : 실제 문 길이  , doorOrgHeight: 실제 문 밑변
            const doorOrgWidth:number = doorWidth;
            const doorOrgHeight:number = doorWidth * parseFloat((rotateY * Math.PI / 180).toFixed(3));

            //https://m.blog.naver.com/PostView.nhn?blogId=pss2072&logNo=220798287435&proxyReferer=https%3A%2F%2Fwww.google.com%2F
            // 실제 밑변 구하기 : doorWidth / parseFloat((45 * Math.PI / 180).toFixed(3))
            // (실제 밑변 - 전체 삼각형 밑변) / 2 - (기둥 width  / 2)
            let openDoorX:number =  (doorWidth / parseFloat((rotateY * Math.PI / 180).toFixed(3)) - doorOrgWidth) / 2;
            let openDoorY:number =  (doorOrgHeight / 2) - frameWidth ;
            let door:Box3DObject = new Box3DObject({
                x:openDoorX,
                y:openDoorY,
                z:windowsFrameRightZ ,
                width:doorWidth,
                height:frameWidth,
                depth:(partitionDoorDepth / 3)  * 2 - frameWidth,
                edgesVisible:true ,
                color:0x7b2100,
                rotateY:rotateY,
            });
            this._partitionDoorGroup.add(door.getGroup());
        }

        // parseFloat((45 * Math.PI / 180).toFixed(3));

        const positionY:number = z ;//+ (wellDepth / 2);
        if(rotateY != undefined){
            this._partitionDoorGroup.rotateY( rotateY  * Math.PI / 180 );
        }
        this._partitionDoorGroup.position.set( x , positionY , y );
    }

    public getGroup():Group{
        return this._partitionDoorGroup;
    }
}
