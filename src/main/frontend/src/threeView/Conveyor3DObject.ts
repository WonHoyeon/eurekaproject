import {CylinderGeometry, Group, Mesh, MeshBasicMaterial} from 'three';
import Box3DObject from "./Box3DObject";

export default class Conveyor3DObject{

    private readonly _frameColor:number = 0xe7e7bb;
    private _axis:Group = new Group();
    private readonly _centerFrameWidth:number = 5;

    constructor( x:number, y:number, z:number, loadWidth:number , loadHeight:number , dept:number , rotate?: number ){
        const hframeDept:number = (dept / 4) * 3;

        let orgLoadWidth : number = loadWidth;
        let orgLoadHeight : number = loadHeight;

        if(loadWidth < loadHeight) {
            rotate = 90;
            loadWidth = orgLoadHeight;
            loadHeight = orgLoadWidth;
        }

        this.addMovingBelt( 0, 0, 0, loadWidth , loadHeight, (dept / 4) );
        this.addHFrame( 0  , 0 , 0 , loadWidth , loadHeight, hframeDept );
        this.addHFrame( (loadWidth / 2) - (this._centerFrameWidth /2)  , 0 , 0 , loadWidth , loadHeight, hframeDept );
        this.addHFrame( loadWidth - this._centerFrameWidth , 0 , 0 , loadWidth , loadHeight, hframeDept );

        let centerBeltFrameGeometry:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:-(dept / 2) +this._centerFrameWidth ,
            width:loadWidth,
            height:this._centerFrameWidth,
            depth:this._centerFrameWidth ,
            edgesVisible:false ,
            color:this._frameColor
        });
        this._axis.add(centerBeltFrameGeometry.getGroup());

        if(rotate != undefined){
            this._axis.rotation.y = 1.57 * (rotate / 90);
        }
        this._axis.position.set( x ,  z , y);
    }

    public getGroup():Group{
        return this._axis;
    }

    private addMovingBelt( x:number, y:number, z:number, loadWidth:number , loadHeight:number , depth:number ):void{
        const beltFrameWidth:number = this._centerFrameWidth;

        let leftBeltFrameGeometry :Box3DObject = new Box3DObject({
            x:x,
            y:-(loadHeight / 2) +(beltFrameWidth / 2),
            z:z,
            width:loadWidth,
            height:beltFrameWidth,
            depth:depth ,
            edgesVisible:true ,
            color:this._frameColor
        });
        this._axis.add(leftBeltFrameGeometry.getGroup());
        //
        let rightBeltFrameGeometry:Box3DObject = new Box3DObject({
            x:x,
            y:(loadHeight / 2)-(beltFrameWidth / 2),
            z:z,
            width:loadWidth ,
            height:beltFrameWidth,
            depth:depth ,
            edgesVisible:true ,
            color:this._frameColor
        });
        this._axis.add(rightBeltFrameGeometry.getGroup());

        let cylinderLineGroup:Group = new Group();
        let gap:number = 2;

        for( let startPoint = 0; startPoint <= loadWidth ; startPoint += gap + ( 10 ) ) {
            let geometry:CylinderGeometry = new CylinderGeometry( 2, 2, (loadHeight - (beltFrameWidth) ), 7 );
            let material:MeshBasicMaterial = new MeshBasicMaterial( {color: 0xc5c4c8 } );
            let cylinder:Mesh = new Mesh( geometry, material );
            cylinder.rotation.x = 1.57 * (90 / 90);
            cylinder.position.set ( startPoint  , 0 , 0 );
            cylinderLineGroup.add(cylinder);
        }
        cylinderLineGroup.position.set( x - (loadWidth / 2) , z ,y + ((loadHeight /2) - (beltFrameWidth * 2)) );
        this._axis.add(cylinderLineGroup);
    }

    private addHFrame( x:number, y:number, z:number, loadWidth:number , loadHeight:number , depth:number):void {
        const beltFrame:number = this._centerFrameWidth;

        let hFrameGroup:Group = new Group();
        let leftBeltFrameX:number = - (loadWidth / 2) + (beltFrame / 2);
        let leftBeltFrameY:number = - (loadHeight / 2) + (beltFrame / 2);
        let leftBeltFrameZ:number = - (depth / 2);

        let leftBeltFrameGeometry:Box3DObject = new Box3DObject({
            x:leftBeltFrameX,
            y:leftBeltFrameY,
            z:leftBeltFrameZ,
            width:beltFrame,
            height:beltFrame,
            depth:depth,
            edgesVisible:false ,
            color:this._frameColor
        });
        hFrameGroup.add(leftBeltFrameGeometry.getGroup());

        let rightBeltFrameX:number = - (loadWidth / 2) + (beltFrame / 2);
        let rightBeltFrameY:number = + (loadHeight / 2) - (beltFrame / 2);
        let rightBeltFrameZ:number = - (depth / 2);

        let rightBeltFrameGeometry:Box3DObject = new Box3DObject({
            x:rightBeltFrameX,
            y:rightBeltFrameY,
            z:rightBeltFrameZ,
            width:beltFrame,
            height:beltFrame,
            depth:depth,
            edgesVisible:false ,
            color:this._frameColor
        });
        hFrameGroup.add(rightBeltFrameGeometry.getGroup());

        let centerBeltFrameX:number = - (loadWidth / 2) + (beltFrame / 2);
        let centerBeltFrameY:number = 0 ;
        let centerBeltFrameZ:number = - (depth / 2);

        let centerBeltFrameGeometry:Box3DObject = new Box3DObject({
            x:centerBeltFrameX,
            y:centerBeltFrameY,
            z:centerBeltFrameZ,
            width:beltFrame,
            height:loadHeight - (beltFrame ),
            depth:beltFrame ,
            edgesVisible:false ,
            color:this._frameColor
        });
        hFrameGroup.add(centerBeltFrameGeometry.getGroup());

        hFrameGroup.position.set(  x ,z,y);
        this._axis.add(hFrameGroup);
    }
}
