import {Group, CylinderBufferGeometry, MeshBasicMaterial, Mesh, Geometry, Vector3, Face3, Triangle, CanvasTexture,
    RepeatWrapping,
    PlaneBufferGeometry} from 'three';
import {IMarking3dObject, MarkingType} from "./I3DModel";
import Box3DObject from "./Box3DObject";

export default class Marking3DObject{

    private _axis:Group = new Group();

    constructor( parameter:IMarking3dObject ){

        switch(parameter.type){
            case MarkingType.CHECK :
                this.addCheck(0x00FF00 ,  parameter.IconFrame , parameter.IconWidth);
                break;
            case MarkingType.X:
                this.addX(0xd90502 ,  parameter.IconFrame , parameter.IconWidth);
                break;
            case MarkingType.ALERT:
                if(parameter.IconWidth <= 40){
                    parameter.IconWidth = 40;
                }

                this.addAlert(parameter.IconFrame , parameter.IconWidth);
                break;
            default:
                break;
        }

        this._axis.position.set( parameter.x , parameter.z , parameter.y );
    }

    public getGroup():Group{
        return this._axis;
    }

    private addCheck(color:number , IconFrame:number , IconWidth:number ):void{
        let icon2X:number = (IconFrame / Math.cos(45 * Math.PI / 180));
        let iconX:number = (IconWidth * Math.cos(45 * Math.PI / 180)) ;
        let checkColumnLong:Box3DObject = new Box3DObject({x:0, y:0, z:0, width:IconWidth, height:IconFrame, depth:IconFrame , edgesVisible:false , color:color , rotateZ:45 });
        this._axis.add(checkColumnLong.getGroup());
        let checkColumn:Box3DObject = new Box3DObject({x:-(iconX / 2)-(icon2X /2) , y:0, z:-(iconX / 2)+(icon2X), width:IconFrame, height:IconFrame, depth:(IconFrame * 2) , edgesVisible:false, color:color , rotateZ:45 });
        this._axis.add(checkColumn.getGroup());
    }

    private addX(color:number , IconFrame:number , IconWidth:number ):void{

        let column:Box3DObject = new Box3DObject({x:0, y:0, z:0, width:IconWidth, height:IconFrame, depth:IconFrame , edgesVisible:false , color:color , rotateZ:45 });
        this._axis.add(column.getGroup());

        let column2:Box3DObject = new Box3DObject({x:0 , y:0, z:0, width:IconFrame, height:IconFrame, depth:IconWidth , edgesVisible:false, color:color , rotateZ:45 });
        this._axis.add(column2.getGroup());
    }

    private addAlert( IconFrame:number , IconWidth:number):void{
        let geometry:CylinderBufferGeometry = new CylinderBufferGeometry( (IconWidth/2), (IconWidth / 2), IconFrame,3 , 1,undefined, 6.3  );
        let material:MeshBasicMaterial = new MeshBasicMaterial( {color: 0xFF0000} );
        let cylinder:Mesh = new Mesh( geometry, material );
        cylinder.rotation.x = -0.5 * Math.PI;
        this._axis.add(cylinder);

        let geometry2:CylinderBufferGeometry = new CylinderBufferGeometry( (IconWidth/2) - IconFrame, (IconWidth / 2)- IconFrame, 0.5,3 , 1,undefined, 6.3  );
        let material2:MeshBasicMaterial = new MeshBasicMaterial( {color: 0xFFFFFF} );
        let cylinder2:Mesh = new Mesh( geometry2, material2 );
        cylinder2.rotation.x = -0.5 * Math.PI;
        cylinder2.position.set(0,  0 , (IconFrame / 2) + 0.1);
        this._axis.add(cylinder2);


        let ctx:CanvasRenderingContext2D | null = document.createElement("canvas").getContext("2d");
        if(ctx != null){
            let fontSize:string = (IconWidth - 20).toString();
            ctx.canvas.width = IconWidth;
            ctx.canvas.height = IconWidth;
            ctx.translate((IconWidth/2), (IconWidth / 2));
            ctx.fillStyle = "rgb(0,0,0)";
            ctx.textAlign = "center";
            ctx.textBaseline = "middle";
            ctx.font = fontSize + "px sans-serif";
            ctx.fillText("!", 0, 0);
            let texture:CanvasTexture = new CanvasTexture(ctx.canvas);
            texture.wrapS = RepeatWrapping;
            texture.wrapT = RepeatWrapping;
            texture.repeat.x = 1;
            texture.repeat.y = 1;

            let stripGeo:PlaneBufferGeometry = new PlaneBufferGeometry(50, 50);
            let stripMat:MeshBasicMaterial = new MeshBasicMaterial({
                map: texture,
                color:0xFFFFFF,
                depthWrite: false,
                depthTest: false,
                transparent: true,
            });
            const stripMesh:Mesh = new Mesh(stripGeo, stripMat);
            stripMesh.position.set(0,0 , 3.5);
            this._axis.add(stripMesh);
        }

        //
        // let column:Box3DObject = new Box3DObject({x:item.x, y:item.y, z:item.z , width:item.width, height:item.height , depth:item.depth  , edgesVisible:true , color:item.color });
        // scene.add(column.getGroup());
    }

}
