import {Group} from 'three';
import Box3DObject from "./Box3DObject";

export default class Load3DObject{

    private readonly _asphaltColor:number = 0x868e96;
    private _axis:Group = new Group();

    private readonly _centerLineWidth:number = 2;
    private readonly _center:number = 20;
    private readonly _centerLineHeightGap:number = 5;

    constructor( x:number, y:number, z:number, loadWidth:number , loadHeight:number ){
        this.addAsphalBackGround( 0, 0, 2.5, loadWidth , loadHeight, 5 );

        if(loadWidth > loadHeight){
            this.addAsphalCenterLine( 0 ,0 , 5 ,loadWidth , true);
            this._axis.position.set( x , z , y );
        }else{
            this.addAsphalCenterLine( 0 ,0 , 5 ,loadHeight);
            this._axis.position.set( x , z , y  +(loadHeight / 2) );


        }
    }


    public getGroup():Group{
        return this._axis;
    }

    private addAsphalBackGround( x:number, y:number, z:number, loadWidth:number , loadHeight:number , depth:number ):void{

        let colorNum:number = this._asphaltColor;
        // let yPosition:number = y + (loadHeight / 2);
        let asphalt:Box3DObject = new Box3DObject({
            x:x,
            y:y,
            z:z,
            width:loadWidth,
            height:loadHeight,
            depth:depth ,
            edgesVisible:false ,
            color:colorNum
        });
        this._axis.add(asphalt.getGroup());

    }

    private addAsphalCenterLine( x:number, y:number, z:number,loadHeight:number , rotation : boolean = false ):void {
        let lineColorNum:number = 0xFFFFFF;

        const loadHalfHeight:number = (loadHeight / 2);
        const centerLineGroup:Group = new Group();

        for( let startPoint = - loadHalfHeight ; startPoint < loadHalfHeight ; startPoint += (this._center + this._centerLineHeightGap)) {
            let lineHeight :number = this._center;
            // 마지막 라인 설정 ...
            if((startPoint + this._centerLineHeightGap + lineHeight) > loadHalfHeight ){
                lineHeight = loadHalfHeight - (startPoint);
                startPoint = startPoint - (this._center ) + (this._centerLineHeightGap * 2) +(lineHeight /2);
            }

            let centerLine:Box3DObject = new Box3DObject({
                x:0,
                y:startPoint,
                z:0.1,
                width:this._centerLineWidth,
                height:lineHeight,
                depth:0.1 ,
                edgesVisible:false ,
                color:lineColorNum
            });
            this._axis.add(centerLine.getGroup());
            centerLineGroup.add(centerLine.getGroup());
        }
        let yPosition : number = y + ( (this._center/2) ); //+(this._centerLineHeightGap /2));
        if(rotation == true ){
            centerLineGroup.position.set( yPosition, z, x  );
            centerLineGroup.rotation.y= 1.57 * ( 90 / 90);
        }else{
            centerLineGroup.position.set( x, z, yPosition  );
        }
        this._axis.add(centerLineGroup);
    }
}
