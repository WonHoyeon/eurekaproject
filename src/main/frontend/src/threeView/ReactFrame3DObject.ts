import {BoxGeometry, EdgesGeometry, Group, LineBasicMaterial, LineSegments, Mesh, MeshLambertMaterial, Texture,
    MeshLambertMaterialParameters} from 'three';
import Box3DObject from "./Box3DObject";

export default class ReactFrame3DObject{

    private _rectFrameGroup:Group = new Group();

    constructor( x:number, y:number, z:number, rectFrameWidth:number , rectFrameHeight:number , rectFrameDepth:number, edgesVisible:boolean = true, color:number = 0xc908786 , borderColor:number = 0x000000, rotateY?: number , opacity?:number){
        // 1. frame 상단
        let rectFrameTop:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:0 ,
            width:rectFrameWidth,
            height:rectFrameHeight,
            depth:rectFrameHeight ,
            edgesVisible:edgesVisible ,
            color:color,
            borderColor:borderColor
        });
        this._rectFrameGroup.add(rectFrameTop.getGroup());

        let frameLeftX:number = -(rectFrameWidth / 2) + (rectFrameHeight / 2);
        let frameLeftY:number = 0;
        let frameLeftZ:number = (- (rectFrameDepth) + rectFrameHeight) / 2;
        // 2. frame 좌측
        let rectFrameLeft:Box3DObject = new Box3DObject({
            x:frameLeftX,
            y:frameLeftY,
            z:frameLeftZ ,
            width:rectFrameHeight,
            height:rectFrameHeight,
            depth: - (rectFrameDepth) +(rectFrameHeight * 2 ) -0.2,
            edgesVisible:edgesVisible ,
            color:color,
            borderColor:borderColor
        });
        this._rectFrameGroup.add( rectFrameLeft.getGroup() );

        let frameRightX:number = (rectFrameWidth / 2) - (rectFrameHeight / 2);
        let frameRightY:number = 0;
        let frameRightZ:number = frameLeftZ;
        // // 2. frame 우측
        let rectFrameRight:Box3DObject = new Box3DObject({
            x:frameRightX,
            y:frameRightY,
            z:frameRightZ ,
            width:rectFrameHeight,
            height:rectFrameHeight,
            depth: -(rectFrameDepth)+ (rectFrameHeight * 2) -0.2 ,
            edgesVisible:edgesVisible ,
            color:color,
            borderColor:borderColor
        });
        this._rectFrameGroup.add(rectFrameRight.getGroup());

        let frameDownZ:number = frameLeftZ -(rectFrameDepth / 2) + (rectFrameHeight / 2) ;

        let frameDown:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:frameDownZ ,
            width:rectFrameWidth,
            height:rectFrameHeight,
            depth: rectFrameHeight ,
            edgesVisible:edgesVisible ,
            color:color,
            borderColor:borderColor
        });
        this._rectFrameGroup.add(frameDown.getGroup());

        if(rotateY != undefined){
            this._rectFrameGroup.rotateY( rotateY * Math.PI / 180);
        }
        let zposition:number = z + (rectFrameDepth / 2) - (rectFrameHeight/2)
        this._rectFrameGroup.position.set( x , zposition , y );
    }

    public getGroup():Group{
        return this._rectFrameGroup;
    }
}
