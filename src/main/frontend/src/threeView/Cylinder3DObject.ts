import {
    BoxGeometry,
    CanvasTexture,
    EdgesGeometry,
    Group,
    LineBasicMaterial,
    LineSegments,
    Mesh,
    MeshBasicMaterial,
    MeshLambertMaterial,
    MeshLambertMaterialParameters,
    PlaneBufferGeometry,
    RepeatWrapping,
    Texture,
    CylinderGeometry
} from 'three';
import {IBox3DObject, ITextObject, ICylinder3DObject} from "./I3DModel";

export default class Cylinder3DObject{

    private _axis:Group = new Group();

    constructor( parameter:ICylinder3DObject ){

        let color:number = 0x000000;

        if(parameter.color != null){
            color = parameter.color;
        }


        let cylinderGeometry:CylinderGeometry = new CylinderGeometry ( parameter.radiusTop , parameter.radiusBottom , parameter.depth , 12 , 1 , false, 0 , 6.3);
        this.addCylinder(cylinderGeometry , color );
        // if(parameter.edgesVisible == true){
        //     if(parameter.borderColor == null){
        //         parameter.borderColor = 0x000000;
        //     }
        //      this.addEdges(cylinderGeometry, 0, 0, 0, parameter.borderColor );
        // }
        
        if(parameter.rotateX != undefined){

            this._axis.rotateX( parameter.rotateX * Math.PI / 180);
        }

        this._axis.position.set( parameter.x , parameter.z , parameter.y );
    }

    public getGroup():Group{
        return this._axis;
    }

    private addCylinder(cylinderGeometry:CylinderGeometry , color:number ):void{
        let parame:MeshLambertMaterialParameters = {};
        parame.color = color;
        let meshMaterial:MeshLambertMaterial = new MeshLambertMaterial(parame);
        //, opacity:0.4 , transparent:true
        let leftBeltFrame:Mesh = new Mesh( cylinderGeometry , meshMaterial );
        leftBeltFrame.position.set ( 0,0,0 );
        this._axis.add(leftBeltFrame);
    }

    private addEdges( cylinderGeometry:CylinderGeometry , x:number, y:number, z:number,  color:number ):void{
        let edges:EdgesGeometry = new EdgesGeometry( cylinderGeometry );
        let line:LineSegments = new LineSegments( edges, new LineBasicMaterial( { color: color , linewidth:10 } ) );
        line.position.set ( x, y, z);
        this._axis.add(line);
    }
}
