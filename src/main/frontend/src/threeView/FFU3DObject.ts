import {CylinderBufferGeometry, Group, Mesh, MeshBasicMaterial, MeshBasicMaterialParameters} from 'three';
import Box3DObject from "./Box3DObject";
import Marking3DObject from "./Marking3DObject";
import {IFFU3dObject, MarkingType} from "./I3DModel";

export default class FFU3DObject{

    private _ffuGroup:Group = new Group();

    constructor( paramter:IFFU3dObject ){
        let frameWidth : number = 5;
        //x:number, y:number, z:number, ffuWidtdh:number , ffuHeight:number, ffuDepth:number, frameWidth:number =5 , rotationX?:number , rotationY?:number
        if(paramter.frameWidth != null){
            frameWidth=paramter.frameWidth;
        }

        let meshBasicMaterialParameters:MeshBasicMaterialParameters = {color:0x000000};
        if(paramter.opacity != null){
            meshBasicMaterialParameters.transparent = true;
            meshBasicMaterialParameters.opacity = paramter.opacity;
        }

        // 1.pan
        let cylinderPan:CylinderBufferGeometry = new CylinderBufferGeometry( (paramter.ffuWidtdh/5) , (paramter.ffuWidtdh/5) , frameWidth , 10 , 1 ,false,0 , 6.3);
        let material:MeshBasicMaterial = new MeshBasicMaterial( meshBasicMaterialParameters );
        let cylinderMesh:Mesh = new Mesh( cylinderPan, material );
        this._ffuGroup.add( cylinderMesh );

        let boxZ:number = -(paramter.ffuDepth / 2);
        let boxLineZ:number = 0;

        let down:Box3DObject = new Box3DObject( {
            x:0,
            y:0,
            z:boxZ ,
            width:paramter.ffuWidtdh,
            height:paramter.ffuHeight,
            depth:paramter.ffuDepth - frameWidth ,
            edgesVisible:false ,
            opacity: paramter.opacity,
            color:0x80ffff
        });
        this._ffuGroup.add(down.getGroup());

        let boxLine1:Box3DObject = new Box3DObject({
                x:(paramter.ffuWidtdh / 2) - frameWidth,
                y:0,
                z:boxLineZ ,
                width:frameWidth,
                height:paramter.ffuHeight,
                depth:frameWidth ,
                opacity: paramter.opacity,
                edgesVisible:false ,
                color:0x000000
        });
        this._ffuGroup.add(boxLine1.getGroup());

        let boxLine2:Box3DObject = new Box3DObject({
            x:-(paramter.ffuWidtdh / 2) + frameWidth,
            y:0,
            z:boxLineZ ,
            width:frameWidth,
            height:paramter.ffuHeight,
            depth:frameWidth ,
            opacity: paramter.opacity,
            edgesVisible:false ,
            color:0x000000
        });
        this._ffuGroup.add(boxLine2.getGroup());


        let checkWidth:number = (paramter.ffuWidtdh > 50) ? 50: paramter.ffuWidtdh;


        if(paramter.marking != null && paramter.marking.visible == true){

            let markingType:MarkingType = MarkingType.CHECK;
            if(paramter.marking.type != null){
                markingType = paramter.marking.type;
            }

            let check:Marking3DObject = new Marking3DObject({x:0,y:0,z:paramter.ffuDepth +(checkWidth / 2),IconWidth : checkWidth , IconFrame:5 , type:markingType});
            this._ffuGroup.add(check.getGroup());
        }


        const positionY:number = paramter.z + (paramter.ffuDepth / 2);

        if(paramter.rotationX !=  undefined){
            this._ffuGroup.rotation.x = -0.5 * Math.PI * (paramter.rotationX / 90);
        }
        if(paramter.rotationY != undefined) {
            this._ffuGroup.rotation.y = -0.5 * Math.PI * (paramter.rotationY / 90);
        }
        this._ffuGroup.position.set( paramter.x , positionY , paramter.y );
    }

    public getGroup():Group{
        return this._ffuGroup;
    }
}
