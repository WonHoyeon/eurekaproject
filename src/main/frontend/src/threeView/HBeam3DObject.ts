import {Group} from 'three';
import Box3DObject from "./Box3DObject";
import Windows3DObject from "./Windows3DObject";

export default class HBeam3DObject{

    private _hBeamGroup:Group = new Group();

    constructor( x:number, y:number, z:number, hbeamWidth:number , hbeamHeight:number, hbeamDepth:number, rotationX?:number , rotationY?:number , color:number=0x006766 ,borderColor:number=0x000000 ){

        // 1.well 하단
        let hBeamTopDownDepth:number = (hbeamDepth / 6);

        let hBeamTop:Box3DObject = new Box3DObject({
                x:0,
                y:0,
                z:0,
                width:hbeamWidth,
                height:hbeamHeight,
                depth:hBeamTopDownDepth,
                edgesVisible:true,
                color:color,
                borderColor:borderColor
        });
        this._hBeamGroup.add(hBeamTop.getGroup());

        // 2.well Center
        let hBeamCenterDepth:number = (hBeamTopDownDepth * 4);
        let hBeamCenterZ:number = -(hBeamCenterDepth / 2) - ( hBeamTopDownDepth / 2) ;

        let hBeamCenter:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:hBeamCenterZ,
            width:hbeamWidth,
            height:(hbeamHeight /5),
            depth:hBeamCenterDepth,
            edgesVisible:true,
            color:color,
            borderColor:borderColor
        });
        this._hBeamGroup.add(hBeamCenter.getGroup());

        // 3.well Down
        let hBeamDownZ:number = - (hBeamCenterDepth) - ( hBeamTopDownDepth / 2) ;

        let hBeamDown:Box3DObject = new Box3DObject({
            x:0,
            y:0,
            z:hBeamDownZ,
            width:hbeamWidth,
            height:hbeamHeight,
            depth:hBeamTopDownDepth,
            edgesVisible:true,
            color:color,
            borderColor:borderColor
        });
        this._hBeamGroup.add(hBeamDown.getGroup());

        const positionY:number = z + (hbeamDepth / 2) - hBeamTopDownDepth;

        if(rotationX !=  undefined){
            this._hBeamGroup.rotation.x = -0.5 * Math.PI * (rotationX / 90);
        }
        if(rotationY != undefined) {
            this._hBeamGroup.rotation.y = -0.5 * Math.PI * (rotationY / 90);
        }
        this._hBeamGroup.position.set( x , positionY , y );
    }

    public getGroup():Group{
        return this._hBeamGroup;
    }
}
