import React, {Component} from 'react';
import {
    AnimationAction,
    AnimationMixer,
    Camera,
    Clock,
    Group,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Scene,
    Texture,
    TextureLoader,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import Stats from 'stats.js';
import Box3DObject from "./Box3DObject";
import FFU3DObject from "./FFU3DObject";
import {IFactoryModelAttrData, IProcessMapInfo, IProcessModelInfo} from "./IProcessModelMain";
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import Marking3DObject from "./Marking3DObject";
import {IBox3DObject, MarkingType} from "./I3DModel";
import Barricades3DObject from "./Barricades3DObject";
import Model3DObject from "./Model3DObject";
import * as createjs from "createjs-module";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import ScissorLift3DObject from "./ScissorLift3DObject";
import {IHseAnimationInfo} from "./IHseModelMain";
import {FBXLoader} from "three/examples/jsm/loaders/FBXLoader";

@inject('resultViewStore', 'processModelStore')
@observer
class HSEProcessModel extends Component<any, any> {
    private _statePerson:'caret-right'|'pause'='caret-right';
    private personAction:Array<any>=[];
    /** @description  movieClip과 연결된 타임라인, 인스턴스에 전달하기위함 (일시중지, 되감기등 지원) */
    private _timeline: createjs.Timeline = new createjs.Timeline([], {}, {paused: true});
    // 시간
    private _clock: Array<Clock> = [];
    // 애니메이션 플레이어
    private _workerAnimationList: Array<AnimationMixer> = [];

    /** @description DOM에 직접접근을 위한 HTMLElement 변수 선언 */
    private _mount: HTMLElement | null = null;
    private _scene: Scene | null = null;
    private camera: Camera | null = null;

    /** @description 정의한 맵, 모델을 렌더하기위한 변수 */
    private renderer: WebGLRenderer | null = null;
    /** @description 귀도조정을 위한 컨트롤 변수선언 */
    private controls: OrbitControls | null = null;
    /** @description 에니메이션 프레임 아이디 선언변수 */
    private frameId: number = 0;
    private modelItem: Object3D | null = null;
    /** @description panel 에대한 변수선언(웹사이트 성능 모니터링 북마클릿) */
    private stats: Stats = new Stats();
    private _textBox: Array<Box3DObject> = [];
    // private _mapData:Array<IProcessMapInfo> = [];
    // private _modelListData:Array<IProcessModelInfo> = [];
    // private _modelAttrData:{ [key:string]:{ [key:string]:string } } = {};
    // private _hseAnimationData:{ [index:string] : Array<IHseAnimationInfo> } = {};


    @autobind
    private setModel(model: Object3D) {
        this.modelItem = model;
    }


    // ============================================================================================================================================================================
    /**
     * @description 모델 가지고 와서 set 하는 메서드
     * @param scene 모델과 관련된 정보(scene에 추가한 무대(광원 등))
     */
    private async getData(scene: Scene) {
        const {processModelStore} = this.props;
        await processModelStore.HSEModelsetData();
        const attrListData: Array<IFactoryModelAttrData> = processModelStore.getModelAttrData;

        let positionZ: number = 0;
        let mapObject: any = [];

        // Map Image 로딩 및 배치(_mapData)
        for (let i: number = 0; i < processModelStore.getMapData.length; i++) {
            let item: IProcessMapInfo = processModelStore.getMapData[i];
            // let nextItem:IProcessMapInfo | null = this._mapData[i];
            // if(nextItem != null){
            //     positionZ = positionZ + nextItem.mapRangeLenz;
            // }

            //맵의 그리트타일 규격 100 / 100
            let gridTile: { width: number, height: number } = {width: item.mapGridx, height: item.mapGridy};
            // map x 범위 = 2000
            let gridwidth: number = item.mapRangeLenx;
            // map y 범위 = 2000
            let gridheight: number = item.mapRangeLeny;

            //그리드 지형  2000, 2000, 2000/100 = 20, 2000/100 = 20
            let gridGeometry: PlaneGeometry = new PlaneGeometry(gridwidth, gridheight, gridwidth / gridTile.width, gridheight / gridTile.height);
            //MeshBasicMaterial -> 그리기 재료
            let gridMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0x000000, wireframe: true});
            //삼각 폴리쉬 기반으로 그리드를 표현
            let grid: Mesh = new Mesh(gridGeometry, gridMaterial);
            //데이터에 각각 0, 400, 800값이 있으므로 반복문에의하여 3개의 층이 생성
            grid.position.set(0, item.mapRangeLenz, 0);

            // 맵아이디를 index값으로 하여 mapObject에 넣는다 (z : 0)
            mapObject[item.mapId] = {
                mapRangeLenx: item.mapRangeLenx,
                mapRangeLeny: item.mapRangeLeny,
                mapRangeLenz: item.mapRangeLenz,
                z: positionZ,
            };
            grid.rotation.x = -0.5 * Math.PI;
            scene.add(grid);
            //그리드 지형  2000, 2000, 2000/100 = 20, 2000/100 = 20
            // 추가
            let mapGeometry: PlaneGeometry = new PlaneGeometry(gridwidth, gridheight, gridwidth / gridTile.width, gridheight / gridTile.height);
            let mapMaterial: MeshBasicMaterial = new MeshBasicMaterial({
                color: 0xFFFFFF,
                transparent: true,
                opacity: 0.5,
            });
            let map: Mesh = new Mesh(mapGeometry, mapMaterial);
            map.position.set(0, positionZ, 0);
            map.rotation.x = -0.5 * Math.PI;
            scene.add(map);
        }


        /**
         * @description attrListData 값을 받아온것이 있다면..(상단에서 getData()로 받은것을 알수 있다.
         */
        if (attrListData != undefined) {
            attrListData.forEach(function (this: HSEProcessModel, item: IFactoryModelAttrData) {
                // 스텐스아이디에 값이 없으면
                if (processModelStore.getModelAttrData[item.stencId] == null) {
                    processModelStore.getModelAttrData[item.stencId] = {};
                }
                //이중배열로 스탠스 아이디, 속성키(를 넣어준다.)
                processModelStore.getModelAttrData[item.stencId][item.attrkey] = item.attrval;
            }, this);
        }

        const attrData: { [key: string]: { [key: string]: string } } = processModelStore.getModelAttrData;

        //stence정보가 들어있다. (stence가 객체를 말하는거같은데 정확히 모르겠음)
        for (let i: number = 0; i < processModelStore.getModelListData.length; i++) {
            let item2: IProcessModelInfo = processModelStore.getModelListData[i];
            // mapData와 ModelListData의 속성중 mapId가 서로 연관되어있다. (mapData의 속성 mapId, modelListData의 mapId)
            const mapInfo: {
                mapRangeLenx: number,
                mapRangeLeny: number,
                mapRangeLenz: number,
                z: number,
            } = mapObject[item2.mapId];

            // mapData와 ModelListData의 속성으로 포지션 선정
            let positionX: number = -(mapInfo.mapRangeLenx / 2) + item2.stencPosx + (item2.stencLenx / 2);
            let positionY: number = -(mapInfo.mapRangeLeny / 2) + item2.stencPosy + (item2.stencLeny / 2);
            let positionZ: number = mapInfo.mapRangeLenz + item2.stencPosz + (item2.stencLenz / 2);


            //포문을 돌리면서 해당되는 case가 있으면 출력
            // item2는 modelListData// stenkey( ex)"AirConditioner")
            //각각의 맞는 객체생성위치를 찾아 포지션설정 ----->scene에 추가
            switch (item2.stenckey) {
                case "ShapeWall":
                    const fillColor: number = (item2.fillColor != null) ? parseInt('0x' + item2.fillColor.toString(), 16) : 0xFFFFFF;
                    let box3DObject: Box3DObject = new Box3DObject({
                        x: positionX,
                        y: positionY,
                        z: positionZ,
                        width: item2.stencLenx,
                        height: item2.stencLeny,
                        depth: item2.stencLenz,
                        edgesVisible: false,
                        color: fillColor
                    });
                    scene.add(box3DObject.getGroup());
                    break;
                case "AirConditioner":
                    let ffu3DObject: FFU3DObject = new FFU3DObject({
                        x: positionX,
                        y: positionY,
                        z: positionZ,
                        ffuWidtdh: item2.stencLenx,
                        ffuHeight: item2.stencLeny,
                        ffuDepth: item2.stencLenz,
                        opacity: item2.opacity
                    });
                    scene.add(ffu3DObject.getGroup());
                    break;

                case "ShapeDangerousArea":
                    let alert2: Marking3DObject = new Marking3DObject({
                        x: positionX,
                        y: positionY,
                        z: positionZ + 100,
                        IconWidth: 100,
                        IconFrame: 5,
                        type: MarkingType.ALERT,
                    });
                    scene.add(alert2.getGroup());

                    let barricadesLeft: Barricades3DObject = new Barricades3DObject({
                        x: positionX - (item2.stencLenx / 2),
                        y: positionY - 2.5,
                        z: positionZ,
                        width: item2.stencLenx - 10,
                        frameHeight: 10,
                        depth: item2.stencLenz,
                        edgesVisible: true,
                        color: 0xf6d21d,
                        rotateY: 90
                    });
                    scene.add(barricadesLeft.getGroup());

                    let barricadesRight: Barricades3DObject = new Barricades3DObject({
                        x: positionX + (item2.stencLenx / 2),
                        y: positionY - 2.5,
                        z: positionZ,
                        width: item2.stencLenx - 10,
                        frameHeight: 10,
                        depth: item2.stencLenz,
                        edgesVisible: true,
                        color: 0xf6d21d,
                        rotateY: 90
                    });
                    scene.add(barricadesRight.getGroup());

                    let barricadesTop: Barricades3DObject = new Barricades3DObject({
                        x: positionX,
                        y: positionY - (item2.stencLeny / 2),
                        z: positionZ,
                        width: item2.stencLenx - 10,
                        frameHeight: 10,
                        depth: item2.stencLenz,
                        edgesVisible: false,
                        color: 0xf6d21d
                    });
                    scene.add(barricadesTop.getGroup());

                    let barricadesButtom: Barricades3DObject = new Barricades3DObject({
                        x: positionX,
                        y: positionY + (item2.stencLeny / 2) - 5,
                        z: positionZ,
                        width: item2.stencLenx - 10,
                        frameHeight: 10,
                        depth: item2.stencLenz,
                        edgesVisible: true,
                        color: 0xf6d21d
                    });
                    scene.add(barricadesButtom.getGroup());
                    break;


                case "ShapeCompressedGasArea":

                    let loadTextURL: string = require('../textures/sample.jpg');
                    let loader: TextureLoader = new TextureLoader();
                    let parameter: IBox3DObject = {
                        x: positionX,
                        y: positionY,
                        z: positionZ,
                        width: item2.stencLenx,
                        height: item2.stencLeny,
                        depth: item2.stencLenz,
                        edgesVisible: false,
                        color: 0xFFFFFF,
                        opacity: item2.opacity,
                    };

                    loader.load(
                        loadTextURL,
                        function (texture: Texture): void {
                            let parameter2: IBox3DObject = Object.assign(parameter, {});
                            parameter2.texture = texture;
                            parameter2.color = 0xf6bb43;
                            let area: Box3DObject = new Box3DObject(parameter2);
                            scene.add(area.getGroup());
                        },
                        undefined,
                        // onError callback
                        function (event: ErrorEvent): void {
                            let area: Box3DObject = new Box3DObject(parameter);
                            scene.add(area.getGroup());
                        }
                    );

                    let alert: Marking3DObject = new Marking3DObject({
                        x: positionX,
                        y: positionY,
                        z: positionZ + 100,
                        IconWidth: 100,
                        IconFrame: 5,
                        type: MarkingType.ALERT,
                    });
                    scene.add(alert.getGroup());
                    break;


                //사다리차
                case "Lift":
                    // attrData[item2.stencId]에는
                    // example -->
                    // Capacity: "2"
                    // Height: "6"
                    // ID: "0"
                    // IsRiding: "1"
                    // Length: "2"
                    // ProcessName: "Scissor Lift"
                    // ScissorHeight: "200"
                    // Type: "0"
                    // Velocity: "0.8"
                    // Width: "1.5"

                    const attr: { [key: string]: string } = attrData[item2.stencId];
                    let isRiding: boolean = false;

                    // isriding은 사다리차 위에 탄사람을 표현할 것인지 아니할 것인지 나타내기 위함
                    if (attr["IsRiding"] != undefined && attr["IsRiding"].toString() == "1") {
                        isRiding = true
                    }

                    let left3DObject: ScissorLift3DObject = new ScissorLift3DObject({
                        text:'지게차',
                        x: positionX,
                        y: positionY,
                        z: positionZ,
                        width: item2.stencLenx,
                        height: item2.stencLeny,
                        depth: item2.stencLenz,
                        rotationY: item2.rotAngle,
                        IsRiding: isRiding,
                    });

                    // 생성한 사다리 객체를 scene(무대)에 추가
                    scene.add(left3DObject.getGroup());

                    //사다리 지게의 높이를 가져와 형변환후 사이즈set을 해준다.
                    left3DObject.setScissorSize(parseInt(attr["ScissorHeight"]));

                    this.shapeLift(left3DObject , processModelStore.getHseAnimationData[item2.stencId] , item2 , mapInfo);
                    // this.setAnimation( "scissorLift", left3DObject , processModelStore.getHseAnimationData[item2.stencId] , item2 , mapInfo);

                    break;

                //걷는사람(fbx)
                case "ShapeWorker":
                    //fbx url
                    // let glbURL:string = "/dexi-web/sim/getSimulationModelFile.do?stenc3did="+item2.stencId;
                    // modelListData를 담은값은 item2입니다.
                    let fbxURL: string = require("../model/test.fbx");
                    let fbxLoader: FBXLoader = new FBXLoader();
                    let modelLoadFunction: (object: Group) => void = (object: any) => {
                        let obj: Group = object;
                        obj.position.x = positionX;
                        obj.position.y = positionZ;
                        obj.position.z = positionY;
                        obj.scale.set(item2.stencLenx, item2.stencLeny, item2.stencLenz);
                        obj.rotation.y = -0.5 * Math.PI;
                        if (item2.rotAngle != null) {
                            obj.rotateY(1.57 * (item2.rotAngle / 90));
                        }
                        scene.add(obj);

                        //애니메이션 플레이서 Mixer장치
                        const mixer: AnimationMixer = new AnimationMixer(object);
                        //시간적용
                        const clock: Clock = new Clock();
                        this._workerAnimationList.push(mixer);
                        this._clock.push(clock);
                        let action:AnimationAction = mixer.clipAction(object.animations[0]);

                        //플레이어 중지시 fbx 플레이어 파일을 중지하기 위함.
                        this.personAction.push(action);
                        action.play();


                        //애니메이션 관련 정보가 존재한다면
                        if (processModelStore.getHseAnimationData[item2.stencId] != undefined) {
                            if (processModelStore.getHseAnimationData[item2.stencId].length > 1) {
                                //obj = 걷는사람(fbx파일), 애니메이션 데이터정보 , 모델리스트정보, 맵정보 를 넘겨준다
                                this.shapeWorker(obj, processModelStore.getHseAnimationData[item2.stencId] , item2 , mapInfo, action);
                            }
                        }

                    };
                    fbxLoader.load(fbxURL, modelLoadFunction, undefined, function (e: any) {
                        console.log("error");
                    });
                    break;


                // ModelListData에 stenckey가 default값이면...
                default :
                    let modelGlbURL: string = "/dexi-web/sim/getSimulationModelFile.do?stenc3did=" + item2.stenc3did;
                    let modelLoader: GLTFLoader = new GLTFLoader();
                    modelLoader.load(modelGlbURL, function (object: GLTF) {
                            let object3dModel: Object3D = object.scene.children[0];
                            let object3D: Model3DObject = new Model3DObject();
                            let modelItem: IProcessModelInfo = {
                                stencId: item2.stencId,
                                stenckey: item2.stenckey,
                                stencNm: item2.stencNm,
                                stencLenx: item2.stencLenx,
                                stencLeny: item2.stencLeny,
                                stencLenz: item2.stencLenz,
                                stencPosx: item2.stencPosx,
                                stencPosy: item2.stencPosy,
                                stencPosz: item2.stencPosz,
                                zIndex: 0,
                                mapId: "",
                                modelScale: 1,
                                rotAngle: 0,
                                stenc3dTp: "",
                                stenc3did: item2.stenc3did,
                                stenc3dinfoid: "",
                                fillColor: 0x000000,
                                lineColor: 0x000000,
                            };

                            object3D.init({
                                obj: object3dModel,
                                modelItem: modelItem,
                                mapRangeLenx: mapInfo.mapRangeLenx,
                                mapRangeLeny: mapInfo.mapRangeLeny
                            });
                            scene.add(object3D.getGroup());
                        }, undefined,
                        function (event: ErrorEvent): void {
                            let box3DObject: Box3DObject = new Box3DObject({
                                x: positionX,
                                y: positionY,
                                z: positionZ,
                                width: item2.stencLenx,
                                height: item2.stencLeny,
                                depth: item2.stencLenz,
                                edgesVisible: false,
                                color: 0xFFFF00
                            });
                            scene.add(box3DObject.getGroup());
                        });
                    break;
            }
        }
    }

    /**
     * @param humenWorker
     * @param animationListData 부모메서드에서 포문으로 하나씩 애니메이션리스트를 받는다.
     * @param item 3d 모델 정보를 담은 modelListData
     * @param mapInfo 맵 데이터
     */
    @autobind
    private shapeWorker(humenWorker: any, animationListData: Array<IHseAnimationInfo>, item: IProcessModelInfo, mapInfo: { mapRangeLenx: number, mapRangeLeny: number, mapRangeLenz: number, z: number },action:any){;
        //Tween 객체에 인간의 포지션 위치 주입( 반복문으로 모든 인간위치 받음)
        //객체 생성된 지정 위치에서시작( 따로 스타트포인트 x)

        let positionX: number = -(mapInfo.mapRangeLenx / 2) + animationListData[0].x + (item.stencLenx / 2);
        let positionY: number = -(mapInfo.mapRangeLeny / 2) + animationListData[0].y + (item.stencLeny / 2);
        let positionZ: number = mapInfo.mapRangeLenz + animationListData[0].z + (item.stencLenz / 2);


        //Tween객체를 만들시 첫 시퀀스의 position정보
        let setPosition:any = {
            x:positionX,
            y:positionY,
            z:positionZ,
            rotationY:animationListData[0].rotationY,
            rotationZ:animationListData[0].rotationZ,
        };

        const model = humenWorker;
        const modelItem: IProcessModelInfo = item;
        let cubeTween: createjs.Tween = new createjs.Tween(setPosition);



        /**
         * @description animationListData를 하나씩 받으면서 stencId에
         * 대한 좌표정보(여기서는 fbx 사람파일)에대한 모든것을 forEach로 받는다.
         */
        animationListData.forEach(function (item: IHseAnimationInfo) {
            let positionX: number = -(mapInfo.mapRangeLenx / 2) + item.x + (modelItem.stencLenx / 2);
            let positionY: number = -(mapInfo.mapRangeLeny / 2) + item.y + (modelItem.stencLeny / 2);
            let positionZ: number = mapInfo.mapRangeLenz + item.z + (modelItem.stencLenz / 2);

            //생성한 Tween 객체를 이동시킬 postion, 및 이동기간(duration).
            cubeTween.to({
                x: positionX,
                y: positionZ,
                z: positionY,
            },item.time/50)
                .to({
                x: positionX,
                y: positionZ,
                z: positionY,
                rotateY : item.rotationY
            },0)
                .to({
                    x: positionX,
                    y: positionZ,
                    z: positionY,
                    rotateY : item.rotationY,
                    rotateZ : item.rotationZ,
                },0);


            //트윈 객체가 변화시마다 데이터, 로그를 확인 및 설정하는 메서드
            cubeTween.on("change", function(event:any) {
                let x : number = event.target.target.x;
                let y : number = event.target.target.z;
                let z : number = event.target.target.y;
                let rotateY : number = event.target.target.rotateY;
                let rotateZ : number = event.target.target.rotateZ;


                model.position.x = x;
                model.position.z = y;
                model.position.y = z;
                // model.rotation.y =
            });
        });
        this._timeline.addTween(cubeTween);
    }

    /**
     * @description 사다리 지게차를 animation화 시키는 구간.
     */
    @autobind
    private shapeLift(shapeLifter: any, animationListData: Array<IHseAnimationInfo>, item: IProcessModelInfo, mapInfo: { mapRangeLenx: number, mapRangeLeny: number, mapRangeLenz: number, z: number }) {
        // console.log('*****************************************************');
        //Tween 객체에 인간의 포지션 위치 주입( 반복문으로 모든 인간위치 받음)
        //객체 생성된 지정 위치에서시작( 따로 스타트포인트 x)

        // let positionX: number = -(mapInfo.mapRangeLenx / 2) + animationListData[0].x + (item.stencLenx / 2);
        // let positionY: number = -(mapInfo.mapRangeLeny / 2) + animationListData[0].y + (item.stencLeny / 2);
        // let positionZ: number = mapInfo.mapRangeLenz + animationListData[0].z + (item.stencLenz / 2);

        const modelItem: IProcessModelInfo = item;

        let positionX: number = -(mapInfo.mapRangeLenx / 2) + animationListData[0].x + (modelItem.stencLenx / 2);
        let positionY: number = -(mapInfo.mapRangeLeny / 2) + animationListData[0].y + (modelItem.stencLeny / 2);
        let positionZ: number = mapInfo.mapRangeLenz + animationListData[0].z + (modelItem.stencLenz / 2);

        let setPosition:any = {
            x:positionX,
            y:positionY,
            z:positionZ,
            scissorSize:animationListData[0].scissorSize,
            rotateY:animationListData[0].rotationY,
            // rotationZ:animationListData[0].rotationZ,
            // rotationZ:animationListData[0].rotationZ,
            // completeFunction:undefined
        };
        let cubeTween: createjs.Tween = new createjs.Tween(setPosition);

        /**
         * @description animationListData를 하나씩 받으면서 stencId에
         * 대한 좌표정보(여기서는 fbx 사람파일)에대한 모든것을 forEach로 받는다.
         */
        animationListData.forEach(function (item: IHseAnimationInfo) {
            const model:ScissorLift3DObject = shapeLifter;
            let positionX: number = -(mapInfo.mapRangeLenx / 2) + item.x + (modelItem.stencLenx / 2);
            let positionY: number = -(mapInfo.mapRangeLeny / 2) + item.y + (modelItem.stencLeny / 2);
            let positionZ: number = mapInfo.mapRangeLenz + item.z + (modelItem.stencLenz / 2);
            cubeTween.to({
                x:positionX,
                y:positionY,
                z:positionZ,
                scissorSize:0
            },  item.time / 50);
            cubeTween.to({
                x:positionX,
                y:positionY,
                z:positionZ,
                rotateY: item.rotationY,
                scissorSize:item.scissorSize
            },item.time / 50);
            cubeTween.to({
                x:positionX,
                y:positionY,
                z:positionZ,
                rotateY: item.rotationY,
            },0);

            cubeTween.on("change", function(event:any) {
                let x : number = event.target.target.x;
                let y : number = event.target.target.y;
                let z : number = event.target.target.z;
                let rotateY : number = event.target.target.rotateY;
                let scissorSize : number = event.target.target.scissorSize;

                if(event.currentTarget.target.rotateY != undefined) {
                    // console.log('Test***',event.currentTarget.target.rotateY);
                    // console.log('Test2',rotateY);
                    model.getGroup().rotation.y =  -Math.PI * (rotateY / 180);
                }

                if (event.currentTarget.target.scissorSize != undefined) {
                    // console.log('Test***',event.currentTarget.target.scissorSize);
                    // console.log('Test2',scissorSize);
                    model.setScissorSize(scissorSize);
                }

                model.getGroup().position.x = x;
                model.getGroup().position.z = y;
                model.getGroup().position.y = z;


                //&& object.rotationY > 0
                // if(event.currentTarget.target.rotationY != undefined ){
                    // model.getGroup().rotation.y = - Math.PI * (event.target.target.rotationY / 180);
                    // model.getGroup().rotation.y = - Math.PI * (event.target.target.rotationY / 180);
                // }
                // if(event.currentTarget.target.rotationZ != undefined ){
                //     model.getGroup().rotation.z = - Math.PI * (event.target.target.rotationZ / 180);
                // }

                //&& object.scissorSize > 0
                // if(event.currentTarget.target.scissorSize != undefined  ){
                //     model.setScissorSize(event.target.target.scissorSize);
                // }

            });
        });
        this._timeline.addTween(cubeTween);

    }

    /** @description 컴포넌트가 열리면서 시작됨 */
    componentDidMount() {
        this.load3DModelInfo();
    }



    /**
     * @description HTML 너비 높이, 카메라, 무대(Scene), webGL렌더러를 설정
     * 이에 모델과 관련된 정보(scene에 추가한 무대(광원 등) ) getData(scene)에 파라미터로 넘겨준다.
     */
    load3DModelInfo() {
        //HTML엘리멘트 요소가 없으면 그냥 리턴(요소접근을 위해 필요)
        if (this._mount === null) {
            return;
        }
        //엘리먼트의 넓이값 꾸미기
        let width: number = this._mount.clientWidth - 28;
        //엘리먼트의 높이값 꾸미기
        let height: number = this._mount.clientHeight - 10;
        // 무대 만들기
        const scene: Scene = new Scene();

        // 카메라 종횡비 설정
        const camera: Camera = new PerspectiveCamera(
            60,// 시야
            width / height, // 종횡비
            1, // 어디 부터 (0.1)
            10000  // 어디 까지 1000
        );

        // 화면 엘리먼트에서 구한 너비 높이 값을 렌더길이로 set, colorset
        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(0xcccccc, 1);

        //라이트
        // 하늘에서 -> 지상으로 희미해지는 색상의 그라데이션 광원
        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);
        //광원 지점 set
        light.position.set(0, 200, 0);
        //scene 무대에 light 빛설정 한 정의추가
        scene.add(light);


        // 카메라 컨트롤
        this.controls = new OrbitControls(camera, renderer.domElement);
        // 컨트롤에 무게감을주는 데 사용할 수있는 댐핑
        this.controls.enableDamping = true;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 3000;
        this.controls.maxPolarAngle = Math.PI * 2;

        this.getData(scene);

//
        // 카메라 위치 설정
        camera.position.set(0, 1500, 0);

        this._scene = scene;
        this.camera = camera;
        this.renderer = renderer;

        this._mount.appendChild(this.renderer.domElement);

        this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom

        let stateHtml: HTMLElement = this.stats.dom;
        stateHtml.setAttribute("style", "position: absolute; top: 60px; cursor: pointer; opacity: 0.9; z-index: 1000;");
        this._mount.appendChild(stateHtml);

        this.start();
    }

    @autobind
    start(): void {
        if (this.frameId == 0) {
            this.frameId = requestAnimationFrame(this.animate);
        }
    }

    animate = (time: number) => {

        //북마크 시작
        this.stats.begin();
        //애니메이션 업데이트
        // this._animationTween.update(time);

        //반복문을 돌려서 다음 프레임(1000ms)를 계속 돌린다.
        if (this._workerAnimationList.length > 0) {
            for (let i: number = 0; i < this._workerAnimationList.length; i++) {
                // this.workerAnimationList[i].update(time);
                this._workerAnimationList[i].update(this._clock[i].getDelta());
            }
        }

        if (this.modelItem != null) {

        }


        // this._cameraRotationZ = this.camera.rotation.z;
        // this._cameraRotationY = this.camera.rotation.y;
        // this._stripMesh.rotation.x = this.camera.rotation.x;
        // this._stripMesh.rotation.z = this.camera.rotation.y;

        if (this.camera != null) {
            for (let item of this._textBox) {}
            // item.setRotation(this.camera.rotation.y);
            //, this.camera.rotation.x

        }
        // for(let item of this.flowTextureList){
        //     item.offset.x -=(0.01 * 3 % 1);
        // }

        //북마크 종료
        // this.stats.end();
        //화면렌더
        this.renderScene();
        //해당 애니메이션의 수행에대한 ID를 변수에 저장
        this.frameId = window.requestAnimationFrame(this.animate)
    };


    componentWillUnmount() {
        if (this.renderer == null || this._mount == null) {
            return;
        }
        this.stop();
        this._mount.removeChild(this.renderer.domElement);
    }


    @autobind
    stop() {
        // 스케줄된 애니메이션 프레임 요청을 취소
        cancelAnimationFrame(this.frameId)
    };

    /**
     * @description 현재의 카메라, Scene에 대해 (각 this변수에 담은..) 렌더를 하기 위한 함수
     */
    @autobind
    renderScene() {
        //null이면 아무것도 아닌걸로
        if (this.renderer == null || this._scene == null || this.camera == null) {
            return;
        }
        //컨트롤 움직임에 대한 (orbitControll) 변환 update
        if (this.controls != null) {
            this.controls.update();
        }
        // 저장한 scene과 camera를 렌더한다
        this.renderer.render(this._scene, this.camera);
    }

    /**
     * @description HSEFactory.tsx의 ref를 이용하여 set 할것이다.
     * @param time 시간이동뒤의 set할 시간
     */
    @autobind
    public change(time: number) {
        this._timeline.setPosition(time);
    };

    /**
     * @description 타임라인 버튼이 Stop 일때 사람 fbx파일 행동중지
     * @param playState 버튼 중지
     */
    @autobind
    public personStop(playState:'caret-right'|'pause'){
        const dataList = this.personAction;
        // HSEFactory에서의 playState상태가 다른지 정확이 이해못함
        if(playState === 'caret-right'){
            this._statePerson = 'pause';
            for(let x=0; dataList.length > x; x++) {
                dataList[x].stop();
            }
            // this.personAction.stop();
        } else {
            this._statePerson = 'caret-right';
            for(let x=0; dataList.length > x; x++) {
                dataList[x].play();
            }
        }
    };
// ======================================================== 기준점 ======================================================
    render() {
        return (
            <div style={{width: "100%", height: "100%"}}
                // 클래스형식에서 React.createRef()를 통해 생성
                // 컴포넌트의 인스턴스의 어느 곳에서도 Ref에 접근할 수 있다.
                 ref={(mount) => {
                     if (mount === null) {
                         return;
                     }
                     this._mount = mount
                 }}/>
        );
    }
}

export default HSEProcessModel;