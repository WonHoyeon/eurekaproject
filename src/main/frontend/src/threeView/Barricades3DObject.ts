import {CylinderGeometry, Group, Mesh, MeshBasicMaterial} from 'three';
import Box3DObject from "./Box3DObject";
import {IBarricades3DObject} from "./I3DModel";
import ReactFrame3DObject from "./ReactFrame3DObject";


export default class Barricades3DObject{

    private _axis:Group = new Group();
    private readonly _supportSize:number = 10;
    private _barSize:number = 0;
    constructor( parameter:IBarricades3DObject ){
        this._barSize = (parameter.frameHeight / 2);
        this.addFrame(parameter);

        const rightSupport:IBarricades3DObject  = Object.assign({} , parameter);
        rightSupport.x = -(parameter.width / 2) + (parameter.frameHeight / 2);
        rightSupport.y =0;
        rightSupport.z = -(parameter.depth / 2) +(parameter.frameHeight / 2);
        this.addSupport(rightSupport);


        const leftSupport:IBarricades3DObject  = Object.assign({} , parameter);
        leftSupport.x =(parameter.width / 2) - (parameter.frameHeight / 2) ,
        leftSupport.y = 0;
        leftSupport.z = -(parameter.depth / 2) +(parameter.frameHeight / 2);
        this.addSupport(leftSupport);

        if(parameter.rotateY != undefined){
            this._axis.rotation.y = 1.57 * (parameter.rotateY / 90);
        }
        this._axis.position.set( parameter.x ,  parameter.z , parameter.y);
    }

    public getGroup():Group{
        return this._axis;
    }

    private addFrame( parameter:IBarricades3DObject ){
        let rectFrame:ReactFrame3DObject = new ReactFrame3DObject(
            0,
            0,
            0,
            parameter.width,
            parameter.frameHeight,
            parameter.depth - this._supportSize,
            true,
            parameter.color
        );
        this._axis.add(rectFrame.getGroup());
        let startX:number =  -(parameter.width / 2) + (parameter.frameHeight) ;
        for(let i=0;i<((parameter.width - parameter.frameHeight * 2) / parameter.frameHeight) ; i++){
            startX+=parameter.frameHeight;

            let bar:Box3DObject = new Box3DObject({
                x:startX ,
                y:0,
                z:0, // -(parameter.depth / 2) + parameter.frameHeight
                width: this._barSize ,
                height: this._barSize ,
                depth:parameter.depth - this._supportSize - parameter.frameHeight ,
                edgesVisible:false,
                color:parameter.color
            });
            this._axis.add(bar.getGroup());
        }
    }

    private addSupport( parameter:IBarricades3DObject ){

        let supportbarDown:Box3DObject = new Box3DObject({
            x:parameter.x ,
            y:0,
            z:parameter.z ,
            width:parameter.frameHeight,
            height:this._supportSize,
            depth:parameter.frameHeight,
            edgesVisible:false,
            color:parameter.color
        });
        this._axis.add(supportbarDown.getGroup());
    }
}
