import {CylinderGeometry, Group, Mesh, MeshBasicMaterial, Matrix4, Vector3, ConeGeometry, SphereBufferGeometry,
    EdgesGeometry,
    LineSegments,
    LineBasicMaterial,
    RingBufferGeometry,
    TextureLoader,
    Texture,
    MeshBasicMaterialParameters,
    CanvasTexture} from 'three';
import Box3DObject from "./Box3DObject";
import FlowText3DObject from "./FlowText3DObject";

export default class Line3DObject{

    private _axis:Group = new Group();
    private _lineGroup:Group = new Group();
    private _textureList:Array<CanvasTexture> = [];
    private _textureMeshList:Array<Mesh> = [];

    constructor( positionList:Array<{x:number, y:number, z:number}>, lineWeight:number , color :number = 0xFFFFFF , opacity:number = 1, flowVisible:boolean = false){

        if(positionList.length == 0){
            console.error("position data is null");
            return;
        }

        if(positionList.length == 1){
            let positionItem:{x:number, y:number, z:number} = Object.assign({} , positionList[0]);
            positionList.push(positionItem);
        }

        this.addLineList( positionList , lineWeight , color , opacity , flowVisible);

    }

    public getGroup():Group{
        return this._axis;
    }

    public getTextureList():Array<CanvasTexture>{
        return this._textureList;
    }

    public getTextureMeshList():Array<Mesh>{
        return this._textureMeshList;
    }

    private addFlowTexture( startPoint:{x:number, y:number, z:number} , endPoint:{x:number, y:number, z:number} , xSin:number , zSin:number , lineWeight:number , wheight:number ){

        let animationType:"flow" | "reverse"="flow";

        if(startPoint.x > endPoint.x ){
            animationType = "reverse";
        }

        if(startPoint.y < endPoint.y ){
            animationType = "reverse";
        }

        if(startPoint.z > endPoint.z ){
            animationType = "reverse";
        }

        //→⇒≫▶
        let text:FlowText3DObject = new FlowText3DObject(0, 0, 0, wheight , lineWeight ,  "→"  );
        text.getGroup().position.x = startPoint.x+( (endPoint.x - startPoint.x) /2);
        text.getGroup().position.y = startPoint.z+( (endPoint.z - startPoint.z) /2);
        text.getGroup().position.z = startPoint.y+( (endPoint.y - startPoint.y) /2);

        const ckSin314:number = parseFloat(xSin.toFixed(2));
        const ckZSin314:number = parseFloat(zSin.toFixed(2));

        if(ckSin314 == 0 && startPoint.x == endPoint.x && startPoint.y != endPoint.y){
            xSin = 90;
        }
        if(ckSin314 == 3.14){
            xSin = 90;
        }

        if( startPoint.z == endPoint.z && animationType == "reverse"){
            xSin = xSin + 180;
        }


        if( Math.abs(ckZSin314) == 1.57){
            zSin = 0;
        }

        if(ckZSin314 == 0 && startPoint.x == endPoint.x && startPoint.y == endPoint.y && startPoint.z != endPoint.z){
            zSin = 90;

            // 양수일경우엔 반전 시킴
            if(Math.sign(startPoint.x) > 0 && Math.sign(endPoint.x) > 0){
                xSin = xSin+180
            }
            if( startPoint.z != endPoint.z && animationType == "reverse"){
                zSin =  270 ;
                xSin = xSin + 180;

            }
        }
        //
        if(ckZSin314 == 3.14){
            zSin = 90;
        }
        text.getGroup().rotation.z=  1.57  *  (zSin / 90);
        // console.log(zSin);
        text.getGroup().rotation.y=  1.57 * (xSin / 90);
        this._axis.add(text.getGroup());

        const texture:CanvasTexture | null = text.getTexture();
        if(texture != null){
            this._textureList.push(texture);
        }

        const textureMesh:Mesh | null = text.getMesh();
        if(textureMesh != null){
            this._textureMeshList.push(textureMesh);
        }


    }

    private addLineList( positionData:Array<{x:number, y:number, z:number}>, lineWeight:number , color:number , opacity:number, flowVisible:boolean = false):void{
        // let positionArray:{[index:number]:Mesh} = {};
        for(let index:number = 0; index <= positionData.length; index++){
            let startPoint:{x:number, y:number, z:number} = positionData[index];
            const endPoint:{x:number, y:number, z:number} | undefined = positionData[index+1];
            if(endPoint == undefined){
                return;
            }
            // 삼각형 길이
            let wheight:number = Math.sqrt(Math.pow((endPoint.y - startPoint.y), 2) + Math.pow((endPoint.z - startPoint.z ), 2) + Math.pow((endPoint.x - startPoint.x ), 2) );
            let sin:number = Math.atan2( (endPoint.z - startPoint.z) , (endPoint.y - startPoint.y)  );
            let sin2:number = Math.PI / 2.0 - Math.acos((endPoint.x - startPoint.x) / wheight);
            let item : MeshBasicMaterialParameters = {};
            item.color = color;

            if(opacity > 0){
                item.transparent= true;
                item.opacity=opacity;
            }

            let material:MeshBasicMaterial = new MeshBasicMaterial( item );

            let geometry:CylinderGeometry = new CylinderGeometry( lineWeight, lineWeight, wheight, 8, 1, true );
            let mesh = new Mesh( geometry, material );
            mesh.position.x = startPoint.x+( (endPoint.x - startPoint.x) /2);
            mesh.position.y = startPoint.z+( (endPoint.z - startPoint.z) /2);
            mesh.position.z = startPoint.y+( (endPoint.y - startPoint.y) /2);
            mesh.rotation.x = -0.5 * Math.PI;
            mesh.rotation.x = mesh.rotation.x - sin;
            mesh.rotation.z = sin2;
            this._lineGroup.add(mesh);

            //→≫▶
            //←≪◀
            if(flowVisible != false){
                this.addFlowTexture(startPoint , endPoint , sin ,sin2 ,  lineWeight , wheight);
            }

            // let animationType:"flow" | "reverse"="flow";
            // const ckSin314:number = parseFloat(sin.toFixed(2));
            //
            // if( (startPoint.y - endPoint.y) <= 0 ){
            //     if(Math.sign(startPoint.y) == -1 && Math.sign(endPoint.y) >= 0){
            //         animationType = "reverse";
            //     }
            //     if(Math.sign(startPoint.y) == -1 && Math.sign(endPoint.y) == -1 && (startPoint.y - endPoint.y ) < 0 ){
            //         animationType = "reverse";
            //     }
            //     if(Math.sign(endPoint.x) >= 0 && Math.sign(startPoint.x) >= 0 && (startPoint.x - endPoint.x) > 0 ) {
            //         animationType = "reverse";
            //     }
            // }
            //
            // if( (startPoint.x - endPoint.x) >= 0  ){
            //     if(Math.sign(endPoint.x) == -1 && Math.sign(startPoint.x) >= 0){
            //         animationType = "reverse";
            //     }
            //
            //     if(Math.sign(endPoint.x) >= 0 && Math.sign(startPoint.x) >= 0 && (startPoint.x - endPoint.x) > 0 ) {
            //         animationType = "reverse";
            //     }
            // }
            //
            // let text:FlowText3DObject = new FlowText3DObject(0, 0, 0, wheight , lineWeight ,  "→"  );
            // text.getGroup().position.x = startPoint.x+( (endPoint.x - startPoint.x) /2);
            // text.getGroup().position.y = startPoint.z+( (endPoint.z - startPoint.z) /2);
            // text.getGroup().position.z = startPoint.y+( (endPoint.y - startPoint.y) /2);
            //
            // if(ckSin314 == 0 && startPoint.x == endPoint.x && startPoint.y != endPoint.y){
            //     sin = 90;
            // }
            // if(ckSin314 == 3.14){
            //     sin = 90;
            // }
            //
            // if(animationType == "reverse"){
            //     sin = sin + 180;
            // }
            //
            // text.getGroup().rotation.y=  1.57 * (sin / 90);
            // group.add(text.getGroup());
            //
            // const texture:CanvasTexture | null = text.getTexture();
            // if(texture != null){
            //     this._textureList.push(texture);
            // }
            // console.log(`-------------------------------------------`);
            // console.log(`sin : ${sin} , ${(sin == 0 ) ? sin : 0}`);
            // console.log(`x : ${startPoint.x}, ${endPoint.x}`);
            // console.log(`y : ${startPoint.y}, ${endPoint.y}`);
            // console.log(`z : ${startPoint.z}, ${endPoint.z}`);





            let thetaLength:number = 3.14;

            // startPoint
            let startPointGeometry:SphereBufferGeometry = new SphereBufferGeometry( lineWeight, 10, 5 , 1 , 6.3 , 0 ,thetaLength );
            let startPointMaterial:MeshBasicMaterial = new MeshBasicMaterial( item );
            let startPointSphere:Mesh = new Mesh( startPointGeometry , startPointMaterial );
            startPointSphere.position.x = startPoint.x;
            startPointSphere.position.y = startPoint.z;
            startPointSphere.position.z = startPoint.y;
            this._lineGroup.add( startPointSphere );

            // endPoint
            let endPointGeometry:SphereBufferGeometry = new SphereBufferGeometry( lineWeight, 10, 5 , 1 , 6.3 , 0 ,thetaLength );
            let endPointMaterial:MeshBasicMaterial = new MeshBasicMaterial( item );
            let endPointSphere:Mesh = new Mesh( endPointGeometry , endPointMaterial );
            endPointSphere.position.x = endPoint.x;
            endPointSphere.position.y = endPoint.z;
            endPointSphere.position.z = endPoint.y;
            this._lineGroup.add( endPointSphere );

            this._axis.add(this._lineGroup);
        }
    }
}
