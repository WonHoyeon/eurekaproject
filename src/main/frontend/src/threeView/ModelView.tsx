import React, {Component} from 'react';
import {
    AxesHelper,
    Camera,
    DirectionalLight,
    HemisphereLight,
    Object3D,
    PerspectiveCamera,
    Scene,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import {IModelListDataSource, IModelViewProps} from "../etc/IModelList";

/**
 * DEMO : https://threejs.org/examples/#webgl_multiple_elements
 * 참고 : https://github.com/mrdoob/three.js/blob/master/examples/webgl_multiple_elements.html
 */
class ModelView extends Component<IModelViewProps, any> {

    private _mount: HTMLElement | null = null;
    private _scene : Scene | null = null;
    private _camera : Camera | null = null;
    private _renderer:WebGLRenderer | null = null;
    private _controls:OrbitControls | null = null;
    private _frameId :number = 0;
    private _obj3D : Object3D | null = null;

    constructor(props:IModelViewProps) {
        super(props);
        this.start = this.start.bind(this);
        this.animate = this.animate.bind(this);
        this.stop = this.stop.bind(this);
    }

    getDerivedStateFromProps(nextProps:any,prevProps:any) {
        if (nextProps.someValue !== prevProps.someValue) {

            if(this._scene != null){
                let axes:AxesHelper = new AxesHelper( 500 );
                axes.name = "AxesHelper";
                this._scene.add( axes );

                //라이트
                let light:HemisphereLight = new HemisphereLight( 0xffffff, 0x444444 );
                light.position.set( 0, 200, 0 );
                this._scene.add( light );

                let light2:DirectionalLight = new DirectionalLight( 0xffffff );
                light2.position.set( 0, 200, 100 );
                light2.castShadow = true;
                light2.shadow.camera.top = 180;
                light2.shadow.camera.bottom = - 100;
                light2.shadow.camera.left = - 120;
                light2.shadow.camera.right = 120;
                this._scene.add( light2 );
                let modelItem:IModelListDataSource = this.props.positionInfo;

                let obj3d:Object3D | null = this._obj3D ;
                let gltfLoader:GLTFLoader = new GLTFLoader();
                let glbURL:string = "/dexi-web/sim/getSimulationModelFile.do?stenc3did="+modelItem.stenc3dId ;
                gltfLoader.load( glbURL , function (this:ModelView, gltf: GLTF ):void {
                    if (obj3d == null) {
                        obj3d = gltf.scene.children[0];
                        obj3d.position.x = 0;
                        obj3d.position.y = 0;
                        obj3d.position.z = 0;
                        obj3d.scale.set(modelItem.modelScale, modelItem.modelScale, modelItem.modelScale);
                        if (this._scene != null) {
                            this._scene.add(obj3d);
                        }

                    }
                }, undefined , undefined );
            }


            return {someState: nextProps.someValue};
        } else return null;
    }

    componentDidMount() {
        if(this._mount === null) {
            return;
        }

        let width:number    = this._mount.clientWidth - 10;
        let height:number   = this._mount.clientHeight - 1;

        // 무대 만들기
        const scene:Scene = new Scene();

        // 카메라 종횡비 설정
        const camera:Camera = new PerspectiveCamera(
            60,// 시야
            1 , // 종횡비
            1, // 어디 부터 (0.1)
            10  // 어디 까지 1000
        );
        // 카메라 위치 설정
        camera.position.set(50,100,50);

        // 화면 사이즈 설정x
        const renderer:WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor( 0xcccccc, 1 );

        let axes:AxesHelper = new AxesHelper( 500 );
        axes.name = "AxesHelper";
        scene.add( axes );

        //라이트
        let light:HemisphereLight = new HemisphereLight( 0xffffff, 0x444444 );
        light.position.set( 0, 200, 0 );
        scene.add( light );
        let light2:DirectionalLight = new DirectionalLight( 0xffffff );
        light2.position.set( 0, 200, 100 );
        light2.castShadow = true;
        light2.shadow.camera.top = 180;
        light2.shadow.camera.bottom = - 100;
        light2.shadow.camera.left = - 120;
        light2.shadow.camera.right = 120;
        scene.add( light2 );

        // 카메라 컨트롤
        this._controls = new OrbitControls(camera , renderer.domElement );
        this._controls.enableDamping = true;
        this._controls.dampingFactor = 0.25;
        this._controls.minDistance = 2;
        this._controls.maxDistance = 5;
        this._controls.enablePan = false;
        this._controls.enableZoom = false;

        // // grid Tile 기준 정보
        // let gridTile:{width:number, height:number}={ width:5 , height:5};
        // let gridwidth:number = 5;
        // let gridheight:number = 5;
        //
        // // grid Tile 기준 정보
        // let planeGeometry:PlaneGeometry = new PlaneGeometry(5,5 , gridwidth / gridTile.width,gridheight / gridTile.height);
        // let planeMaterial:MeshBasicMaterial = new MeshBasicMaterial({color: 0x000066});
        // let grid = new Mesh(planeGeometry, planeMaterial);
        // grid.position.set (0, 0, 0);
        // grid.rotation.x = -0.5 * Math.PI;
        // scene.add(grid);

        let modelItem:IModelListDataSource = this.props.positionInfo;
        let gltfLoader:GLTFLoader = new GLTFLoader();

        let obj3d:Object3D | null = this._obj3D ;

        // let loaderCallBack:(gltf: GLTF)=>void = function (this:ModelView, gltf: GLTF ):void{
        //     if(obj3d == null){
        //         obj3d =  gltf.scene.children[0];
        //         obj3d.position.x = 0;
        //         obj3d.position.y = 0;
        //         obj3d.position.z = 0;
        //         obj3d.scale.set( modelItem.modelScale, modelItem.modelScale, modelItem.modelScale );
        //         scene.add( obj3d );
        //     }
        // }.bind(this);
        let glbURL:string = "/dexi-web/sim/getSimulationModelFile.do?stenc3did="+modelItem.stenc3dId ;

        gltfLoader.load( glbURL , function (this:ModelView, gltf: GLTF ):void{
            if(obj3d == null){
                obj3d =  gltf.scene.children[0];
                obj3d.position.x = 0;
                obj3d.position.y = 0;
                obj3d.position.z = 0;
                obj3d.scale.set( modelItem.modelScale, modelItem.modelScale, modelItem.modelScale );
                scene.add( obj3d );
            }
        }, undefined , undefined );

        this._scene = scene;
        this._camera = camera;
        this._renderer = renderer;
        this._mount.appendChild(this._renderer.domElement);
        this.start();
    }

    start():void{
        if(this._frameId == 0){
            this._frameId = requestAnimationFrame(this.animate);
        }
    }

    public animate():void{
        // if(this._obj3D != null){
        //     this._obj3D.rotateY(0.01);
        // }

        this.renderScene();

        this._frameId = window.requestAnimationFrame(this.animate);
    }



    componentWillUnmount() {
        if(this._renderer == null || this._mount == null ){
            return;
        }
        this.stop();
        this._mount.removeChild(this._renderer.domElement);
    }

    stop = () => {
        cancelAnimationFrame(this._frameId)
    }

    renderScene() {
        if(this._renderer == null || this._scene == null || this._camera == null){
            return;
        }

        if(this._controls != null){
            this._controls.update();
        }

        this._renderer.render(this._scene, this._camera);
    }

    render() {
        // @ts-ignore
        return (
            <div style={{width:"100%" , height:"100%"}}
                 ref={(mount) => {
                     if(mount === null){
                         return;
                     }
                     this._mount = mount

                 }} />
        );
    }
}

export default ModelView;