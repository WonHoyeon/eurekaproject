import SimulationStore from "../admin/simulation/store/SimulationStore";
import ProcessModelStore from "../admin/simulation/store/ProcessModelStore";
import ExcelStore from "../admin/simulation/store/ExcelStore";
import IndexStore from "./IndexStore";
import dashboard from "../admin/DashBoard/store/dashboard";
import monitor from "../admin/monitoring/store/monitor";
import kpiStore from "../admin/kpi/store/kpiStore";
import user from "../admin/User/store/user";

const stores = {
    SimulationStore: new SimulationStore(),
    ProcessModelStore : new ProcessModelStore(),
    ExcelStore : new ExcelStore(),
    IndexStore : new IndexStore(),
    dashboard: new dashboard(),
    monitor: new monitor(),
    kpi: new kpiStore(),
    user: new user()
};

export default stores;