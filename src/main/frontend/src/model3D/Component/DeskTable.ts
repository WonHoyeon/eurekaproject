import {Group} from "three";
import {IHtable} from "../ModelTs";
import Box3DObject from "../Box3DObject";

export default class DeskTable{

    private _dt:Group = new Group();

    //타입스크립트 만들기
    constructor(parameter:IHtable){

        this.deskTable(parameter);

        // 객체 만들기

        //

        // row 추가시...
        // column 추가시..

    }

    /** @description 안전책 객체(전역변수에 담긴 정보)를 반환한다. */
    public getGroup():Group{
        return this._dt;
    }

    private deskTable = (parameter:any) =>{
        let positionX: number = parameter.x + (parameter.x / 2);
        let positionY: number = parameter.y + (parameter.y / 2);
        let positionZ: number = parameter.z + (parameter.z / 2);

        let boxZ: number = parameter.depth / 2;
        let frameWidth: number = 5;
        let deskSide : Box3DObject = new Box3DObject({
            x: positionX ,
            y: positionY- parameter.height/3,
            z: positionZ + parameter.depth/2,
            width: parameter.width,
            height: parameter.height/3,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._dt.add(deskSide.getGroup());

        let middle : Box3DObject = new Box3DObject({
            x: positionX + parameter.height/5,
            y: positionY,
            z: positionZ + parameter.depth/2,
            width: parameter.width/5,
            height: parameter.height/3,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._dt.add(middle.getGroup());

        let deskSide2 : Box3DObject = new Box3DObject({
            x: positionX ,
            y: positionY + parameter.height/3,
            z: positionZ + parameter.depth/2,
            width: parameter.width,
            height: parameter.height/3,
            depth: parameter.depth,
            edgesVisible: parameter.edgesVisible,
            color: parameter.color,
            borderColor: parameter.borderColor
        });
        this._dt.add(deskSide2.getGroup());
    }
}