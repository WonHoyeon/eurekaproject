import React, {Component} from 'react';
import {
    AnimationMixer,
    Camera,
    Clock,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Scene,
    TextureLoader,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import * as createjs from "createjs-module";

import Stats from 'stats.js';
import HTable from "./HTable";
import ConfirmTable from "./Component/ConfirmTable";
import Box3DObject from "./Box3DObject";
import Lac3DObject from "./Component/Lac3DObject";
import {Card} from "antd";


class ThreeMap extends Component<any, any> {


    // 시간
    private _clock: Array<Clock> = [];
    // 애니메이션 플레이어
    private _workerAnimationList: Array<AnimationMixer> = [];
    private stats: Stats = new Stats();
    private mixers = [];
    private _mount: HTMLElement | null = null;
    private scene: Scene | null = null;
    private camera: Camera | null = null;
    private renderer: WebGLRenderer | null = null;
    private controls: OrbitControls | null = null;
    private frameId: number = 0;
    private modelItem: Object3D | null = null;


    private _timeline: createjs.Timeline = new createjs.Timeline([], {}, {paused: true});

    constructor(props: any) {
        super(props);
        this.state = {
            count: 0,
        };
        this.start = this.start.bind(this);
        this.animate = this.animate.bind(this);
        this.stop = this.stop.bind(this);
    }


    private getData(scene: Scene) {
        // ================================================================
        let Cmiddle1: any = {
            positionX: 1000,
            positionY: 770,
            positionZ: 0,
            width: 70,
            height: 70,
            depth: 70,
            column: 2,
            row: 4,
            column_row: 1,
            scene: scene
        };
        this.makeLec(Cmiddle1);
        // ================================================================

    }

    private makeLec(parameter: any) {
        for (let x = 0; x < parameter.column; x++) {
            let box3DObject: Lac3DObject = new Lac3DObject({
                x: parameter.positionX,
                y: parameter.positionY,
                z: parameter.positionZ + (x * (65 * (parameter.depth / 100))),
                width: parameter.width,
                height: parameter.height,
                depth: parameter.depth,
                edgesVisible: false,
                color: 0xFFFFFF,
                borderColor: 0x04B431
            });

            parameter.scene.add(box3DObject.getGroup());
            for (let y = 0; y < parameter.row; y++) {
                let box3DObject: Lac3DObject = new Lac3DObject({
                    x: parameter.positionX + (y * (65 * (parameter.width / 100))),
                    y: parameter.positionY,
                    z: parameter.positionZ + (x * (65 * (parameter.depth / 100))),
                    width: parameter.width,
                    height: parameter.height,
                    depth: parameter.depth,
                    edgesVisible: false,
                    color: 0xFFFFFF,
                    borderColor: 0xFFFFFF
                });
                parameter.scene.add(box3DObject.getGroup());

                for (let z = 0; z < parameter.column_row; z++) {
                    let box3DObject: Lac3DObject = new Lac3DObject({
                        x: parameter.positionX + (y * (65 * (parameter.width / 100))),
                        y: parameter.positionY + (z * (65 * (parameter.height / 100))),
                        z: parameter.positionZ + (x * (65 * (parameter.depth / 100))),
                        width: parameter.width,
                        height: parameter.height,
                        depth: parameter.depth,
                        edgesVisible: false,
                        color: 0xFFFFFF,
                        borderColor: 0xFFFFFF
                    });
                    parameter.scene.add(box3DObject.getGroup());
                }
            }
        }
    }

    async componentDidMount() {
        if (this._mount === null) {
            return;
        }
        let width: number = this._mount.clientWidth - 28;
        let height: number = this._mount.clientHeight - 10;
        const scene: Scene = new Scene();

        const camera: Camera = new PerspectiveCamera(
            100,
            width / height,
            1,
            10000
        );

        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(0xcccccc, 1);


        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);

        light.position.set(0, 200, 0);

        scene.add(light);


        this.controls = new OrbitControls(camera, renderer.domElement);

        this.controls.enableDamping = true;
        this.controls.minDistance = 10;
        this.controls.maxDistance = 10000;
        this.controls.maxPolarAngle = Math.PI * 2;

        this.getData(scene);

        let gridTile: { width: number, height: number } = {width: 50, height: 100};
        let gridwidth: number = 1200;
        let gridheight: number = 800;

        let loadTextURL: string = require("./map01.jpg");
        let loader: TextureLoader = new TextureLoader();


        let planeGeometry: PlaneGeometry = new PlaneGeometry(15000, 7000, 150, 70);
        let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0x000000, wireframe: true});
        let grid = new Mesh(planeGeometry, planeMaterial);
        grid.position.set(0, 0, 0);


        grid.rotation.x = -0.5 * Math.PI;
        scene.add(grid);









        let test7: ConfirmTable = new ConfirmTable(
            {x: 4700, y: 1770, z: 10, width: 150, depth: 220, height: 280});
        scene.add(test7.getGroup());
        let test8: ConfirmTable = new ConfirmTable(
            {x: 4700, y: 1950, z: 10, width: 150, depth: 220, height: 280});
        scene.add(test8.getGroup());
// ==============================================================

        // let modelGlbURL: string = "http://115.145.177.34:3483/File/3DModel/48c61cb0-8ebf-4375-8568-7b081eaffb92/9b278f83-5e93-43a1-ade3-d28b88bbbe85/%EC%A0%9C%ED%92%88(PCB).glb";
        // let modelLoader: GLTFLoader = new GLTFLoader();
        //     modelLoader.load(modelGlbURL, function (object: GLTF) {
        //         let object3dModel: Object3D = object.scene.children[0];
        //     });

        postData('http://example.com/answer', {answer: 42})
            .then(data => console.log(JSON.stringify(data))) // JSON-string from `response.json()` call
            .catch(error => console.error(error));

        function postData(url = '', data = {}) {
            // Default options are marked with *
            return fetch(url, {
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, cors, *same-origin
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, *same-origin, omit
                headers: {
                    'Content-Type': 'application/json',
                    // 'Content-Type': 'application/x-www-form-urlencoded',
                },
                redirect: 'follow', // manual, *follow, error
                referrer: 'no-referrer', // no-referrer, *client
                body: JSON.stringify(data), // body data type must match "Content-Type" header
            })
                .then(response => response.json()); // parses JSON response into native JavaScript objects
        }

// ====================================구역나누기===========================================================================
        let textBox: Box3DObject = new Box3DObject({
            x: 0,
            y: 0,
            z: 4,
            width: 15000,
            height: 7000,
            depth: 1,
            edgesVisible: false,
            color: 0xFFFFFF,
            opacity: 1,
            textInfo: {
                width: 300,
                height: 50,
                text: ""
            }
        });

        scene.add(textBox.getGroup());

        let textBox2: Box3DObject = new Box3DObject({
            x: -4000,
            y: -1800,
            z: 6,
            width: 6500,
            height: 800,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8,
            textInfo: {
                width: 400,
                height: 150,
                text: "Yura Vietnam C building"
            }
        });
        scene.add(textBox2.getGroup());


        let textBox3: Box3DObject = new Box3DObject({
            x: 3500,
            y: -2300,
            z: 6,
            width: 7000,
            height: 1800,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8,

        });
        scene.add(textBox3.getGroup());


        // ============================왼쪽라인================================
        let textBox4: Box3DObject = new Box3DObject({
            x: -4800,
            y: -500,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(textBox4.getGroup());


        let textBox5: Box3DObject = new Box3DObject({
            x: -4800,
            y: 600,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(textBox5.getGroup());

        let textBox6: Box3DObject = new Box3DObject({
            x: -4800,
            y: 1700,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(textBox6.getGroup());

        let textBox7: Box3DObject = new Box3DObject({
            x: -4800,
            y: 2800,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(textBox7.getGroup());
// :::::::::::::::::::::::::::::중간라인::::::::::::::::::::::::::::::::::::::::

        let center1: Box3DObject = new Box3DObject({
            x: 0,
            y: -500,
            z: 6,
            width: 4000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(center1.getGroup());


        let center2: Box3DObject = new Box3DObject({
            x: 0,
            y: 600,
            z: 6,
            width: 4000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(center2.getGroup());

        let center3: Box3DObject = new Box3DObject({
            x: 0,
            y: 1700,
            z: 6,
            width: 4000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(center3.getGroup());

        let center4: Box3DObject = new Box3DObject({
            x: 0,
            y: 2800,
            z: 6,
            width: 4000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(center4.getGroup());

// :::::::::::::::::::::::::::::오른쪽 부분::::::::::::::::::::::::::::::::::::::::


        let right1: Box3DObject = new Box3DObject({
            x: 4800,
            y: -500,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(right1.getGroup());


        let right2: Box3DObject = new Box3DObject({
            x: 4800,
            y: 600,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(right2.getGroup());

        let right3: Box3DObject = new Box3DObject({
            x: 4800,
            y: 1700,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(right3.getGroup());

        let right4: Box3DObject = new Box3DObject({
            x: 4800,
            y: 2800,
            z: 6,
            width: 5000,
            height: 1000,
            depth: 1,
            edgesVisible: false,
            color: 0xE6E6E6,
            opacity: 0.8
        });
        scene.add(right4.getGroup());



        let desk5: HTable = new HTable({
            x: 1750,
            y: 900,
            z: 6,
            width: 220,
            height: 120,
            depth: 140,
            color: 0xE3CEF6,
        });
        scene.add(desk5.getGroup());

        let desk6: HTable = new HTable({
            x: 2000,
            y: 900,
            z: 6,
            width: 220,
            height: 120,
            depth: 140,
            color: 0xE3CEF6,
        });
        scene.add(desk6.getGroup());

        let desk7: HTable = new HTable({
            x: 1750,
            y: 1650,
            z: 6,
            width: 220,
            height: 120,
            depth: 140,
            color: 0xE3CEF6,
        });
        scene.add(desk7.getGroup());

        let desk8: HTable = new HTable({
            x: 2000,
            y: 1650,
            z: 6,
            width: 220,
            height: 120,
            depth: 140,
            color: 0xE3CEF6,
        });
        scene.add(desk8.getGroup());
// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::













// ::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        // 카메라 위치 설정
        camera.position.set(0, 1500, 0);
        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;
        this._mount.appendChild(this.renderer.domElement);

        this.stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom

        let stateHtml: HTMLElement = this.stats.dom;
        stateHtml.setAttribute("style", "position: absolute; top: 60px; cursor: pointer; opacity: 0.9; z-index: 1000;");
        this._mount.appendChild(stateHtml);

        this.start();

    }

    start(): void {
        this.animate();
    }

    animate = () => {

        this.stats.begin();
        // this.stats.end();
        this.frameId = window.requestAnimationFrame(this.animate);
        //push호 mixers에 담아두었던 것을 반복문을 통해 전체 프레임을 실행시킨다.
        if (this.mixers.length > 0) {
            for (let i = 0; i < this.mixers.length; i++) {
                // @ts-ignore
                //Object for keeping track of time. This uses performance.now if it is available, otherwise it reverts to the less accurate Date.now.
                //시간을 추척하는 용도.
                this.mixers[i].update(this._clock.getDelta());
            }
        }

        //반복문을 돌려서 다음 프레임(1000ms)를 계속 돌린다.
        if (this._workerAnimationList.length > 0) {
            for (let i: number = 0; i < this._workerAnimationList.length; i++) {
                // this.workerAnimationList[i].update(time);
                this._workerAnimationList[i].update(this._clock[i].getDelta());
            }
        }


        this.renderScene();
    };

    componentWillUnmount() {
        if (this.renderer == null || this._mount == null) {
            return;
        }
        this.stop();
        this._mount.removeChild(this.renderer.domElement);
    }

    stop = () => {
        cancelAnimationFrame(this.frameId)
    };


    public change = (time: number) => {
        this._timeline.setPosition(time);

        this.setState({})

    };

    renderScene() {
        if (this.renderer == null || this.scene == null || this.camera == null) {
            return;
        }

        if (this.controls != null) {
            this.controls.update();
        }
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return (
            <div className={'monitorTop'} style={{height:400}}
                     ref={(mount) => {
                         if (mount === null) {
                             return;
                         }
                         this._mount = mount
                     }}

                />
        )
    }
}

export default ThreeMap;