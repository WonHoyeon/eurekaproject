import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {Button, Card, DatePicker, message, Modal, PageHeader, Timeline} from 'antd';
import moment from "moment";
import {inject, observer} from "mobx-react";
import Searvice from "../../common/Searvice";
import autobind from "autobind-decorator";
import {Link} from "react-router-dom";
import './css/dashboard.css'
import monitor from "../monitoring/store/monitor";
import {GoogleMap, InfoWindow, Marker, withGoogleMap} from "react-google-maps";
import 'react-open-weather/lib/css/ReactWeather.css';
import {FactoryMessage, GlobalParameter, MapLocation, SpotChange} from "./typescript/DashBoardType";
import DashBoardService from "./typescript/DashBoardService";
import KpiComponent from "./Component/KpiComponent";
import StringUtile from "../../utile/StringUtile";
import {ExclamationCircleOutlined, GlobalOutlined} from "@ant-design/icons/lib";



/** @description 구글 API Key */
const API_KEY = "ea4e6450e60a3bd22df8a41e32ccc7c2"
// 인터넷에 떠도는 API key
const API_KEY2 = "8b4a1cfe7b37f251dcce8b232975fd6d"
// DashBoard Service
const service: DashBoardService = new DashBoardService();
// 오늘날자
const TodayDate = {
    userId:sessionStorage.getItem('loggedInfo'),
    date: StringUtile.formatDate()
}
const { confirm } = Modal;


/**
 * @description mobx [[dashboard]] 와 [[monitor]] 을 상속한 클래스 (GlobalMap.tsx)
 */
@inject('dashboard', 'monitor')
@observer
class GlobalMap extends Component<any, SpotChange> {
    constructor(props: any) {
        super(props);
        this.state = {
            latitude: 25,
            longitude: 1,
            zoom: 1.8,
            factoryName: '',
            modelId: '',
            city: "",
            description: "",
            tempertaure: 0,
            pressure: 0,
            humidity: 0
        }
    }


    /**
     * @description 시작메서드, 현재날짜 기준으로 맵정보, 공장정보를 호출한다.
     */
    async componentDidMount() {
        if(sessionStorage.getItem('loggedInfo') == null){
            this.confirmMessage();
        }
        const {monitor, dashboard} = this.props;
        dashboard.setMapLoading(true);
        dashboard.setPickerDate(StringUtile.formatDate());
        /** @description 오늘날짜 기준으로 데이터 호출 */
        const entireMarker = await service.getGlobalMapData(TodayDate)
        const factoryMessage = await service.factoryMessage(TodayDate)
        const globalKpi = await service.globalKpi(TodayDate)

        /** @description mobx에 저장*/
        dashboard.setDashboardKpi(globalKpi);
        dashboard.setMapLocation(entireMarker);
        dashboard.setCompanyMessage(factoryMessage);
        dashboard.setMapLoading(false);
    }

    /**
     * @description openweathermap 라이브러리를 사용하여 날씨정보를 가지고 오는 메서드
     * @param e 선택한 공장에대한 정보
     */
    @autobind
    private async getWeather(e: MapLocation) {
        const cityname = "Regensburg";
        const url = `https://api.openweathermap.org/data/2.5/weather?lat=${e.latitude}&lon=${e.longitude}&appid=${API_KEY2}`;
        if (cityname) {
            fetch(url)
                .then(response => response.json())
                .then(data => {
                    if (data.cod === 404) {
                    }
                    this.setState({
                        city: data.name,
                        description: data.weather[0].description,
                        tempertaure: data.main.temp,
                        humidity: data.main.humidity,
                        pressure: data.main.pressure,
                        modelId :e.modelID
                    });
                })
                .catch(e => console.log("error", e));
        } else {
            this.setState({
                city: "",
                description: "",
                tempertaure: 0,
                pressure: 0,
                humidity: 0,
            });
        }
    }


    /**
     * @description 해당 날짜 기준으로 데이터를 호출한다(공장메세지, kpi정보, 맵정보 등)
     * @param selectDate 선택한 날짜 데이터
     */
    @autobind
    public async dateSelect(date: moment.Moment | null, selectDate: string) {
        const {dashboard} = this.props;
        dashboard.setDashLoading(true);
        let parameter: GlobalParameter = {
            userId: sessionStorage.getItem('loggedInfo'),
            date: selectDate
        }
        const factoryMessage = await service.factoryMessage(parameter)
        const globalKpi = await service.globalKpi(parameter)
        dashboard.setDashboardKpi(globalKpi);
        dashboard.setPickerDate(selectDate);
        dashboard.setCompanyMessage(factoryMessage);
        dashboard.setDashLoading(false);
    }


    /**
     * @description 맵 Marker 또는 공장메세지 리스트에서 클릭한 정보를 받아 설치
     * @param value 해당 공장에 대한 기본정보
     */
    @autobind
    public getCI(value: FactoryMessage) {

        const {dashboard, monitor} = this.props;
        let parameter = {
            name: value.factoryName,
            location: "",
            id: value.factoryID,
            mapID: "",
            latitude: value.latitude,
            modelID: value.modelID,
            longitude: value.longitude,
            companyID: value.factoryID,
        }
        this.getWeather(parameter);
        dashboard.setMapLoading(true);
        monitor.setPostId(value.factoryID);
        monitor.setModelId(value.modelID)
        this.setState({
            latitude: value.latitude,
            longitude: value.longitude,
            zoom: 6,
            factoryName: value.factoryName
        })
        this.props.dashboard.setMapLoading(false);
    }


    /**
     * @description 마커 클릭시 마커 중심으로 이동, 날씨조회
     */
    @autobind
    public op(store: MapLocation) {
        this.props.monitor.setPostId(store.companyID);
        this.props.monitor.setModelId(store.modelID);
        this.getWeather(store);
        this.setState({
            latitude: store.latitude,
            longitude: store.longitude,
            zoom: 3.9,
            factoryName: store.name,
            modelId:store.modelID
        })
    }


    /** @description 공장 전체 마커표현 */
    displayMarkers = () => {
        return this.props.dashboard.getMapLocation.map((store: MapLocation, index: number) => {
            return <Marker key={index} position={{
                lat: store.latitude,
                lng: store.longitude
            }}
                           icon={"http://maps.google.com/mapfiles/ms/icons/green-dot.png"}
                           onClick={() => this.op(store)}
            />
        })
    }

    /** @description 공장메세지  마커표현 */
    displayMarkers2 = () => {
        const {dashboard} = this.props;
        return dashboard.getCompanyMessage.map((store: FactoryMessage, index: number) => {
            let parameter = {
                name: store.factoryName,
                location: "",
                id: store.factoryID,
                mapID: "",
                latitude: store.latitude,
                modelID: store.modelID,
                longitude: store.longitude,
                companyID: store.relatedID,
            }
            return <Marker key={index} position={{lat: store.latitude, lng: store.longitude}}
                           icon={`http://maps.google.com/mapfiles/ms/icons/${
                               (() => {
                                   switch (store.eventType) {
                                       case 'Warning':
                                           return 'yellow'
                                           break;
                                       case 'Bad':
                                           return 'orange'
                                           break;
                                       case 'NG':
                                           return 'red'
                                           break;
                                       default:
                                           return 'blue'
                                           break;
                                   }
                               })()
                           }-dot.png`}
                           onClick={() => this.op(parameter)}
            />
        })
    }

    /**
     * @description CompanyId를 가지고 회사 kpi 및 맵정보 가지고 오는 메서드
     * @param value CompanyId
     */
    @autobind
    public async moveCompany(value: number, store:string) {
        const {dashboard, monitor} = this.props;
        let parameter:GlobalParameter = {
            factoryId: value,
            date: dashboard.getPickerDate,
            factoryType:'Factory',
            userId:sessionStorage.getItem('loggedInfo')
        }
        let parame:{modelId:string, level:number} ={
            modelId:store,
            level: -1
        }
        sessionStorage.setItem('currentState', 'Factory');

        try {
            const resultData:any =  await Searvice.getPost("dash/factory3DInfo.do", parame);
            monitor.setFactory3DInfo(resultData.data.factoryMessage)
            monitor.setNowLevel(1)
        } catch (e) {
            console.log(e);
        }
        try {
            const resultData:any = await Searvice.getPost("dash/selectFactorylMap.do", parameter);

            monitor.setHirerachyName(resultData.data.factoryInfo[0].hirerachyName);

            monitor.setHirerachyName2(resultData.data.factoryInfo[0].hirerachyName);
            monitor.setFactory(resultData.data)
        } catch (e) {
            console.log(e);
        }
    }
    /**
     * @description 지도 popover에 openweathermap 에서 받은 날씨 정보 출력
     */
    @autobind
    displayWins() {
        const {monitor} = this.props;
        return <InfoWindow
            position={{
                lat: this.state.latitude,
                lng: this.state.longitude,
            }}>
            <div style={{width: 300, textAlign: 'center'}}>
                <h3 style={{textDecoration: "underline"}}>
                    {(this.state.modelId !== "")? <Link to={'/Main/Monitoring/Entire/' + this.props.monitor.getPostId} onClick={() => {
                        this.moveCompany(monitor.getPostId, monitor.getModelId);
                    }}>{this.state.factoryName}</Link> : <span>{this.state.factoryName}</span>}
                    </h3>
                <div>도시 : {this.state.city}</div>
                <div>기온 : {this.state.tempertaure}</div>
                <div>기압 : {this.state.pressure}</div>
                <div>습도 : {this.state.humidity}</div>
                <div>설명 : {this.state.description}</div>
            </div>
        </InfoWindow>
    }

    @autobind
    private confirmMessage(){
        confirm({
            title: 'DashBoard 사용시 로그인이 필요합니다.',
            icon: <ExclamationCircleOutlined />,
            okText: 'Cancle',
            okType: 'danger',
            cancelText: 'Login',
            onOk() {
                window.location.replace("/");
            },
            onCancel() {
                window.location.replace("/eureka/user/Login");
            },
        });
    }

    @autobind
    public spotReset(){
        this.setState({
            latitude: 25,
            longitude: 1,
            zoom: 1.8,
        })
    }
    globalMap = () => {
        window.location.replace("/eureka/Main");
    }
    render() {
        const {dashboard} = this.props;
        //pickerdate 데이터 형식 설정 (ex- 'YYYY/MM/DD')
        const dateFormat = 'YYYY-MM-DD';
        /** @description 구글맵 API 적용 */
        const GoogleMapAPI = withGoogleMap(props => (
            <GoogleMap
                defaultCenter={{lat: this.state.latitude, lng: this.state.longitude}}
                defaultZoom={this.state.zoom}
            >
                {(this.state.latitude !== 25) ? this.displayWins() : null}
                {this.displayMarkers()}
                {this.displayMarkers2()}
            </GoogleMap>
        ));

        return (
            <div style={{minWidth: 650}}>
                <PageHeader style={{backgroundColor: 'white'}} title="">
                    <h2 style={{marginRight:20, float:"left"}} onClick={this.globalMap}><a> Monitoring</a></h2>
                    <div style={{float:"right"}}>
                        <DatePicker value={moment(dashboard.getPickerDate)} format={dateFormat}
                                    style={{marginRight: 5, width: 250}}
                                    onChange={this.dateSelect}
                        />
                    </div>
                </PageHeader>

                <Card style={{marginTop: 5}}>
                    <Card style={{float: "left", width: '69%'}} loading={dashboard.getMapLoading}>
                        <Button type={'primary'} onClick={this.spotReset} style={{position:"absolute", bottom: 0, left:210,top:38, zIndex:3}}><GlobalOutlined/></Button>

                        <GoogleMapAPI
                            containerElement={<div style={{height: `400px`, width: '100%'}}/>}
                            mapElement={<div style={{height: `100%`}}/>}
                        />
                    </Card>
                    <Card className={'earthInfo'} style={{height: 450, overflowY: "scroll"}}
                          loading={dashboard.getDashLoading}>
                        <div><h2>LATEST ACTIVITY</h2></div>
                        <Timeline style={{marginTop: 20}}>
                            {dashboard.getCompanyMessage.map((value: FactoryMessage, key: number) => {
                                return <Timeline.Item color={
                                    (() => {
                                        switch (value.eventType) {
                                            case 'Warning':
                                                return 'yellow'
                                                break;
                                            case 'Bad':
                                                return 'orange'
                                                break;
                                            case 'NG':
                                                return 'red'
                                                break;
                                            default:
                                                return 'blue'
                                                break;
                                        }
                                    })()
                                } key={key}>
                                    <p style={{color: 'red'}}>
                                        &nbsp;&nbsp;<Button size={'small'}
                                                            style={{
                                                                backgroundColor: (() => {
                                                                    switch (value.eventType) {
                                                                        case 'Warning':
                                                                            return 'yellow'
                                                                            break;
                                                                        case 'Bad':
                                                                            return 'orange'
                                                                            break;
                                                                        case 'NG':
                                                                            return 'red'
                                                                            break;
                                                                        default:
                                                                            return 'blue'
                                                                            break;
                                                                    }
                                                                })(), color: 'white'
                                                            }}
                                                            onClick={() => this.getCI(value)}>{value.factoryName}</Button>
                                    </p>
                                    <p>{value.message}</p>
                                </Timeline.Item>
                            })}
                        </Timeline>
                    </Card>
                </Card>
                <KpiComponent/>
            </div>
        );
    };
}


export default GlobalMap;
