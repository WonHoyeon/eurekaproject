import React, {Component} from 'react';
import 'antd/dist/antd.css';
import Chart, {CommonSeriesSettings, Format, Label, Series, Tooltip} from "devextreme-react/chart";
import PieChart, {Connector, Label as PieLabel, Series as PieSeries, Size} from "devextreme-react/pie-chart";
import {Card, Statistic,} from "antd";
import {inject, observer} from "mobx-react";


@inject('dashboard', 'monitor')
@observer
class KpiFactory extends Component<any, any> {


    componentDidMount() {

    }

    render() {

        const {dashboard} = this.props;
        return (
            <div style={{marginTop: 10}}>{dashboard.getGlobalKpiInfo.map((param: any, index: number) => {
                return <> {dashboard.getTest.map((value: any, index: number) => {

                    if (param.kpiid === value[0]) {
                        return <Card key={index} title={param.kpiname} size={'small'}
                                     style={{height: 200, width: '33%', float: "right"}}>


                            {(param.uitype == '123') ?
                                value.flat().map((store: any, index: number) => {
                                    if (index !== 0) {
                                        return <Statistic key={index}
                                                          value={store.value}
                                                          valueStyle={{fontSize: 20, paddingTop: 30}}
                                            // prefix={value.text}
                                                          suffix={'%'}
                                        />
                                    }
                                })
                                :
                                null
                            }


                            {
                                (() => {
                                    switch (param.uitype) {

                                        case 'Bar':
                                            return <Chart id="chart"
                                                          dataSource={value[1]}
                                                          style={{height: 140}}>
                                                <CommonSeriesSettings
                                                    argumentField="name"
                                                    type="bar"
                                                    hoverMode="allArgumentPoints"
                                                    selectionMode="allArgumentPoints"
                                                >
                                                    <Label visible={true}>
                                                        <Format type="fixedPoint" precision={0}/>
                                                    </Label>
                                                </CommonSeriesSettings>
                                                <Series
                                                    valueField="value"
                                                    name="계획정지"
                                                />
                                                <Tooltip enabled={true} shared={true}/>
                                            </Chart>
                                            break;
                                        case 'Pie':
                                            return <span>  <PieChart style={{height: 190}} dataSource={value[1]}>
                                                        <PieSeries argumentField={'name'} valueField={'value'}>
                                                            <PieLabel visible={true}>
                                                                <Connector visible={true} width={1}/>
                                                            </PieLabel>
                                                        </PieSeries>
                                                        <Size/>
                                                    </PieChart>
                                                    <Tooltip enabled={true} shared={true}/>
                                                    </span>
                                            break;
                                    }
                                })()
                            }

                        </Card>
                    }
                })}
                </>
            })}

            </div>
        );

    };
}


export default KpiFactory;
