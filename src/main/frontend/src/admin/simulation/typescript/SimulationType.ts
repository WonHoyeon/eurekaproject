export interface SmulationList{
    simReqId: string,
    modelNm: string,
    simReqNm: string,
    insDt: string,
    userNm: string,
    simReqStNm: string,
    simReqTpNm: string
}