import React from "react";
import {action, computed, observable, runInAction} from "mobx";
import {message} from "antd";
import Searvice from "../../../common/Searvice";

class ExcelStore {


    @action
    excelDownload(parame: string, sorttype: string, sortname: string) {
        const parameter = {
            simulationid: parame,
            loclid: "KO",
            sorttype: sorttype,
            sortname: sortname
        }
        Searvice.getFileDownload("sim/timeoutExcelReport.do", parameter);
    }

    @action
    async facilityoutputExcel(parame: string, checkedkeys: Array<string>, sortname: string, sorttype: string) {

        const parameter = {
            simulationid: parame,
            loclid: "KO",
            checkedkeys: checkedkeys,
            sorttype: sorttype,
            sortname: sortname
        }

        try {
            await Searvice.getFileDownload("sim/facilityoutputExcelReport.do", parameter);
        } catch (error) {
            runInAction(() => {
                message.error('데이터 불러오기에 실패하였습니다.', 5);
                return null;
            })
        }
    }

    @action
    async timebufferusageExcel(parame: string, sortname: string) {
        const parameter = {
            simulationid: parame,
            loclid: "KO",
            sortname: sortname
        }
        try {
            await Searvice.getFileDownload("sim/timebufferusageExcelReport.do", parameter);
        } catch (error) {
            runInAction(() => {
                message.error('데이터 불러오기에 실패하였습니다.', 5);
                return null;
            })
        }
    }

    @action
    async productoutputExcel(parame: string, checkedkeys: any, sortname: any) {
        const parameter = {
            simulationid: parame,
            loclid: "KO",
            checkedkeys: checkedkeys,
            sortname: sortname
        }
        try {
            await Searvice.getFileDownload("sim/productoutputExcelReport.do", parameter);
        } catch (error) {
            runInAction(() => {
                message.error('데이터 불러오기에 실패하였습니다.', 5);
                return null;
            })
        }
    }

    @action
    async facilityoperatingrateExcel(parame: string, checkedkeys: string, sorttype: string, sortname: string) {
        var checkedkeystring: string = "";

        for (var i = 0; i < checkedkeys.length; i++) {
            if(checkedkeys.length == 1){
                checkedkeystring += "(" + '"' + checkedkeys[i] + '"'+ ")"
            }
            else if(i == 0){
                checkedkeystring += "(" + '"' + checkedkeys[i] + '"'
            }
            else if(i == checkedkeys.length -1){
                checkedkeystring += "," + '"' + checkedkeys[i] + '"' + ")"
            }
            else{
                checkedkeystring +=  "," + '"' + checkedkeys[i] + '"';
            }

        }
        const parameter = {
            simulationid: parame,
            loclid: "KO",
            checkedkeysstring: checkedkeystring,
            sorttype: sorttype,
            sortname: sortname
        }
        try {
            await Searvice.getFileDownload("sim/facilityoperatingrateExcelReport.do", parameter);
        } catch (error) {
            runInAction(() => {
                message.error('데이터 불러오기에 실패하였습니다.', 5);
                return null;
            })
        }
    }
}


export default ExcelStore;