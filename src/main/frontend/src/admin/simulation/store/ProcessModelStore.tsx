import React from "react";
import {action, computed, observable, runInAction} from "mobx";
import {IProcessAnimationInfo, IProcessMapInfo, IProcessModelInfo} from "../../../threeView/IProcessModelMain";
import {IProcessModelData} from "./SimulationType";
import {message} from "antd";
import SimulationTapsService from "../Service/SimulationTapsService";
import * as createjs from "createjs-module";


class ProcessModelStore {

    /**
     * @description 모델 loading에대한 변수
     */
    @observable modelLoading:boolean = false;

    /**
     * @description mobx store에 loading 설정값을 저장해주는 메서드
     * @param parameter oading에대한 설정값
     */
    @action setModelLoading = (parameter: boolean) => {
        this.modelLoading = parameter;
    };

    /**
     * @description mobx Store에 설정되어진 loading 설정값을 반환한다.
     */
    @computed get getModelLoading() {
        return this.modelLoading;
    }


    /**
     * @description reactInterval 에서 초단위를 설정할때 필요한 변수
     */
    @observable count:number = 0;

    /**
     * @description count의 값을 받아 mobxStore 값에 저장한다.
     * @param parameter count값
     */
    @action setCount =(parameter:number)=>{
        this.count = parameter;
    };

    /**
     * @description mobxStore에 저장된 현 count의 값을 반환한다.
     */
    @computed get getCount(){
        return this.count;
    }

    /**
     * @description reactInterval에서 재생속도에 사용하는 변수
     */
    @observable speedValue:number = 1;

    /**
     * @description reactInterval에서 재생속도에 사용하는 변수
     * @param reactInterval에서 재생속도에 사용하는 변수
     */
    @action setSpeedValue =(parameter:number)=>{
        this.speedValue = parameter;
    };

    /**
     * @description reactInterval에서 재생속도에 사용하는 변수
     */
    @computed get getSpeedValue(){
        return this.speedValue;
    }


    /**
     * @description  // 재생 | 중지를 결정하는 변수.
     */
    @observable timerPlayEnable:boolean = true;

    @action setTimerPlayEnable =(parameter:boolean)=>{
        this.timerPlayEnable = parameter;
    };


    @computed get getTimerPlayEnable(){
        return this.timerPlayEnable;
    }


    /**
     * @description  nextCount를 임시로 받아둘 변수 (현재 재생시간을 출력할때 사용할 것)
     */
    @observable timeValue:number = 0;

    @action setTimeValue =(parameter:number)=>{
        this.timeValue = parameter;
    };

    @computed get getTimeValue(){
        return this.timeValue;
    }


    /**
     * @description 재생상태를 담을 mobx Store 변수
     */
    @observable palyIcon:'caret-right'|'pause' = 'caret-right';

    @action setPalyIcon =(parameter:'caret-right'|'pause')=>{
        this. palyIcon = parameter;
    };

    @computed get getPalyIcon():'caret-right'|'pause'{
        return this.palyIcon;
    }
// ==================================================================================
//  =================================================================================
//  ProcessModel

    /** @description 공정 모델 > 3d 모델 정보를 담을 mobx 변수 */
    @observable mapData:Array<IProcessMapInfo> = [];

    @action setMapData =(parameter:Array<IProcessMapInfo>)=>{
        this.mapData = parameter;
    };

    @computed get getMapData():Array<IProcessMapInfo>{
        return this.mapData;
    }


    /** @description 공정 모델 > 3d 모델 정보를 담을 mobx 변수 */
    @observable modelListData:Array<IProcessModelInfo> = [];

    @action setModelListData =(parameter:Array<IProcessModelInfo>)=>{
        this.modelListData = parameter;
    };

    @computed get getModelListData():Array<IProcessModelInfo>{
        return this.modelListData;
    }


    /** @description 프로세스 애니메이션 리스트 정보를 담을 mobx 변수 */
    @observable processAnimation:Array<IProcessAnimationInfo> = [];

    @action setProcessAnimation =(parameter:Array<IProcessAnimationInfo>)=>{
        this.processAnimation = parameter;
    };

    @computed get getProcessAnimation():Array<IProcessAnimationInfo>{
        return this.processAnimation;
    }


    /** @description 애니메이션 전체시간 정보를 담을 mobx 변수 */
    @observable processAnimationTotalTime:number = 0;

    @action setProcessAnimationTotalTime =(parameter:number)=>{
        this.processAnimationTotalTime = parameter;
    };

    @computed get getProcessAnimationTotalTime():number{
        return this.processAnimationTotalTime;
    }

    /** @description hse애니메이션 정보를 담을 mobx 변수 */
    @observable hseAnimationData:Array<any> = [];

    @action setHseAnimationData =(parameter:Array<any>)=>{
        this.hseAnimationData = parameter;
    };

    @computed get getHseAnimationData(){
        return this.hseAnimationData;
    }
    /**
     * @description 공정모델 데이터 출력에 필요한 데이터를 가져온다.
     * @param parameter db에서 데이터를 호출한 값
     */
    @action setProcessModelData = (parameter:IProcessModelData) =>{
        this.mapData = parameter.mapData;
        this.modelListData = parameter.modelListData;
        this.processAnimation = parameter.processAnimation;
        this.processAnimationTotalTime = parameter.processAnimationTotalTime;
    }

    @computed get getProcessModelData(){
        return  this.mapData;
        return  this.modelListData;
        return  this.processAnimation;
        return  this.processAnimationTotalTime;
    }

    /** @description set End끝나는 시각 */
    @action setEndTimeStart =(count:number, playIcon:'caret-right'|'pause', playEnable:boolean)=>{
        this.count = count;
        this.palyIcon = playIcon;
        this.timerPlayEnable = playEnable;
    };


    /** @description count값과 palyIcon(플레이버튼 ICON) 변수 초기화 */
    @action setResetProcess =()=>{
        this.palyIcon = 'caret-right';
        this.count = 0;
    };

    /**
     * @description HSE Model에 필용한 데이터 비동기 호출
     * @param parame x
     */
    @action
    async HSEModelsetData() {
        try {
            const service:SimulationTapsService = new SimulationTapsService();
            const data:any = await service.getFactoryModel();
            runInAction(() => {
                this.modelAttrData = data.modelAttrData;
                this.mapData = data.mapData;
                this.modelListData = data.modelListData;
                this.hseAnimationData = data.hseAnimationData;
            })
        } catch (error) {
            message.error('데이터 불러오기에 실패하였습니다.', 5);
            return null;
        }
    }

    @observable modelAttrData:any = [];

    @action setModelAttrData= (parameter:any) =>{
        this.modelAttrData = parameter;
    };

    @computed get getModelAttrData(){
        return this.modelAttrData;
    }


    // ========================================================================================================
    // ======================================================================================================
    /** @description  movieClip과 연결된 타임라인, 인스턴스에 전달하기위함 (일시중지, 되감기등 지원) */
        // private _timeline: createjs.Timeline = new createjs.Timeline([], {}, {paused: true});

    @observable timeline:createjs.Timeline = new createjs.Timeline([], {}, {paused: true});

    @action setTimeline= (parameter:any) =>{
        this.timeline = parameter;
    };

    @computed get getTimeline(){
        return this.timeline;
    }


    /** @description Simulation Log(animation) 로딩설정 */
    @observable animationLoading:boolean = true;

    @action setAnimationLoading =(parameter:boolean)=>{
        this.animationLoading = parameter;
    };

    @computed get getAnimationLoading(){
        return this.animationLoading;
    }


    /** @description Simulation 3D화면(animation) 로딩설정 */
    @observable animation3dLoading:boolean = true;

    @action setAnimation3dLoading =(parameter:boolean)=>{
        this.animation3dLoading = parameter;
    };

    @computed get getAnimation3dLoading(){
        return this.animation3dLoading;
    }

    @observable facilityChartLoading:boolean = true;

    @action setFacilityChartLoading(param:boolean) {

        this.facilityChartLoading = param;
    }
    @computed get getFacilityChartLoading() {
        return this.facilityChartLoading;
    }


    /**
     * @description DASHBOARD에서 설비를 제외한 모든 차트 로딩을 위한 변수
     */
    @observable dashboardChartLoading:boolean = true;

    @action setDashboardChartLoading(param:boolean) {

        this.dashboardChartLoading = param;
    }
    @computed get getDashboardChartLoading() {
        return this.dashboardChartLoading;
    }

    @action setLodingBox = (parameter:boolean) =>{
        this.animationLoading = parameter;
        this.animation3dLoading = parameter;
        this.dashboardChartLoading = parameter;
        this.facilityChartLoading = parameter;
    }
}


export default ProcessModelStore;