import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import '../../../../../css/IndexPage.css';
import {Card} from "antd";
import StringUtile from "../../../../../utile/StringUtile";
import autobind from "autobind-decorator";


@inject('SimulationStore', 'ProcessModelStore')
@observer
class SimLog extends Component<any, any> {
    private _processListData: any = [];
    private _processChartListData: any = [];

    componentDidMount(){
        // this.props.ProcessModelStore.getProcessAnimation.map((value: any) => {
        //
        //     console.log('test');
        //     this.processSuccessFailFun(value.productIdx, value.status, value.startTime, value.endTime, value.stencNm);
        // });
    }

    componentWillUpdate(){
    }

    @autobind
    private processSuccessFailFun(productIdx: number, state: string, simstartTime: string, simendTime: string, stencNm:string): void {
        const {SimulationStore} = this.props;
        let processList: Array<{ productIdx: number, state: string, totalTime: number, ngTime: number, stencNm:string }> = Object.assign([], SimulationStore.getProcessList);
        let processChartList: Array<{ productIdx: number, state: string, totalTime: number, ngTime: number, stencNm:string }> = Object.assign([], SimulationStore.getProcessChartList);


        let startdate: Date = new Date(simstartTime);
        let startSeconds: number = startdate.getTime();
        let enddate = new Date(simendTime);
        let endSeconds: number = enddate.getTime();


        let totalTime: number = startSeconds;
        let ngTime: number = 0;
        if (state == '' || state == 'OK') {
            state = "OK";
        } else {
            ngTime = totalTime;
            totalTime = 0;
        }

        processList.unshift({productIdx: productIdx, state: state, totalTime: totalTime, ngTime: ngTime, stencNm:stencNm});


        processChartList.unshift({productIdx: productIdx, state: state, totalTime: totalTime, ngTime: ngTime, stencNm:stencNm});
        if (processChartList.length > 10) {
            processChartList = processChartList.slice(0, 10);
        }

        this._processChartListData.push(processChartList);
        this._processListData.push(processList);

    }

    render() {
    const {SimulationStore, ProcessModelStore } = this.props;
        return (
            <>
                <Card
                    // style={{height: 300, marginTop: 5}} title={'Simulation Log'} className={'logwidth'}
                    //   loading={ProcessModelStore.getAnimationLoading}
                >
                    {/*<div style={{height: 210, overflowY: "auto"}}>*/}
                    {/*    {SimulationStore.getProcessList.slice(0, (this.props.ProcessModelStore.getCount)).map(function (value: any, key: any) {*/}
                    {/*        const value2 = value.map(function(value:any){*/}
                    {/*            return value;*/}
                    {/*        })*/}
                    {/*        let colorNum: string = '#3F7FBF';*/}
                    {/*        let toDate: Date = new Date(1970, 0, 1); // Epoch*/}

                    {/*        if (value2[0].state != "OK") {*/}
                    {/*            colorNum = '#F87CCC';*/}
                    {/*            toDate.setSeconds(value2[0].ngTime);*/}

                    {/*            let ngTime: string = StringUtile.setDigit2(toDate.getHours()) + ":" + StringUtile.setDigit2(toDate.getMinutes()) + ":" + StringUtile.setDigit2(toDate.getSeconds());*/}

                    {/*            return <p key={key}><span key={key}*/}
                    {/*                                      style={{color: colorNum}}>Product Num:  {value2[0].state} , {ngTime} </span>*/}
                    {/*            </p>;*/}
                    {/*        } else {*/}
                    {/*            toDate.setSeconds(value2[0].totalTime);*/}
                    {/*            let totalTime: string = StringUtile.setDigit2(toDate.getHours()) + ":" + StringUtile.setDigit2(toDate.getMinutes()) + ":" + StringUtile.setDigit2(toDate.getSeconds());*/}

                    {/*            return <p key={key}><span key={key}*/}
                    {/*                                      style={{color: colorNum}}>Product Num: {value2[0].productIdx}, {totalTime} </span>*/}
                    {/*            </p>;*/}
                    {/*        }*/}

                    {/*    })}*/}
                    {/*</div>*/}
                    {ProcessModelStore.getCount}
                </Card>
            </>
        );
    };
}


export default SimLog;