import React, {Component, RefObject} from 'react';
import 'antd/dist/antd.css';
import {Card, Slider, Statistic} from 'antd';
import {ArgumentAxis, Chart, ConstantLine, Label, Legend, Series, Tooltip, ValueAxis} from 'devextreme-react/chart';
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import ProcessModelMain from "../../../../threeView/ProcessModelMain";
import SimulationTapsService from "../../Service/SimulationTapsService";
import {IProcessModelData, ISimFacilityNgCntChartData, SimulationMenuType} from "../../store/SimulationType";
import {SliderValue} from "antd/es/slider";
import StringUtile from "../../../../utile/StringUtile";
import PieChart, {Connector, Label as PieLabel, Series as PieSeries, Size} from 'devextreme-react/pie-chart';
import '../../../../css/IndexPage.css'
import {
    BtnTimeLine,
    SliderLayout,
    TimeControlBtnLayout,
    TimeLineControl,
    TImeLineLayout,
    TimeText
} from "../../store/SimStyle_Componet";
import ProcessModelStore from "../../store/ProcessModelStore";
import ReactInterval from "react-interval";
import {Icon} from "semantic-ui-react";


/**
 * @description SIMULATION 의 DASHBOARD를 담당하는 클래스
 */
@inject('SimulationStore', 'ProcessModelStore')
@observer
class DashBoard extends Component<any, any> {
    private _barChart: RefObject<any>;
    private _pieChart: RefObject<any>;

    private _threejsConvas: RefObject<ProcessModelMain>;
    private _oldSize: { width: number, height: number } = {width: 0, height: 0};
    private _timeValue: number = 0;
    private _speedValue: number = 1;
    private _timerPalyEnable: boolean = true;

    private _processListData: Array<any> = [];
    private _processChartListData: Array<any> = [];

    constructor(props: any) {
        super(props);

        this._barChart = React.createRef();
        this._pieChart = React.createRef();
        this._threejsConvas = React.createRef();
        this.props.setRenderEvent(this._renderEvent);
        this._renderEvent();
    }


    /**
     * @description Timeline의 초값을 가지고 온다. +재생이 끝나면 아이콘 멈춤 로직
     * @param nextCount Timeline 초값
     */
    @autobind
    private setTimeLineCount(nextCount: number) {
        const {ProcessModelStore, SimulationStore} = this.props;
        if (!(nextCount >= 0 && nextCount <= ProcessModelStore.getProcessAnimationTotalTime)) {
            return;
        }

        ProcessModelStore.setTimeValue(nextCount);
        if (this._threejsConvas.current != null) {
            this._threejsConvas.current.change(nextCount);
        }

        if (nextCount >= ProcessModelStore.getProcessAnimationTotalTime || SimulationStore.getPlayIcon == "pause") {
            ProcessModelStore.setCount(nextCount);
            this.props.SimulationStore.setPlayIcon('pause');
            this._timerPalyEnable = false;
        } else {
            ProcessModelStore.setCount(nextCount);
        }
    }

    @autobind
    private setInterval() {
        const {ProcessModelStore} = this.props;
        const nextCount: number = ProcessModelStore.getCount + this._speedValue;
        this.setTimeLineCount(nextCount);
    }

    @autobind
    private onTimeChange(value: SliderValue) {
        const nextCount: number = parseInt(value.toString());
        this.setTimeLineCount(nextCount);
    };

    @autobind
    private _btnStopClick() {
        const {SimulationStore} = this.props;
        this.setTimeLineCount(0);
        SimulationStore.setPlayIcon("pause");
        this._timerPalyEnable = false;
    }

    @autobind
    private _btnPlayClick() {
        const {SimulationStore} = this.props;
        let iconType: "play" | "pause" = "play";
        let timerPalyEnable: boolean = true;
        if (SimulationStore.getPlayIcon == "play") {
            iconType = "pause";
            timerPalyEnable = false;
        }
        SimulationStore.setPlayIcon(iconType);
        this._timerPalyEnable = timerPalyEnable;
    }

    @autobind
    private _renderEvent(type?: boolean): void {
        let modelLayout: any = document.getElementById("processModelContainer");
        if (modelLayout != undefined && this._threejsConvas != undefined) {
            let element: HTMLDivElement = modelLayout;
            let width: number = (element.clientWidth - 1);
            let height: number = (element.clientHeight - 7);

            if (type == false) {
                width = this._oldSize.width;
                height = this._oldSize.height;
            }

            if (this._threejsConvas.current != null) {
                this._threejsConvas.current.handleResize(width, height);
            }
        }
    }

    private _btnBackWardClick() {
        const speed: number = (this._speedValue * 3);
        if (speed > 1) {
            const result: number = speed - 1;
            this._speedValue = result;
        }
    }


    private _btnForwardClick() {
        const speed: number = this._speedValue;
        if (speed < 10) {
            const result: number = speed + 1;
            this._speedValue = result;
        }
    }


    componentWillUnmount() {
        const {ProcessModelStore, SimulationStore} = this.props;
        ProcessModelStore.setCount(0);

        this.setTimeLineCount(0);
    }

    public async componentDidMount() {
        const {SimulationStore, ProcessModelStore} = this.props;
        this._btnStopClick();
        ProcessModelStore.setLodingBox(true);
        SimulationStore.setTopMenuSelectView(SimulationMenuType.DASHBOARD);

        let modelLayout: any = document.getElementById("processModelContainer");
        // elementId가 "processModelContainer"인 div가 존재한다면...
        if (modelLayout != undefined && modelLayout != null) {
            let element: HTMLDivElement = modelLayout;
            this._oldSize = {width: (element.clientWidth - 2), height: (element.clientHeight - 7)};
        }


        const service: SimulationTapsService = new SimulationTapsService();
        let parameter: any = {
            simulationid: this.props.SimulationStore.getSimReqId,
            loclid: "KO"
        };

        try {
            //DashBoard 처음 열릴시에만 데이터 받기 null
            if (SimulationStore.getFacilityNgCntChartData == null) {
                // 시뮬레이션
                const resultData: any = await service.getFacilityNgCntChartData(parameter);
                SimulationStore.setFacilityNgCntChartData(resultData);
            }
        } catch (e) {
            console.log(e);
        }

        try {
            const {SimulationStore} = this.props;
            //DashBoard 처음 열릴시에만 데이터 받기 null
            if (SimulationStore.getDashboardChartData.simReqId == '') {

                const resultData2: any = await service.getSimulationViewData(parameter);
                SimulationStore.setDashboardChartData(resultData2);
            }
        } catch (e) {
            console.log(e);
        }
        let boardData = SimulationStore.getDashboardChartData;
        SimulationStore.setDashboardChartData(boardData);

        let chartItemName: { [index: string]: string } = {};
        let productCheckData: any = [];
        let threeData: Array<ISimFacilityNgCntChartData> = [];

        //설비별 생산량/불량건수 정보 가지고오기
        let chartData: any = SimulationStore.getFacilityNgCntChartData;
        // Id, Nm을 맵핑
        chartData.forEach(function (value: ISimFacilityNgCntChartData, key: number) {
            chartItemName[value.processId] = value.processNm;
            productCheckData.push(value.processId);

        });
        //설비 ID, NM 맵핑 STORE 저장 ex) 2982fdeb-4009-42d4-8036-bf9a47c1896f: "AOI 검사 1"
        SimulationStore.setChartItemName(chartItemName);
        //전체 processId 추출
        SimulationStore.setProductCheckData(productCheckData);

        // 체크테이블로 제품들을 표현하기 위한 메서드
        threeData = chartData.sort((leftItem: ISimFacilityNgCntChartData, rightItem: ISimFacilityNgCntChartData) => {
            return StringUtile.getFullTextSort(leftItem.processNm, rightItem.processNm);
        });
        SimulationStore.setTreeList(threeData);


        try {
            let resultData1: IProcessModelData = await service.getProcessModel(SimulationStore.getSimReqId);
            ProcessModelStore.setProcessModelData(resultData1);

            if (SimulationStore.getProcessList.length == 0) {
                this.setSimulationLog()
            }
        } catch (e) {
            console.log(e);
        }
        ProcessModelStore.setLodingBox(false);
    }

    /**
     * @iterator를 통해 log의 내용을 담은 값들을 store Mobx에 저장한다.
     */
    @autobind
    private setProcessData() {
        const {SimulationStore, ProcessModelStore} = this.props;
        let test= this._processListData.flat()
        let newPostList = test.sort((a:any, b:any)=>{
            if(a.totalTime < b.totalTime){
                return 1;
            }
            if (a.totalTime > b.totalTime) {
                return -1;
            }
            // a must be equal to b
            return 0;
        });

        SimulationStore.setProcessList(newPostList);
    }

    /**
     * @description SimulationStore에서 processList에 값이 [] 이면 log를 담는 아래 메서드 수행
     */
    @autobind
    private setSimulationLog() {
        const {ProcessModelStore} = this.props;
        ProcessModelStore.getProcessAnimation.map((value: any) => {
            let processList: Array<{ productIdx: number, state: string, totalTime: number, ngTime: number, stencNm: string }> = [];
            let startdate: Date = new Date(value.startTime);
            let startSeconds: number = startdate.getTime();
            let enddate = new Date(value.endTime);
            let endSeconds: number = enddate.getTime();

            let totalTime: number = startSeconds;
            let ngTime: number = 0;

            if(totalTime){
                processList.unshift({
                    productIdx: value.productIdx,
                    state: value.status,
                    totalTime: totalTime,
                    ngTime: ngTime,
                    stencNm: value.stencNm
                });
            }

            this._processListData.push(processList);
        });
        this.setProcessData();
    }

    render() {
        const {SimulationStore, ProcessModelStore} = this.props;
        const resultData = SimulationStore.getProcessList.filter((value:any) => value.totalTime <= ProcessModelStore.getCount / 10)
        return (
            <>
                <div style={{float: "left", width: '60%'}}>
                    <Card
                        loading={ProcessModelStore.getAnimation3dLoading}
                        style={{height: 350, marginTop: 10}}
                    >
                        <div id={"processModelContainer"}>

                            <ProcessModelMain modelInfo={ProcessModelStore.getModelListData}
                                              mapInfo={ProcessModelStore.getMapData}
                                              processAnimationData={ProcessModelStore.getProcessAnimation}
                                              SimulationStore={SimulationStore}
                                              ref={this._threejsConvas}
                            />

                            <TimeLineControl>
                                <ReactInterval timeout={100 / this._speedValue} enabled={this._timerPalyEnable}
                                               callback={() => {
                                                   this.setInterval()
                                               }}/>
                                <TImeLineLayout>
                                    <SliderLayout>
                                        <Slider style={{width: "83%", float: "left"}}
                                                defaultValue={ProcessModelStore.getTimeValue}
                                                value={ProcessModelStore.getTimeValue} onChange={this.onTimeChange}
                                                max={ProcessModelStore.getProcessAnimationTotalTime}
                                                tooltipVisible={false}
                                        />
                                        <TimeText>
                                            {StringUtile.setTimerhhmmss(ProcessModelStore.getProcessAnimationTotalTime / 10)}
                                        </TimeText>
                                    </SliderLayout>
                                </TImeLineLayout>
                            </TimeLineControl>
                        </div>
                        <div>
                            <TimeControlBtnLayout>
                                <BtnTimeLine onClick={() => this._btnBackWardClick()}> <Icon className={"backward"}/>
                                </BtnTimeLine>
                                <BtnTimeLine onClick={() => this._btnStopClick()}
                                             style={{marginBottom: 5, marginTop: 5}}><Icon
                                    className={"stop"}/></BtnTimeLine>
                                <BtnTimeLine onClick={() => this._btnPlayClick()}><Icon
                                    className={SimulationStore.getPlayIcon}/></BtnTimeLine>
                                <BtnTimeLine onClick={() => this._btnForwardClick()}><Icon
                                    className={"forward"}/></BtnTimeLine>

                                <span style={{marginLeft: 10}}>
                            {StringUtile.setTimerhhmmss(ProcessModelStore.getTimeValue / 10)}
                                    &nbsp;
                                    <BtnTimeLine>x {this._speedValue}</BtnTimeLine>
                        </span>
                            </TimeControlBtnLayout>
                        </div>
                    </Card>

                    <div style={{marginTop: 5, marginRight: 5}}>
                        <Card style={{width: "50%", float: "left"}} title={'설비별 생산량 / 불량건수'}
                              loading={ProcessModelStore.getFacilityChartLoading}
                        >
                            <Chart id="chart1" dataSource={SimulationStore.getFacilityNgCntChartData}
                                   style={{width: "100%", height: 229}}>
                                <ArgumentAxis>
                                    <Label customizeText={function (this: DashBoard, info: any) {
                                        if (info.valueText != "") {
                                            return `${SimulationStore.getChartItemName[info.valueText]}`;
                                        }
                                        return `${info.valueText}%`;
                                    }.bind(this)}/>
                                </ArgumentAxis>
                                <ValueAxis name={'frequency'} position={'left'}/>
                                <ValueAxis name={'percentage'} position={'right'} showZero={true}
                                           valueMarginsEnabled={false}>
                                    <Label customizeText={function customizePercentageText(info: any) {
                                        return `${info.valueText}%`;
                                    }}/>
                                    <ConstantLine value={80} width={2} color={'#fc3535'} dashStyle={'dash'}>
                                        <Label visible={false}/>
                                    </ConstantLine>
                                </ValueAxis>

                                <Series name={'생산량'} valueField={'okCount'} type={'bar'} color={'#42a5f5'}
                                        argumentField={'processId'}/>
                                <Series name={'불량건수'} valueField={'ngCount'} type={'bar'} color={'#ffd700'}
                                        argumentField={'processId'}/>

                                <Tooltip enabled={true} shared={true}/>

                                <Legend verticalAlignment={'top'} horizontalAlignment={'right'}/>
                            </Chart>
                        </Card>
                        <Card style={{width: "50%", float: "right"}} title={'생산량 / 불량건수'}
                              loading={ProcessModelStore.getDashboardChartLoading}
                        >
                            <PieChart key={'pichart'}
                                      ref={this._pieChart}
                                      style={{width: "100%", height: 229}}
                                      id={'pie'}
                                      dataSource={[
                                          {country: '생샨량', area: SimulationStore.getDashboardChartData.output},
                                          {country: '불량건수', area: SimulationStore.getDashboardChartData.ngCount},
                                      ]}>
                                <PieSeries argumentField={'country'} valueField={'area'}>
                                    <PieLabel visible={true}>
                                        <Connector visible={true} width={1}/>
                                    </PieLabel>
                                </PieSeries>
                                <Tooltip enabled={true} shared={true}/>
                                <Size/>
                            </PieChart>
                        </Card>
                    </div>
                </div>

                <div style={{float: 'right', width: '40%'}}>
                    <Card style={{marginTop: 10, marginLeft: 5}}>

                        <Card style={{width: "100%", padding: 0}}><h4>시뮬레이션 명
                            : {SimulationStore.getSimReqNm}</h4></Card>

                        <div style={{marginTop: 5}}>
                            <Card title={'Throught put'} style={{height: 120, width: "43%", float: "left",}}
                                  loading={ProcessModelStore.getDashboardChartLoading}
                                  bodyStyle={{paddingTop: 10, paddingLeft: 15}}
                            >
                                <Statistic className={'chartfontData'}
                                           style={{fontSize: 11}}
                                           value={SimulationStore.getDashboardChartData.output}
                                           valueStyle={{color: 'red'}}
                                    // prefix={<Icon type="arrow-up"/>}
                                           suffix="EA"
                                />
                            </Card>
                            <Card title={"Simulation Time"} style={{height: 120, width: "53%", float: "right"}}
                                  loading={ProcessModelStore.getDashboardChartLoading}
                                  bodyStyle={{paddingTop: 10, paddingLeft: 15}}
                            >
                                <Statistic
                                    value={SimulationStore.getDashboardChartData.analysisTime.toString()}
                                    valueStyle={{color: 'black'}}
                                    // prefix={<Icon type="arrow-up"/>}
                                    suffix="Sec"
                                />
                            </Card>
                        </div>
                        <div style={{marginTop: 5}}>
                            <Card title={"NG Count"} style={{height: 120, width: "43%", float: "left"}}
                                  loading={ProcessModelStore.getDashboardChartLoading}
                                  bodyStyle={{paddingTop: 10, paddingLeft: 15}}
                            >
                                <Statistic
                                    value={SimulationStore.getDashboardChartData.ngCount}
                                    valueStyle={{color: '#cf1322'}}
                                    // prefix={<Icon type="arrow-down"/>}
                                    suffix="EA"
                                />
                            </Card>
                            <Card title={"Warm-up Time"} style={{width: "53%", height: 120, float: "right"}}
                                  loading={ProcessModelStore.getDashboardChartLoading}
                                  bodyStyle={{paddingTop: 10, paddingLeft: 15}}
                            >
                                <Statistic
                                    value={SimulationStore.getDashboardChartData.warmUpTime.toString()}
                                    valueStyle={{color: '#3f8600'}}
                                    // prefix={<Icon type="arrow-up"/>}
                                    suffix="Sec"
                                />
                            </Card>
                        </div>
                    </Card>
                    <Card style={{height: 300, marginTop: 5}} title={'Simulation Log'} className={'logwidth'}>
                        <div style={{height: 210, overflowY: "auto"}}>
                            {resultData.map(function (value: any, key: number) {
                                let colorNum:string = '#3F7FBF';
                                let toDate:Date = new Date(1970, 0, 1); // Epoch
                                toDate.setSeconds(value.totalTime);
                                let totalTime:string = StringUtile.setDigit2(toDate.getHours()) +":" + StringUtile.setDigit2(toDate.getMinutes()) + ":"+ StringUtile.setDigit2(toDate.getSeconds());
                                if(value.state != "OK"){
                                    colorNum = '#F87CCC';
                                    return <p key={key}><span key={key} style={{color:colorNum}}>Product Num: {value.productIdx} ({value.state}), {totalTime}  , {value.stencNm}</span></p>;
                                }else{
                                    return <p key={key}><span key={key} style={{color:colorNum}}>Product Num: {value.productIdx}, {totalTime} , {value.stencNm}</span></p>;
                                }
                            })}
                        </div>
                    </Card>
                </div>
            </>
        );
    };
}


export default DashBoard;