import React, {Component, CSSProperties, RefObject} from 'react';
import 'antd/dist/antd.css';
import {
    ChartBox,
    ChartContendBox,
    CheckBoxContend,
    OptionButton,
    SortContend,
    TitleText
} from "../../store/SimStyle_Componet";
import {Button, Card, Col, Row, Select, Table, Tree} from "antd";
import {ArgumentAxis, Label, Legend, Series, ValueAxis, Tooltip, Size} from "devextreme-react/chart";
import autobind from "autobind-decorator";
import {ISimFacilityNgCntChartData, SimulationFormType, SimulationTreeKey} from "../../store/SimulationType";
import {inject, observer} from "mobx-react";
import html2canvas from "html2canvas";
import StringUtile from "../../../../utile/StringUtile";
import Searvice from "../../../../common/Searvice";
import {Route, Switch} from "react-router";
import {Link} from "react-router-dom";
import {ColumnProps} from "antd/es/table";
import SimulationStore from "../../store/SimulationStore";

/**
 * @description SIMULATION 의 제품별생산량 담당하는 클래스
 */
@inject('SimulationStore','ExcelStore')
@observer
class ProductOutput extends Component<any, any> {
    private textSortAttrName: string = "simLoadingTimeHour";
    private _barChart: RefObject<any> = React.createRef();
    private offFullScreen: CSSProperties = {height: "calc(100% - 54px)"};
    private onFullScreen: CSSProperties = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
        zIndex: 999,
        backgroundColor: "#ffffff"
    };
    private readonly _dataGridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [
        {
            title: '설비명',
            dataIndex: 'processNm',
            key: 'processNm'
        },
        {
            title: '생산량',
            dataIndex: 'okCount',
            key: 'okCount',
            align: "right",
            width: '100px'
        }
    ];
    constructor(props: any) {
        super(props);

        this.state = {
            chartPngDownloadData: "",
            fullscreenType: false,
            renderEvent: function (type: boolean) {
            }
        };
    }

    @autobind
    private _renderEvent(type?: boolean): void {
        let barChart: any = this._barChart.current.instance;
        barChart.render();
        barChart.render();
    }

    /**
     * @description 선택한 정렬옵션을 적용하는 메서드
     * @param value 선택한 정렬옵현 (NAME | ASC | DESC)
     */
    @autobind
    private onChangeSortName(value:string):void{
        this.props.SimulationStore.setAlign(value);
    }

    /**
     * @description 제품선택시 체크한 제품만 출력하기 위한 메서드
     * @param checkedKeys 체크된 해당제품
     */
    @autobind
    private onCheck(checkedKeys: any):void{
        this.props.SimulationStore.setCheck(checkedKeys);
    };

    /**
     * @description 차트를 이미지로 저장하는 메서드
     * @param event 마우스 클릭 이벤트에대한 정보
     */
    @autobind
    private onClickChartDownload(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void {
        let element: HTMLElement | null = document.getElementById("productOutputChartContent");
        let hideDownloadLink: HTMLElement | null = document.getElementById("productOutputChartDownloadLink");
        if (element != null) {
            let setSteteCompleteEvent: () => void = function () {
                if (hideDownloadLink != null) {
                    hideDownloadLink.click();
                }
            }.bind(this);

            let callback: (this: ProductOutput, canvas: HTMLCanvasElement) => void = function (this: ProductOutput, canvas: HTMLCanvasElement) {
                this.setState({
                        chartPngDownloadData:canvas.toDataURL("image/png")
                    },
                    setSteteCompleteEvent);
            }.bind(this);
            html2canvas(element).then(callback);
        }
    }

    /**
     * @description 컴포넌트가 열릴때 설비별생산량을 조회후 출력 및 설정하는 메서드
     */
    public async componentDidMount() {
        this.setRenderEvent(this._renderEvent);
        const {SimulationStore} = this.props;
        SimulationStore.setProductOutputSortname("name");
        SimulationStore.setCheckedKeys(SimulationStore.getProductCheckData);

        SimulationStore.setProductOutputSelectionView(SimulationFormType.CHART);
        if (SimulationStore.getSimReqId != "") {
            let parameter: any = {
                simulationid: SimulationStore.getSimReqId,
                loclid: "KO"
            };
        }
        
       const reData =  SimulationStore.getFacilityNgCntChartData;
        //제품 체크후에 데이터 다시 표출하기위한 메서드(object.assign()메서드를 위한...)
        SimulationStore.setOrgChartData(reData);
        //설정후 다시 차트에 표출하기위한 데이터
        SimulationStore.setChartData(reData);
        SimulationStore.setAlign("name");
    }
    /**
     * @description 테이블(표)를 클릭시 출력하기위한 triggerf
     * @param event 마우스 클릭시 이벤트
     */
    @autobind
    private onClickBtnChartTable(event: React.MouseEvent<HTMLButtonElement>): void {
        this.props.SimulationStore.setProductOutputSelectionView(event.currentTarget.name);
    }

    /**
     * @description 버튼 클릭시 시뮬레이션정보를 전체화면으로 보는 메서드
     */
    @autobind
    private onClickEventFullScreen(): void {

        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;

        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }
    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function): void {
        this.setState({renderEvent: callBack});
    }
    /** @description 차트 테이블을 엑셀파일로 다운로드 */
    @autobind
    private onClickBtnExcelDownload() {
        const {SimulationStore} = this.props;
        const checkedkeys = SimulationStore.getCheckedKeys;
        const sortname = SimulationStore.getProductOutputSortname;
        this.props.ExcelStore.productoutputExcel(this.props.SimulationStore.getSimReqId, checkedkeys, sortname);
    }
    render() {
        const {SimulationStore} = this.props;
        return (
            <>
                <div style={{width:'77%', float:'left'}}>
                    <a href={this.state.chartPngDownloadData} download={"TimeBufferUsage.png"} style={{display: "none"}}
                       id="productOutputChartDownloadLink"></a>
                            <ChartBox
                                title={'제품별 생산량'}
                                type={"inner"}
                                bodyStyle={(this.state.fullscreenType == true) ? this.onFullScreen : this.offFullScreen}
                                extra={[
                                    <OptionButton key="1" size={"small"}onClick={this.onClickEventFullScreen}>크게보기</OptionButton>,
                                    <Switch>
                                        <Route path={`${this.props.routerData.match.url}/table`}>
                                            <OptionButton key="2" size={"small"}onClick={this.onClickBtnExcelDownload}>
                                                엑셀 다운로드
                                            </OptionButton>
                                        </Route>
                                        <Route path={`${this.props.routerData.match.url}`}>
                                            <OptionButton key="2" size={"small"}onClick={this.onClickChartDownload}>
                                                차트 다운로드
                                            </OptionButton>
                                        </Route>
                                    </Switch>
                                ]}>

                                <Switch>
                                    <Route path={`${this.props.routerData.match.url}/table`}>
                                        <div>
                                            {(this.state.fullscreenType == true) ? <Button key="1" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40, marginBottom:70, marginTop:5}}>원본크기</Button> : null}

                                            <Table columns={this._dataGridCoulumn}
                                                   dataSource={SimulationStore.getChartData}
                                                   scroll={{y: 460}}
                                                   size={"middle"} bordered={true}/>
                                        </div>
                                    </Route>
                                    <Route path={`${this.props.routerData.match.url}`}>
                                        <TitleText id="productOutputChartContent">

                                            {(this.state.fullscreenType == true) ? <Button key="2" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40}}>원본크기</Button> : null}
                                            <ChartContendBox ref={this._barChart}
                                                             title={'제품별 생산량'}
                                                             dataSource={SimulationStore.getChartData}
                                                             id={'chart'}
                                                             style={{height:682}}
                                            >
                                                {(this.state.fullscreenType == true) ?  <Size height={700} /> : <Size height={675} width={"100%"}/>}
                                                <ArgumentAxis>
                                                    <Label customizeText={function(this:ProductOutput , info:any) {
                                                        if(info.valueText != ""){
                                                            return `${SimulationStore.getChartItemName[info.valueText]}`;
                                                        }
                                                        return `${info.valueText}`;
                                                    }.bind(this)} />
                                                </ArgumentAxis>
                                                <ValueAxis name={'frequency'} position={'left'}/>

                                                <Series name={'생산건수'} valueField={'okCount'} type={'bar'} color={'#42a5f5'}
                                                        argumentField={'processId'}/>

                                                <Tooltip enabled={true} shared={true} />
                                                <Legend verticalAlignment={'top'} horizontalAlignment={'right'}/>
                                            </ChartContendBox>
                                        </TitleText>
                                    </Route>
                                </Switch>
                            </ChartBox>
                        </div>
                <div style={{width:' 22%', float:'right'}}>
                            <Card title={'결과양식'} type={"inner"}>
                                <Link to={`${this.props.routerData.match.url}`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)", marginRight: "10px"}}
                                        type={(SimulationStore.getProductOutputSelectionView == SimulationFormType.CHART) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.CHART}>Chart</Button>
                                </Link>
                                <Link to={`${this.props.routerData.match.url}/table`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)"}}
                                        type={(SimulationStore.getProductOutputSelectionView == SimulationFormType.TABLE) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.TABLE}>Table</Button>
                                </Link>
                            </Card>

                            <CheckBoxContend title={'제품'} type={"inner"}
                                             bodyStyle={{
                                                 overflowY: "auto",
                                                 padding: "0px"
                                             }}>
                                <Tree
                                    checkable
                                    defaultExpandedKeys={['all']}
                                    defaultCheckedKeys={['all']}
                                    onCheck={this.onCheck}
                                >
                                    <Tree.TreeNode title="All" key="all">
                                        {SimulationStore.getTreeList.map(function(item:ISimFacilityNgCntChartData){
                                            return <Tree.TreeNode title={item.processNm} key={item.processId} />;
                                        })}
                                    </Tree.TreeNode>
                                </Tree>
                            </CheckBoxContend>
                            <SortContend title={'정렬'} type={"inner"}>
                                <Select defaultValue={SimulationTreeKey.NAME} size={"default"} style={{width: "100%"}} onChange={this.onChangeSortName}>
                                    <Select.Option value={SimulationTreeKey.NAME}>이름순</Select.Option>
                                    <Select.Option value={SimulationTreeKey.ASC}>오름차순</Select.Option>
                                    <Select.Option value={SimulationTreeKey.DESC}>내림차순</Select.Option>
                                </Select>
                                <br />
                            </SortContend>
                </div>
            </>
        );
    };
}

export default ProductOutput;
