import React, {Component, CSSProperties, RefObject} from 'react';
import 'antd/dist/antd.css';
import {ChartBox, ChartContendBox, OptionButton, TBUSortContend, TitleText} from "../../store/SimStyle_Componet";
import {Button, Card, Col, Row, Select, Table} from "antd";
import {
    ArgumentAxis,
    ConstantLine,
    Label,
    Legend,
    Series,
    ValueAxis,
    Tooltip,
    CommonSeriesSettings, Size
} from "devextreme-react/chart";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import html2canvas from "html2canvas";
import {SimulationFormType} from "../../store/SimulationType";
import {Route, Switch} from "react-router";
import {Link} from "react-router-dom";
import {ColumnProps} from "antd/es/table";
import SimulationTapsService from "../../Service/SimulationTapsService";

/**
 * @description SIMULATION 의 버퍼사용량을 담당하는 클래스
 */
@inject('SimulationStore', 'ExcelStore')
@observer
class TimeBufferUsage extends Component<any, any> {
    private textSortAttrName: string = "simLoadingTimeHour";
    private _barChart: RefObject<any> = React.createRef();

    private offFullScreen: CSSProperties = {height: "calc(100% - 54px)"};
    private onFullScreen: CSSProperties = {
        position: "fixed",
        top: "0px",
        right: "0px",
        bottom: "0px",
        left: "0px",
        zIndex: 999,
        backgroundColor: "#ffffff"
    };
    private readonly _dataGridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [
        {
            title: '시간대',
            dataIndex: 'simLoadingTimeHour',
            key: 'simLoadingTimeHour'
        },
        {
            title: '버퍼 사용량',
            dataIndex: 'conveyorCountForHour',
            key: 'conveyorCountForHour',
            align: "right",
            width: '100px'
        }
    ];
    constructor(props: any) {
        super(props);

        this.state = {
            chartPngDownloadData: "",
            fullscreenType: false,
            renderEvent: function (type: boolean) {
            }
        };
    }
    @autobind
    private _renderEvent(type?: boolean): void {
        let barChart: any = this._barChart.current.instance;
        barChart.render();
        barChart.render();
    }

    /**
     * @description 정렬선택버튼으로 정렬 타입이 변할시 chart를 해당 정렬로 바꾸어주는 Method
     * @param value 정렬형태를 바꿀 정렬설정 버튼값
     */
    private selectChange = (value: string) => {
        const {SimulationStore} = this.props;
        SimulationStore.setSortName(value);
        SimulationStore.setTimeBufferUserData(SimulationStore.commonlogic);
    };

    /**
     * @description 시간별사용량 컴포넌트가 열릴때 구동합니다.
     * 시간별 버퍼 사용량 data를 조회하는 Run Method
     */
    public async componentDidMount() {
        this.setRenderEvent(this._renderEvent);
        this.props.routerData.match.url
        const {SimulationStore, routerData} = this.props;
        const service: SimulationTapsService = new SimulationTapsService();
        SimulationStore.setTimeBufferChartLoading(true);
        SimulationStore.setSortName('buf');
        SimulationStore.setTimeBufferUsageSelectionView(SimulationFormType.CHART);
        let parameter: any = {
            simulationid: this.props.SimulationStore.getSimReqId,
            loclid: "KO"
        };
        if(SimulationStore.getTimeBufferUserData == null) {
            const resultData = await service.getTimeBufferUser(parameter);
            SimulationStore.setTimeBufferUserData(resultData);
        }
        SimulationStore.setTimeBufferUserData(SimulationStore.commonlogic);
        SimulationStore.setTimeBufferChartLoading(false);
    };

    /**
     * @description 조회된 chart표를 이미지(jpg)파일로 저장하는 run Method
     * @param event 마우스 클릭시에 반응하는 event파라미터
     */
    @autobind
    private onClickChartDownload(event: React.MouseEvent<HTMLButtonElement, MouseEvent>): void {



        let element: HTMLElement | null = document.getElementById("timeBufferUsageChartContent");
        let hideDownloadLink: HTMLElement | null = document.getElementById("timeBufferUsageChartDownloadLink");
        if (element != null) {
            let setSteteCompleteEvent: () => void = function () {
                if (hideDownloadLink != null) {
                    hideDownloadLink.click();
                }
            }.bind(this);

            let callback: (this: TimeBufferUsage, canvas: HTMLCanvasElement) => void = function (this: TimeBufferUsage, canvas: HTMLCanvasElement) {
                this.setState({
                        chartPngDownloadData:canvas.toDataURL("image/png")
                    },
                    setSteteCompleteEvent);
            }.bind(this);
            html2canvas(element).then(callback);
        }
    }
    /**
     * @description 테이블(표)를 클릭시 출력하기위한 trigger
     * @param event 마우스 클릭시 이벤트
     */
    @autobind
    private onClickBtnChartTable(event: React.MouseEvent<HTMLButtonElement>): void {
        const {SimulationStore} = this.props;
        SimulationStore.setTimeBufferUsageSelectionView(event.currentTarget.name);
    }
    /** @description 각 메뉴 컴포넌트에서 반환되는 renderEvent를 콜백받는 메서드 */
    @autobind
    private setRenderEvent(callBack: Function): void {
        this.setState({renderEvent: callBack});
    }

    /**
     * @description 버튼 클릭시 시뮬레이션정보를 전체화면으로 보는 메서드
     */
    @autobind
    private onClickEventFullScreen(): void {

        const type: boolean = !this.state.fullscreenType;
        const callBack: Function = this.state.renderEvent;

        this.setState({fullscreenType: type}, function () {
            if (callBack != undefined) {
                callBack(type);
            }
        });
    }
    /** @description 차트 테이블을 엑셀파일로 다운로드 */
    @autobind
    private onClickBtnExcelDownload() {
        const {SimulationStore} = this.props;
        const sortname = SimulationStore.getSortName
        this.props.ExcelStore.timebufferusageExcel(SimulationStore.getSimReqId, sortname);
    }
    render() {
        const {SimulationStore} = this.props;
        return (
            <>
                <div style={{width:'77%', float:'left'}}>
                    <a href={this.state.chartPngDownloadData} download={"TimeBufferUsage.png"} style={{display: "none"}}
                       id="timeBufferUsageChartDownloadLink"></a>
                            <ChartBox
                                title={'버퍼 사용량'}
                                type={"inner"}
                                bodyStyle={(this.state.fullscreenType == true) ? this.onFullScreen : this.offFullScreen}
                                extra={[
                                    <OptionButton key="1" size={"small"}onClick={this.onClickEventFullScreen}>크게보기</OptionButton>,
                                    <Switch>
                                        <Route path={`${this.props.routerData.match.url}/table`}>
                                            <OptionButton key="2" size={"small"}onClick={this.onClickBtnExcelDownload}>
                                                엑셀 다운로드
                                            </OptionButton>
                                        </Route>
                                        <Route path={`${this.props.routerData.match.url}`}>
                                            <OptionButton key="2" size={"small"}onClick={this.onClickChartDownload}>
                                                차트 다운로드
                                            </OptionButton>
                                        </Route>
                                    </Switch>
                                ]}
                                loading={SimulationStore.getTimeBufferChartLoading}>
                                <Switch>
                                    <Route path={`${this.props.routerData.match.url}/table`}>
                                        <div>
                                            {(this.state.fullscreenType == true) ? <Button key="1" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:40, marginBottom:70, marginTop:5}}>원본크기</Button> : null}

                                            <Table columns={this._dataGridCoulumn}
                                               dataSource={SimulationStore.getTimeBufferUserData}
                                               scroll={{y: 460}}
                                               size={"middle"} bordered={true}/>
                                        </div>
                                    </Route>
                                    <Route path={`${this.props.routerData.match.url}`}>
                                        <TitleText id={"timeBufferUsageChartContent"}>
                                            {(this.state.fullscreenType == true) ? <Button key="2" size={"small"}onClick={this.onClickEventFullScreen}
                                                                                           type={"primary"} style={{width:200, height:50}}>원본크기</Button> : null}
                                            <ChartContendBox ref={this._barChart} title={'버퍼 사용량'}
                                                             dataSource={SimulationStore.getTimeBufferUserData}
                                                             id={'chart'}>
                                                {(this.state.fullscreenType == true) ?  <Size height={700} /> : <Size height={675} width={"100%"}/>}


                                                <ArgumentAxis>
                                                    <Label overlappingBehavior={'simLoadingTimeHour'}/>
                                                </ArgumentAxis>

                                                <ValueAxis name={'frequency'} position={'left'}/>
                                                <ValueAxis name={'percentage'} position={'right'} showZero={true}
                                                           valueMarginsEnabled={false}>
                                                    <Label customizeText={function customizePercentageText(info: any) {
                                                        return `${info.valueText}%`;
                                                    }}/>
                                                    <ConstantLine value={80} width={2} color={'#fc3535'} dashStyle={'dash'}>
                                                        <Label visible={false}/>
                                                    </ConstantLine>
                                                </ValueAxis>

                                                <Series
                                                    name={'버퍼 사용량'}
                                                    valueField="conveyorCountForHour" type={'bar'}
                                                    argumentField="simLoadingTimeHour"/>

                                                <Tooltip enabled={true} shared={true}/>

                                                <Legend verticalAlignment={'top'} horizontalAlignment={'right'}/>
                                            </ChartContendBox>
                                        </TitleText>
                                    </Route>
                                </Switch>

                            </ChartBox>
                </div>
                <div style={{width:' 22%', float:'right'}}>
                            <Card title={'결과양식'} type={"inner"} id={"one"}>
                                <Link to={`${this.props.routerData.match.url}`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)", marginRight: "10px"}}
                                        type={(SimulationStore.getTimeBufferUsageSelectionView == SimulationFormType.CHART) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.CHART}>Chart</Button>
                                </Link>
                                <Link to={`${this.props.routerData.match.url}/table`}>
                                    <Button
                                        style={{width: "calc(50% - 5px)"}}
                                        type={(SimulationStore.getTimeBufferUsageSelectionView == SimulationFormType.TABLE) ? "primary" : "default"}
                                        size={"small"}
                                        onClick={this.onClickBtnChartTable}
                                        name={SimulationFormType.TABLE}>Table</Button>
                                </Link>
                            </Card>
                            <Card title={'정렬'} type={"inner"} id={"two"}>
                                <Select defaultValue={"buf"} style={{width: "100%"}} size={"default"}
                                        onChange={this.selectChange}>
                                    <Select.Option value="buf">버퍼량</Select.Option>
                                    <Select.Option value="debuf">버퍼역순</Select.Option>
                                    <Select.Option value="asc">오름차순(시간)</Select.Option>
                                    <Select.Option value="desc">내림차순(시간)</Select.Option>
                                </Select>
                            </Card>
                </div>
            </>
        );
    };
}

export default TimeBufferUsage;
