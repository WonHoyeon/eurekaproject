import React, {Component} from 'react';
import 'antd/dist/antd.css';
import '../../../../css/IndexPage.css';
import moment from 'moment';

import {Button, DatePicker, Input, message, PageHeader, Radio, Select} from 'antd';
import {SimulationListBtnType, SimulationType} from "../../store/SimulationType";
import {inject, observer} from "mobx-react";
import {RadioChangeEvent} from "antd/es/radio";
import autobind from "autobind-decorator";

/**
 * @description SIMULATION 시뮬레이션 조회리스트의 검색 옵션과 검색기능을 담당하는 클래스
 */
@inject('SimulationStore')
@observer
class SimulationList extends Component<any, any> {


    componentDidMount() {
        this.onResetEvent();
    }

    /**
     * @description Search 버튼 클릭시 조건에 맞는 검색리스트를 출력 Run Method
     * @param event 입력(input)..etc 등 이벤트를 작동하지 못하게 하기위한 event parameter
     */
    @autobind
    private onClickBtnSearch(event: React.MouseEvent<HTMLInputElement>): void {
        const {SimulationStore} = this.props;
        SimulationStore.setSimListLoading(true);
        this.props.onSearchEvent();
    };


    /**
     * @description Header 검색 입력창에 입력한 값들을 초기화 하는 Method
     */
    private onResetEvent = () => {
        this.props.SimulationStore.onReset();
    };


    /**
     * @description 검색조건중 시뮬레이션 요청일중 요청시작날짜를 설정하는 Method
     * @param dateString UI날짜 입력란에서 받은 날짜값
     */
    private onChangeFromDate = (date: moment.Moment | null, dateString: string) => {
        this.props.SimulationStore.setFromDate(dateString);
    };

    /**'
     * @description 검색조건중 시뮬레이션 요청일중 요청시작날짜를 설정하는 Method
     * @param dateString UI날짜 입력란에서 받은 날짜값
     */
    private onChangeToDate = (date: moment.Moment | null, dateString: string) => {
        this.props.SimulationStore.setToDate(dateString);
    };

    private onChangeDateBtnList = (event: RadioChangeEvent) => {
        const {SimulationStore} = this.props;
        SimulationStore.setSelectionBtn(event);
        if (SimulationStore.getReturnMessage === true) {
            message.warning('날짜가 올바르지 않습니다.');
            return;
        }
    };


    private onChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.SimulationStore.setSearch(e.target.value);
    };
    private onChangeSimulationType = (value: SimulationType) => {
        this.props.SimulationStore.setSimulationType(value);
    };

    private onChangeUserName = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.SimulationStore.setUserName(e.target.value);
    };

    render() {
        const {SimulationStore} = this.props;
        return (

            <PageHeader
                title="Simulation List"
                subTitle="sub simulation list"
                style={{backgroundColor:'white'}}
            >

                <div>
                    <div>
                        <div className={'simulationName'}>
                            <span>
                                시뮬레이션 이름 &nbsp; : &nbsp;
                            </span>
                            <Input placeholder={'시뮬레이션 이름을 입력하세요'}
                                   value={SimulationStore.getSearch}
                                   onChange={this.onChangeSearch}
                                   className={"simNmInput"}
                            />
                        </div>
                        <div>
                            <div className={'simulationState'}>
                            <span>
                                상태 &nbsp; : &nbsp;
                            </span>
                                <span>
                                <Select value={SimulationStore.getSimulationType}
                                        onChange={this.onChangeSimulationType} className={"state"}>
                                    <Select.Option value={SimulationType.ALL}>ALL</Select.Option>
                                    <Select.Option value={SimulationType.ST_Completed}>시뮬레이션 완성</Select.Option>
                                    <Select.Option value={SimulationType.ST_Request}>시뮬레이션 요청</Select.Option>
                                    <Select.Option value={SimulationType.ST_Running}>시뮬레이션 진행</Select.Option>
                                    <Select.Option value={SimulationType.ST_SimulationNG}>시뮬레이션 NG</Select.Option>
                                </Select>
                            </span>
                            </div>
                        </div>
                    </div>

                    <div className={"submitDiv"}>
                            <span>
                                    <Button className={"submit"} type={"primary"} icon="search"
                                            onClick={this.onClickBtnSearch}>조회</Button>
                                </span>
                    </div>
                </div>


                <div style={{marginTop: 55}}>

                    <div className={"orderer"}>
                    <span>
                        요청자 &nbsp; : &nbsp;
                    </span>
                        <Input placeholder={'요청자 이름을 입력하세요'}
                               value={SimulationStore.getUserName}
                               onChange={this.onChangeUserName}
                               className={"order"}

                        />
                    </div>

                    <div className={"pickerDiv"}>
                    <span style={{visibility: "visible"}}>
                        요청일 : &nbsp; &nbsp;
                        </span>
                        <DatePicker value={moment(SimulationStore.getFromDate)}
                                    onChange={this.onChangeFromDate}
                                    className={"picker"}
                        />
                        <span>
                        ~
                    </span>
                        <DatePicker value={moment(SimulationStore.getToDate)}
                                    onChange={this.onChangeToDate}
                                    className={"picker"}
                        />
                    </div>

                    <Radio.Group defaultValue={SimulationListBtnType.WEEK} value={SimulationStore.getSelectionBtn}
                                 buttonStyle="solid" onChange={this.onChangeDateBtnList} className={"pickerDate"}
                                 style={{marginTop: 0}}>
                        <Radio.Button value={SimulationListBtnType.WEEK}
                                      style={{textAlign: "center"}}>1주</Radio.Button>
                        <Radio.Button value={SimulationListBtnType.ONEMONTH}
                                      style={{textAlign: "center"}}>1개월</Radio.Button>
                        <Radio.Button value={SimulationListBtnType.THREEMONTH}
                                      style={{textAlign: "center"}}>3개월</Radio.Button>
                        <Radio.Button value={SimulationListBtnType.YEAR}
                                      style={{textAlign: "center"}}>1년</Radio.Button>
                    </Radio.Group>

                </div>



                <div className={'submitDiv'}>
                            <span>
                                    <Button className={"submit"} type={"danger"}
                                            onClick={this.onClickBtnSearch}>초기화</Button>
                                </span>
                </div>


                <div className={'submitDiv2'}>
                    <Button className={"submit2"} type={"primary"} icon="search"
                            onClick={this.onClickBtnSearch}>조회</Button>

                    <Button className={"submit2"} type={"danger"}
                            onClick={this.onClickBtnSearch}>초기화</Button>
                </div>
            </PageHeader>
        );
    };
}

export default SimulationList;
