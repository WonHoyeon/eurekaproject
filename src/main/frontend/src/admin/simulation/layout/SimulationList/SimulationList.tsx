import React, {Component} from 'react';
import 'antd/dist/antd.css';
import SimulationListHeader from "./SimulationListHeader";
import {Button, Select, Table} from "antd";
import {inject, observer} from "mobx-react";
import autobind from "autobind-decorator";
import {ColumnProps} from "antd/es/table";
import {ISimulationListDataSource} from "../../store/SimulationType";
import {Link} from "react-router-dom";
import Searvice from "../../../../common/Searvice";


/**
 * ▒▒▒시뮬레이션 리스트▒▒▒
 * ```
 * 시뮬레이션 리스트 출력, 해당리스트 클릭시 DashBoard 이동
 * ```
 * SimulationStore, ProcessModelStore Mobx 적용
 */
@inject('SimulationStore', 'ProcessModelStore')
@observer
class SimulationList extends Component<any, any> {

    private _gridCoulumn: ColumnProps<{ title: string, dataIndex: string, key: string }>[] = [{
        title: '동작 명',
        dataIndex: 'modelNm',
        key: 'modelNm',
        width: '38%'
    }, {
        title: '로그',
        dataIndex: 'simReqNm',
        width: '38%',
        key: 'simReqNm',
        render: function (text: any, record: any) {
            const item: ISimulationListDataSource = record;
            return <Link to={"/Main/SimulationPage/dashboard/" + item.simReqId}>{text}</Link>
        },
        onCell: (recode: any, rowIndex: number) => {
            const data: ISimulationListDataSource = recode;
            return {
                onClick: (event: any) => {
                    this.props.SimulationStore.setSimReqId(data.simReqId);
                    this.props.SimulationStore.setSimReqNm(data.simReqNm);
                }
            }
        }
    },{
        title: '날짜',
        dataIndex: 'simReqTpNm',
        key: 'simReqTpNm',
        align: "center"
    }];

    componentDidMount(): void {
        const {SimulationStore, ProcessModelStore} = this.props;
        let test = window.location.href;
        ProcessModelStore.setProcessAnimation(null);
        SimulationStore.deleteSimulationDataStore(test);
    }


    /**
     * @description 페이지 리스트에 시뮬레이션 information 출력시 row열의 갯수를 설정하는 Method
     * @param value 페이지 리스트 출력시 한페이지에 row열수를 나타낼 값
     */
    @autobind
    private onChangeBtnListPaging(value: string, option: React.ReactElement<any> | React.ReactElement<any>[]): void {
        this.props.SimulationStore.setListPaging(value);
    }

    /**
     * @description SimulationListHeader 에서 조회버튼 클릭시 실행되는 메서드
     */
    @autobind
    private ajaxCallSimulationList() {
        this.props.SimulationStore.printList();
        this.props.SimulationStore.setSimListLoading(false);
    }


    /**
     * @description 출력한 내용을 엑셀로 다운로드 Run Method
     */
    @autobind
    private onClickBtnExcelDownload() {
        const {SimulationStore} = this.props;
        if (SimulationStore.getSimulationList != undefined) {
            Searvice.getFileDownload("sim/getSimulationListExcelReport.do", SimulationStore.sendParameter);
        }
    }


    render() {
        const {Option} = Select;
        const {SimulationStore} = this.props;
        return (
            <div>
                {/*<div>*/}
                {/*    <SimulationListHeader onSearchEvent={this.ajaxCallSimulationList}/>*/}
                {/*    <Select defaultValue={SimulationStore.getListPaging} onChange={this.onChangeBtnListPaging}*/}
                {/*            style={{width: 100, marginTop:15}}>*/}
                {/*        <Option value={10}>10</Option>*/}
                {/*        <Option value={20}>20</Option>*/}
                {/*        <Option value={30}>30</Option>*/}
                {/*    </Select>*/}
                {/*    <div style={{float: "right", marginTop:15}}>*/}
                {/*        <Button type={"dashed"} onClick={this.onClickBtnExcelDownload}>엑셀 다운로드</Button>*/}
                {/*    </div>*/}


                {/*</div>*/}
                {/*<div>*/}
                {/*    <Table columns={this._gridCoulumn} dataSource={SimulationStore.getSimulationList}*/}
                {/*           loading={SimulationStore.getSimListLoading}*/}
                {/*           bordered={true}*/}
                {/*           rowKey={((record:any, index:any) => index)}*/}
                {/*           scroll={{y: 1000}}*/}
                {/*           size={"middle"}*/}
                {/*           pagination={{pageSize: SimulationStore.getListPaging}}*/}
                {/*    />*/}
                {/*</div>*/}
            </div>
        );
    };
}

export default SimulationList;
