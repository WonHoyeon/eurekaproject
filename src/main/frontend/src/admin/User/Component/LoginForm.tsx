import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Card, Icon, Menu, message, Tooltip} from "antd";
import {Credentials, IdRemeber, LoginButton, LoginInput} from "../typescript/UserStyled";
import {Link} from "react-router-dom";
import autobind from "autobind-decorator";
import Searvice from "../../../common/Searvice";



@inject('user')
@observer
class LoginForm extends Component<any, any> {
    state = {
        current: 'user',
    };

    componentDidMount(){
        // const NodeRSA = require('node-rsa');
        // const key = new NodeRSA({b: 512});
        // const text = 'Hello RSA!';
        // const encrypted = key.encrypt(text, 'base64');
        // console.log('encrypted: ', encrypted);
        // const decrypted = key.decrypt(encrypted, 'utf8');
        // console.log('decrypted: ', decrypted);
    }

    /**
     * @param e 로그인 입력Id
     * @constructor 입력하는 로그인값 받기
     */
    @autobind
    private InputId(e:React.FormEvent<HTMLInputElement>){

        this.props.user.setLoginId(e.currentTarget.value);
    }

    /**
     * @param e 로그인 입력Pw
     * @constructor 입력하는 패스워드값 받기
     */
    @autobind
    private InputPw(e:React.FormEvent<HTMLInputElement>){
        this.props.user.setLoginPw(e.currentTarget.value);
    }

    /**
     * @constructor 로그인하기
     */
    @autobind
    private async SubmitLogin(){
        const parameter = this.props.user.submitLogin
        try {
           const resultData = await Searvice.getPost("user/userLogin.do", parameter);
            if(resultData.data.msg == '성공'){
                sessionStorage.setItem('loggedInfo', parameter.id);
                window.location.replace("/eureka/Main/");
            }else{
                message.error('잘못된 정보입니다.');
            }
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        return (
            <div>
                <Credentials selectedKeys={[this.state.current]} mode="horizontal">
                    <Menu.Item key="user">
                        <Icon type="user"/>
                        Credentials
                    </Menu.Item>
                </Credentials>
                <Card style={{backgroundColor: '#f0f2f5'}}>
                    <LoginInput
                        onChange={this.InputId}
                        placeholder="Enter your username"
                        prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        suffix={<Tooltip title="Enter yout ID please">
                            <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                        </Tooltip>}/>
                    <LoginInput
                        onChange={this.InputPw}
                        type={'password'}
                        placeholder="Enter your password"
                        prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        suffix={<Tooltip title="Enter yout PassWord please">
                            <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                        </Tooltip>}/>
                    <div style={{marginTop: 25}}>
                        <IdRemeber>Remember me</IdRemeber>
                        <Link to={'/user/forgotpw'}>
                            <div style={{float: "right", color: 'rgb(24, 144, 255)'}}>Forgot your password?</div>
                        </Link>
                    </div>
                    <LoginButton type={'primary'} onClick={this.SubmitLogin}>Login</LoginButton>
                </Card>
                <Link to={'/user/terms'}>
                    <div style={{float: "right", color: 'rgb(24, 144, 255)', marginTop: 15}}>Sign Up</div>
                </Link>
            </div>
        );
    };
}


export default LoginForm;
