import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Card, Icon, Menu, Tooltip} from "antd";
import {Credentials, LoginInput, RegisterButton} from "../typescript/UserStyled";
import {Link} from "react-router-dom";
import autobind from "autobind-decorator";

@inject('user')
@observer
class ForgotPw extends Component<any, any> {
    state = {
        current: 'user',
    };

     /**
     * @description 비밀번호찾기 아이디
     */
    @autobind
    private findId(e:React.FormEvent<HTMLInputElement>){
        this.props.user.setFindId(e.currentTarget.value);
    }
    /**
     * @description 비밀번호찾기 이메일
     */
    @autobind
    private findEmail(e:React.FormEvent<HTMLInputElement>){
        this.props.user.setFindEmail(e.currentTarget.value);
    }
    /**
     * @description 비밀번호찾기 요청
     */
    @autobind
    private submitFindPw(){
        this.props.user.submitFindPw();
    }

    render() {

        return (
            <div>
                <Credentials selectedKeys={[this.state.current]} mode="horizontal">
                    <Menu.Item key="user">
                        <Icon type="mail"/>
                        Find Pw
                    </Menu.Item>
                </Credentials>
                <Card style={{backgroundColor: '#f0f2f5'}}>
                    <LoginInput
                        onChange={this.findId}
                        placeholder="가입시 등록한 아이디를 입력하세요"
                        prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        suffix={
                            <Tooltip title="Extra information">
                                <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                            </Tooltip>
                        }
                    />
                    <LoginInput
                        onChange={this.findEmail}
                        placeholder="가입시 등록한 이메일을 입력하세요"
                        prefix={<Icon type="mail" style={{color: 'rgba(0,0,0,.25)'}}/>}
                        suffix={
                            <Tooltip title="Extra information">
                                <Icon type="info-circle" style={{color: 'rgba(0,0,0,.45)'}}/>
                            </Tooltip>
                        }
                    />
                    <div style={{width: '100%', marginTop: 25}}>
                        <RegisterButton type={'primary'} onClick={this.submitFindPw}>Find PassWord</RegisterButton>
                        <Link to={'/user/login'}>
                            <div style={{float: "right", color: 'rgb(24, 144, 255)', marginTop: 15}}> Go Login Form?
                            </div>
                        </Link>
                    </div>
                </Card>

            </div>
        )
            ;
    };
}


export default ForgotPw;
