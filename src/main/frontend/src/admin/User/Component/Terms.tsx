import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Card, Icon, Menu} from "antd";
import {Credentials, IdRemeber, TermsButton} from "../typescript/UserStyled";
import {Link} from "react-router-dom";
import SignUpStep from "./SignUpStep";

@inject('user')
@observer
class Terms extends Component<any, any> {
    state = {
        current: 'user',
    };
    render() {

        return (
            <div>
                <SignUpStep/>
                <Credentials selectedKeys={[this.state.current]} mode="horizontal">
                    <Menu.Item key="user">
                        <Icon type="bars"/>
                        이용약관
                    </Menu.Item>
                </Credentials>
                <Card style={{backgroundColor:'#f0f2f5', height:400}}>
                    <div style={{textAlign:'left', overflowY:"auto", height:400}}>
                        ■ 개인정보의 수집 및 이용목적 <br/>
                        회사는 수집한 개인정보를 다음의 목적을 위해 활용합니다. <br/>
                        ○ 서비스제공 관한계약 이행 및 서비스제공에 따른 요금정산 <br/>
                        ○ 회원 관리 <br/>
                        ○ 마케팅 및 광고에 활용 <br/>
                        <br/>
                        ■ 수집하는 개인정보 항목 <br/>
                        회사는 회원가입, 상담, 서비스 신청 등을 위해 아래와 같은 <br/>
                        개인정보를 수집하고 있습니다. <br/>
                        ○ 수집항목 <br/>
                        ○ 개인정보 수집방법 <br/>
                        <br/>
                        ■ 개인정보의 보유 및 이용기간 <br/>
                        원칙적으로, 개인정보 수집 및 이용목적이 달성된 후에는 <br/>
                        해당 정보를 지체 없이 파기합니다. <br/>
                        단, 관계법령의 규정에 의하여 보존할 필요가 있는 경우 회사는 아래와 같이<br/>
                        관계법령에 서 정한 일정한 기간 동안 회원정보를 보관합니다. <br/>
                        ○ 회사 내부 방침에 의한 정보보유 사유   <br/>
                        ○ 관련법령에 따른 정보보유 <br/>
                        <br/>
                        ■ 개인정보의 파기 절차 및 방법 <br/>
                        회사는 원칙적으로 개인정보 수집 및 이용목적이 달성된 후에는 해당 정보를 지체 없이 <br/>
                        파기합니다. <br/>
                        파기절차 및 방법은 다음과 같습니다. <br/>
                        ○ 파기절차 <br/>
                        ○ 파기방법 <br/>
                        <br/>
                        ■ 동의 거부권 및 불이익 <br/>
                        귀하는 위와 같은 본인의 개인정보 수집∙이용에 관한 동의를 거부할 권리가 있습니다. <br/>
                        선택사항의 수집•이용에 관한 동의를 거부하는  경우에도 회원 가입은 가능하나 회원 <br/>
                        가입을 위해 필요한 최소한의 정보인 필수사항의 수집∙이용에 관한 동의를  거부하는 <br/>
                        경우에는 회원 가입이 불가합니다. <br/>
                    </div>

                    <div style={{marginTop: 25}}>
                        <IdRemeber>다음약관에 동의</IdRemeber>
                    </div>

                    <div style={{width: '100%', marginTop: 25}}>
                        <Link to={'/user/signup'}>
                        <TermsButton type={'primary'}>다음단계</TermsButton>
                        </Link>
                    </div>
                </Card>

            </div>
        );
    };
}


export default Terms;
