import React, {Component} from 'react';
import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Button, Result} from "antd";
import {Link} from "react-router-dom";

@inject('user')
@observer
class SignUpSuccess extends Component<any, any> {

    render() {

        return (
            <div style={{marginTop: 55}}>

                <Result
                    status="success"
                    title="C-PALS 프로그램 사이트 회원가입이 완료되었습니다."
                    subTitle="로그인 사용이 가능합니다."
                    extra={[
                        <Link to={'/user/login'}>
                            <Button type="primary" key="console">
                                Login
                            </Button>
                        </Link>,
                        <Link to={'/'}>
                        <Button key="buy">Go Main</Button>
                        </Link>,
                    ]}
                />
            </div>
        );
    };
}


export default SignUpSuccess;
