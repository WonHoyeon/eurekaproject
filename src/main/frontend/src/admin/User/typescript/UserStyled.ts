/**
 * @description 시뮬레이션 좌,우측 레이아웃을 제외한 css
 */
import styled from "styled-components";
import Title from "antd/es/skeleton/Title";
import {Button, Checkbox, Input, Menu} from "antd";


/**
 * 로그인폼 전체에 대한 DIV
 */
export const LoginFormBox = styled.div`
    text-align:center;
    width:450px;
    margin:auto;
          `;

/**
 * 로그인폼 로고 TEXT
 * */
export const LoginLogo = styled(Title)`
    text-align:center;
    font-size: 40px;
    margin:0px;
          `;
/**
 * 선택(Credentials) 메뉴
 * */
export const Credentials = styled(Menu)`
    text-align:center;
    background-color:#f0f2f5;
    margin-top:20px;
          `;
/**
 * 아이디 비밀번호 입력 Input
 * */
export const LoginInput = styled(Input)`
    margin-top: 25px;
    height: 45px;
          `;

/**
 * 회원가입 정보입력 Input
 * */
export const RegistInput = styled(Input)`
    width:70%;
    margin-top: 12px;
    height: 35px;
    float: right;
          `;


/**
 * 아이디 기억 체크박스
 * */
export const IdRemeber = styled(Checkbox)`
    float:left;
          `;

/**
 * 로그인버튼
 * */
export const LoginButton = styled(Button)`
    width:100%;
    margin-top: 30px;
    height:50px;
          `;

/**
 * 회원가입
 * */
export const RegisterButton = styled(Button)`
    width:220px;
    float:left;
    height:50px;
          `;

/**
 * 이용약관 확인 버튼
 * */
export const TermsButton = styled(Button)`
    width:100%;
    margin-top: 30px;
    height:50px;
          `;
