import React, {Component} from 'react';
import 'antd/dist/antd.css';
import Chart, {ArgumentAxis, Label, Series, Tooltip} from "devextreme-react/chart";
import PieChart, {Connector, Label as PieLabel, Legend, Series as PieSeries, Size} from "devextreme-react/pie-chart";
import {Card, Empty, Statistic,} from "antd";
import {inject, observer} from "mobx-react";
import {kpiData, kpiInfo} from "../../typescript/MonitorType";


@inject('dashboard', 'monitor')
@observer
class KpiComponentSub extends Component<any, any> {


    componentDidMount() {

    }

    render() {
        const {monitor} = this.props;
        return (
            <div>{monitor.getFactoryKpiInfo.map((param: kpiInfo, index: number) => {
                if(monitor.getFactoryKpiData.length == 0 && index <3){
                   return <Card key={index} title={param.kpiname} size={'small'}
                          style={{
                              height: 240,
                              width: '33.3%',
                              float: 'left'
                          }}>
                       <Empty />
                    </Card>
                }
                return <div> {monitor.getFactoryKpiData.map((value:any, index: number) => {

                    if (param.kpiid === value[0] && index <3) {
                        return <Card key={index} title={param.kpiname} size={'small'}
                                     style={{
                                         height: 240,
                                         width: '33.3%',
                                         float: 'left'
                                     }}>
                            {
                                (() => {
                                    switch (param.uitype) {

                                        //막대기 차트일 경우
                                        case 'Bar':
                                            return <Chart id="chart" style={{height: 180}}
                                                          dataSource={value[1]}>
                                                <Series
                                                    valueField="value"
                                                    argumentField="text"
                                                    type={'bar'}
                                                />
                                                <ArgumentAxis>
                                                    <Label
                                                        wordWrap="none"
                                                        overlappingBehavior="rotate"
                                                    />
                                                </ArgumentAxis>
                                                <Legend visible={false}/>
                                                <Tooltip enabled={true} shared={true}/>
                                            </Chart>
                                            break;

                                        //꺽은쇠 차트일 경우
                                        case 'Line':
                                            return <Chart id="chart" style={{height: 180}}
                                                          dataSource={value[1]}>
                                                <Series
                                                    valueField="value"
                                                    argumentField="text"
                                                />
                                                <ArgumentAxis>
                                                    <Label
                                                        wordWrap="none"
                                                        overlappingBehavior="rotate"
                                                    />
                                                </ArgumentAxis>
                                                <Legend visible={false}/>
                                                <Tooltip enabled={true} shared={true}/>
                                            </Chart>
                                            break;
                                        case 'Pie':
                                            return <span>  <PieChart style={{height: 180}} dataSource={value[1]}>
                                                        <PieSeries argumentField={'text'} valueField={'value'}>
                                                            <PieLabel visible={true}>
                                                                <Connector visible={true} width={1}/>
                                                            </PieLabel>
                                                        </PieSeries>
                                                        <Size/>
                                                              <Legend visible={true}/>
                                                    </PieChart>
                                                    <Tooltip enabled={true} shared={true}/>
                                                    </span>
                                            break;
                                        default :
                                            return value.flat().map((store: kpiData, index: number) => {
                                                if (index !== 0) {
                                                    return <div>
                                                        <Statistic key={index}
                                                                   prefix={store.text + ' : '}
                                                                   style={{fontSize:7}}
                                                                   value={store.value}
                                                                   suffix={store.unit}
                                                        />
                                                    </div>
                                                }
                                            })
                                            break;
                                    }
                                })()
                            }
                        </Card>
                    }
                }


                )}
                </div>
            })}
            </div>
        );
    };
}


export default KpiComponentSub;
