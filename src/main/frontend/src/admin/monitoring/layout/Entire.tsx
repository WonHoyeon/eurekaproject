import 'antd/dist/antd.css';
import {inject, observer} from "mobx-react";
import {Card, message} from "antd";
import React, {Component, RefObject} from 'react';
import MonitorModelMain from "../MonitorModelMain";
import autobind from "autobind-decorator";
import Searvice from "../../../common/Searvice";
import KpiComponent from "./Kpi/KpiComponent";
import {map3DInfo, model3DInfo} from "../typescript/MonitorType";
import {GlobalParameter} from "../../DashBoard/typescript/DashBoardType";
import KpiComponentSub from "./Kpi/KpiComponentSub";


/**
 * @description DASHBOARD 에서 회사의 공장 KPI정보와 3D 맵을 처리하는 클래스
 */
@inject('monitor', 'dashboard')
@observer
class Entire extends Component<any, any> {

    private _threejsConvas: RefObject<MonitorModelMain>;
    private _oldSize: { width: number, height: number } = {width: 0, height: 0};

    constructor(props: any) {
        super(props);
        this._threejsConvas = React.createRef();
        this.props.setRenderEvent(this._renderEvent);
    }

    @autobind
    private _renderEvent(type?: boolean): void {
        let modelLayout: any = document.getElementById("processModelContainer");
        if (modelLayout != undefined && this._threejsConvas != undefined) {
            let element: HTMLDivElement = modelLayout;
            let width: number = (element.clientWidth - 1);
            let height: number = (element.clientHeight - 7);

            if (type == false) {
                width = this._oldSize.width;
                height = this._oldSize.height;
            }

            if (this._threejsConvas.current != null) {
                this._threejsConvas.current.handleResize(width, height);
            }
        }
    }

    /**
     * @description 3D 화면에서 해당 OBJECT클릭시 해당 맵 정보를 받기위한 메서드
     * @param value
     */
    @autobind
    async setAdress(value: model3DInfo) {
        const {monitor, dashboard} = this.props;
        monitor.setDashLoading(true);

        const mapInfo = monitor.getDepth.map((store: map3DInfo) => {
            if (store.modelItemNm + '_1' == value.stencNm) {
                return store
            }
        })

        const resultMapInfoData = mapInfo.filter((parame: map3DInfo) =>
            parame !== undefined
        )

        if (resultMapInfoData.length == 0) {
            message.error('맵데이터가 없습니다.')
        } else {
            let parame = {
                mapId: resultMapInfoData[0].mapId
            }
            monitor.setPpp(resultMapInfoData[0].mapId);

            try {
                const resultData: any = await Searvice.getPost("dash/getSimulation.do", parame);
                monitor.setMonitorModel(resultData.data.simulationModel);
                monitor.setMonitorMap(resultData.data.simulationMap);
                monitor.setNowLevel(resultMapInfoData[0].lvl + 1);
            } catch (e) {
                console.log(e);
            }

            if (resultMapInfoData[0].lvl == 1) {
                sessionStorage.setItem('currentState', 'Building');
                let parameter: GlobalParameter = {
                    factoryId: 5006,
                    date: monitor.getPickerDate,
                    factoryType: 'Building',
                    userId: sessionStorage.getItem('loggedInfo')
                }


                try {
                    const resultData: any = await Searvice.getPost("dash/selectFactorylMap.do", parameter);
                    monitor.setHirerachyName2(resultData.data.factoryInfo[0].hirerachyName);
                    monitor.setFactory2(resultData.data)
                } catch (e) {
                    console.log(e);
                }


                try {
                    const resultData: any = await Searvice.getPost("dash/getSimulation.do", parame);
                    monitor.setMapId2(parame.mapId);
                    monitor.setMonitorModel2(resultData.data.simulationModel);
                    monitor.setMonitorMap2(resultData.data.simulationMap);
                    monitor.setNowLevel(resultMapInfoData[0].lvl);
                } catch (e) {
                    console.log(e);
                }
            } else if (resultMapInfoData[0].lvl == 2) {
                let parameter: GlobalParameter = {
                    factoryId: 9011,
                    date: monitor.getPickerDate,
                    factoryType: 'Line',
                    userId: sessionStorage.getItem('loggedInfo')
                }

                try {
                    const resultData: any = await Searvice.getPost("dash/selectFactorylMap.do", parameter);
                    monitor.setHirerachyName3(resultData.data.factoryInfo[0].hirerachyName);

                    monitor.setFactory3(resultData.data)
                } catch (e) {
                    console.log(e);
                }
                try {
                    const resultData: any = await Searvice.getPost("dash/getSimulation.do", parame);
                    monitor.setMonitorModel3(resultData.data.simulationModel);
                    monitor.setMonitorMap3(resultData.data.simulationMap);
                    monitor.setNowLevel(resultMapInfoData[0].lvl);
                } catch (e) {
                    console.log(e);
                }}}
        monitor.setDashLoading(false);
    }

    render() {
        const {monitor} = this.props;
        return (
            <>
                <Card style={{textAlign: "center", height: 780}} loading={monitor.getDashLoading}>
                    <MonitorModelMain
                        modelInfo={monitor.getMonitorModel}
                        mapInfo={monitor.getMonitorMap}
                        SimulationStore={monitor}
                        SetAdress={this.setAdress}
                        ref={this._threejsConvas}
                    />
                    <div style={{float: "right", height: 725, width: "33.3%", overflowY: "auto"}}>
                        <KpiComponent/>
                    </div>
                    <div style={{float: "left", height: 245, width: "66.6%"}}>
                        <KpiComponentSub/>
                    </div>
                </Card>
            </>
        );
    };
}

export default Entire;