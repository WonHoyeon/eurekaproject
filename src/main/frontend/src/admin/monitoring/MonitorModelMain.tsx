import React, {Component, RefObject} from 'react';
import {
    Group,
    HemisphereLight,
    Mesh,
    MeshBasicMaterial,
    Object3D,
    PerspectiveCamera,
    PlaneGeometry,
    Raycaster,
    Scene,
    Texture,
    TextureLoader,
    Vector3,
    WebGLRenderer
} from 'three';
import {OrbitControls} from "three/examples/jsm/controls/OrbitControls";
import CoordinateHelper from "../../threeView/CoordinateHelper";
import {IProcessMapInfo, IProcessModelInfo, IProcessModelMain} from "../../threeView/IProcessModelMain";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import Box3DObject from "../../model3D/Box3DObject";
import {GLTF, GLTFLoader} from "three/examples/jsm/loaders/GLTFLoader";
import ModelService from "./typescript/ModelService";

let INTERSECTED: any = null;


@inject('monitor')
@observer
class MonitorModelMain extends Component<IProcessModelMain, any> {

    // movieClip과 연결된 타임라인, 인스턴스에 전달하기위함 (일시중지, 되감기등 지원)
    // private _timeline: createjs.Timeline = new createjs.Timeline([], {}, {paused: true});
    private _threejsConvas: RefObject<HTMLDivElement> | null | undefined;

    private mount: HTMLElement | null = null;
    private scene: Scene | null = null;
    private camera: PerspectiveCamera | null = null;
    private renderer: WebGLRenderer | null = null;
    private controls: OrbitControls | null = null;
    private frameId: number = 0;
    private iisUrl: string = 'http://115.145.177.34:3486';
    private mouse = {x: 0, y: 0};
    private targetList: [] = []

    constructor(props: IProcessModelMain) {
        super(props);
        this._threejsConvas = React.createRef();
        this.state = {
            factoryName: ''
        }
    }

    /**
     * @param width
     * @param height
     */
    @autobind
    public handleResize(width: number, height: number): void {

        if (this.camera != null) {
            this.camera.aspect = width / height;
            this.camera.updateProjectionMatrix()
        }

        if (this.renderer != null) {
            this.renderer.setSize(width, height);
        }

    }

    public updateDimensions() {
        let mount: RefObject<HTMLDivElement> | null | undefined = this._threejsConvas;

        if (mount == null && mount == undefined) {
            return;
        }

        if (mount.current == null && mount.current == undefined) {
            return;
        }

        let width: number = mount.current.clientWidth - 2;
        let height: number = mount.current.clientHeight - 7;
        this.handleResize(width, height);
    };
    componentDidMount() {
        this.load3DModelInfo();
        document.addEventListener('mousedown', this.onDocumentMouseDown.bind(this), false);
        document.addEventListener('mousemove', this.onDocumentMouseMove.bind(this), false);
    }
    componentWillMount() {
    }

    /**
     * @description
     */
    private load3DModelInfo() {
        let mount: RefObject<HTMLDivElement> | null | undefined = this._threejsConvas;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }
        window.addEventListener('resize', this.updateDimensions.bind(this));

        let width: number = mount.current.clientWidth - 2;
        let height: number = mount.current.clientHeight - 7;


        // 무대 만들기
        const scene: Scene = new Scene();

        //지오메트리 설정
        let coordinatehelper: CoordinateHelper = new CoordinateHelper();
        scene.add(coordinatehelper.init());

        // 카메라 종횡비 설정
        const camera: PerspectiveCamera = new PerspectiveCamera(
            100,// 시야
            width / height, // 종횡비
            0.1, // 어디 부터 (0.1)
            1000  // 어디 까지 1000
        );

        //배경회색설정
        let backgroundColor: number = 0xfafffb;
        //부모컴포넌트에서 설정한 지오메틑리 배경색이 있으면 그걸로 선택.
        if (this.props.backgroundColor != undefined) {
            backgroundColor = this.props.backgroundColor;
        }

        // 화면 사이즈 설정x
        const renderer: WebGLRenderer = new WebGLRenderer();
        renderer.setSize(width, height);
        renderer.setClearColor(backgroundColor, 1);

        //라이트
        let light: HemisphereLight = new HemisphereLight(0xffffff, 0x444444);
        light.position.set(0, 200, 0);
        scene.add(light);

        // 카메라 컨트롤
        this.controls = new OrbitControls(camera, renderer.domElement);
        this.controls.enableDamping = true;
        this.controls.dampingFactor = 0.25;
        this.controls.minDistance = 50;
        this.controls.maxDistance = 500;
        this.controls.maxPolarAngle = Math.PI / 2;

        if (this.props.mapInfo == undefined || this.props.modelInfo == undefined) {
            return;
        }

        let mapInfo: { [index: string]: IProcessMapInfo } = {};
        // Map 정보 표기
        this.props.mapInfo.forEach(function (item: IProcessMapInfo): void {
            mapInfo[item.mapId] = item;

            // 1. Map Image 로딩 ...
            let loadTextURL: string = "/dexi-web/sim/getMapFilePatch.do?mapid=" + item.mapId + '&modelItemId=' + item.modelItemId;
            let loader: TextureLoader = new TextureLoader();

            // 2. Map Image 배치 ...
            let planeGeometry: PlaneGeometry = new PlaneGeometry(item.mapRangeLenx, item.mapRangeLeny, item.mapRangeLenx / item.mapGridx, item.mapRangeLeny / item.mapGridy);
            loader.load(
                loadTextURL,
                function (texture: Texture): void {
                    let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff, map: texture});
                    let mapImgMesh: Mesh = new Mesh(planeGeometry, planeMaterial);
                    mapImgMesh.position.set(0, -5, 0);
                    mapImgMesh.rotation.x = -0.5 * Math.PI;
                    scene.add(mapImgMesh);
                },
                undefined,
                // onError callback
                function (event: ErrorEvent): void {
                    let planeMaterial: MeshBasicMaterial = new MeshBasicMaterial({color: 0xffffff});
                    let mapImgMesh: Mesh = new Mesh(planeGeometry, planeMaterial);
                    mapImgMesh.position.set(0, -5, 0);
                    mapImgMesh.rotation.x = -0.5 * Math.PI;
                    scene.add(mapImgMesh);
                }
            );
            // 3. Grid 설정 ...
            let planeMaterialGrid: MeshBasicMaterial = new MeshBasicMaterial({color: 0xe3e3e3, wireframe: true});
            let grid: Mesh = new Mesh(planeGeometry, planeMaterialGrid);
            grid.position.set(0, -5, 0);
            grid.rotation.x = -0.5 * Math.PI;
            scene.add(grid);
        });

        // 공장 머신 배치 ..
        const modelList: Array<IProcessModelInfo> = this.props.modelInfo;


        let drainItem: IProcessModelInfo;
        // 공장 머신 배치 ..
        for (let item of modelList) {
            const modelItem: IProcessModelInfo = item;
            const positionX: number = -(mapInfo[item.mapId].mapRangeLenx / 2) + modelItem.stencPosx + (modelItem.stencLenx / 2);
            const positionY: number = -(mapInfo[item.mapId].mapRangeLeny / 2) + modelItem.stencPosy;
            const positionZ: number = modelItem.stencPosz + (modelItem.stencLenz / 2);
            const fillColor: number = (modelItem.fillColor != null) ? parseInt('0x' + modelItem.fillColor.toString(), 16) : 0xFFFFFF;
            if (item.stenc3did === 'EmptyCode') {
                switch (modelItem.stencNm) {
                    case 'A동_텍스트_1' :
                        null
                        break;
                    case 'B동_텍스트_1' :
                        null
                        break;
                    case 'C동_텍스트_1' :
                        null
                        break;
                    case 'D동_텍스트_1' :
                        null
                        break;
                    case '플로어_서브_텍스트_1' :
                        null
                        break;
                    case '플로어_조립_텍스트_1' :
                        null
                        break;
                    case '소물류_텍스트_1' :
                        null
                        break;
                    default:
                        let box3DObject0: Box3DObject = new Box3DObject({
                            x: positionX,
                            y: positionY + (modelItem.stencLeny / 2),
                            z: positionZ,
                            width: modelItem.stencLenx,
                            height: modelItem.stencLeny,
                            depth: 1,
                            edgesVisible: false,
                            color: fillColor,
                            textInfo: {
                                text: modelItem.stencNm,
                                width: 150,
                                height: 150
                            }
                        });
                        box3DObject0.getGroup().name = modelItem.stencNm;
                        scene.add(box3DObject0.getGroup());

                        // @ts-ignore
                        this.targetList.push(box3DObject0.getGroup())
                        break;
                }
            } else {
                let group: Group = new Group();
                //맵 범위
                const modelService: ModelService = new ModelService(
                    mapInfo[item.mapId].mapRangeLenx,
                    mapInfo[item.mapId].mapRangeLeny,
                    50,
                );
                let glbURL: string = this.iisUrl + item.stenc3dfile
                let obj: GLTFLoader = new GLTFLoader();
                if (modelItem.stencNm.includes("플로어조립")) {
                    obj.load(glbURL, function (object: GLTF) {
                            let obj: Object3D = object.scene;
                            obj = modelService.setModelSize(
                                obj,
                                modelItem.stencLenx,

                                40,
                                modelItem.stencLeny
                            );
                            obj.rotateX(1.57 * (270 / 90))
                            group.add(obj);
                            group.rotation.x = 1.57 * (90 / 90);
                            obj.position.set(positionX, positionY + (modelItem.stencLeny), positionZ);
                            scene.add(group);
                        }, undefined,
                        function (event: ErrorEvent): void {
                            let box3DObject: Box3DObject = new Box3DObject({
                                x: positionX,
                                y: positionY + (modelItem.stencLeny / 2),
                                z: 25,
                                width: modelItem.stencLenx,
                                height: modelItem.stencLeny,
                                depth: 50,
                                edgesVisible: false,
                                color: 0xFFFF00
                            });
                            //positionX  , positionz, positionY +(modelItem.stencLeny / 2) , modelItem.stencLenx , modelItem.stencLeny , modelItem.stencLenz, false , 0xFFFF00 );;
                            scene.add(box3DObject.getGroup());
                        });
                } else if (modelItem.stencNm.includes("구조검사설비")) {
                    obj.load(glbURL, function (object: GLTF) {
                            let obj: Object3D = object.scene;
                            obj = modelService.setModelSize(
                                obj,
                                modelItem.stencLenx,
                                70,
                                modelItem.stencLeny
                            );
                            obj.rotateX(1.57 * (270 / 90))
                            group.add(obj);
                            group.rotation.x = 1.57 * (90 / 90);
                            obj.position.set(positionX, positionY + (modelItem.stencLeny), positionZ);
                            scene.add(group);
                        }, undefined,
                        function (event: ErrorEvent): void {
                            let box3DObject: Box3DObject = new Box3DObject({
                                x: positionX,
                                y: positionY + (modelItem.stencLeny / 2),
                                z: 25,
                                width: modelItem.stencLenx,
                                height: modelItem.stencLeny,
                                depth: 30,
                                edgesVisible: false,
                                color: 0xFFFF00
                            });
                            //positionX  , positionz, positionY +(modelItem.stencLeny / 2) , modelItem.stencLenx , modelItem.stencLeny , modelItem.stencLenz, false , 0xFFFF00 );;
                            scene.add(box3DObject.getGroup());
                        });
                } else {
                    obj.load(glbURL, function (object: GLTF) {
                            let obj: Object3D = object.scene;
                            obj = modelService.setModelSize(
                                obj,
                                modelItem.stencLenx,

                                35,
                                modelItem.stencLeny
                            );
                            obj.rotateX(1.57 * (270 / 90))
                            group.add(obj);
                            group.rotation.x = 1.57 * (90 / 90);
                            obj.position.set(positionX, positionY + (modelItem.stencLeny), positionZ);
                            scene.add(group);
                        }, undefined,
                        function (event: ErrorEvent): void {
                            let box3DObject: Box3DObject = new Box3DObject({
                                x: positionX,
                                y: positionY + (modelItem.stencLeny / 2),
                                z: 25,
                                width: modelItem.stencLenx,
                                height: modelItem.stencLeny,
                                depth: 30,
                                edgesVisible: false,
                                color: 0xFFFF00
                            });
                            //positionX  , positionz, positionY +(modelItem.stencLeny / 2) , modelItem.stencLenx , modelItem.stencLeny , modelItem.stencLenz, false , 0xFFFF00 );;
                            scene.add(box3DObject.getGroup());
                        });
                }
            }
        }

        // 카메라 위치 설정
        //camera.position.set(0,500,1000);
        camera.position.set(200, 200, 200);
        this.scene = scene;
        this.camera = camera;
        this.renderer = renderer;
        mount.current.appendChild(this.renderer.domElement);
        this.start();
    }

    @autobind
    start(): void {
        if (this.frameId == 0) {
            this.frameId = requestAnimationFrame(this.animate);
        }
    }

    /**
     * 재생 , 일시정지 , 정지 참고 <br />
     * https://github.com/tweenjs/tween.js/issues/341
     * @param time
     */
    @autobind
    private animate(time: number) {

        this.renderScene();
        this.frameId = window.requestAnimationFrame(this.animate)
        this.update();
    }

    @autobind
    public update() {
        // @ts-ignore
        this.controls.update();

    }

    componentWillUnmount() {

        let mount: RefObject<HTMLDivElement> | null | undefined = this._threejsConvas;
        if (mount == null && mount == undefined) {
            return;
        }
        if (mount.current == null && mount.current == undefined) {
            return;
        }

        if (this.renderer == null) {
            return;
        }

        this.stop();
        mount.current.removeChild(this.renderer.domElement);
        window.removeEventListener('resize', this.updateDimensions);
    }


    /**
     * @description 마우스 오버일때 해당 오브젝트의 색상을 변화시킨다
     * @param event
     */
    public onDocumentMouseMove(event: any) {
        let gapX: number = event.clientX - event.offsetX;
        let gapY: number = event.clientY - event.offsetY;


        this.mouse.x = ((event.clientX - gapX) / (window.innerWidth * 0.535)) * 2 - 1;
        this.mouse.y = -((event.clientY - gapY) / (window.innerHeight * 0.535)) * 2 + 1;

        const vector = new Vector3(this.mouse.x, this.mouse.y, 1);
        // @ts-ignore
        vector.unproject(this.camera);


        // @ts-ignore
        const ray = new Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());

        // @ts-ignore
        const intersects: [] = ray.intersectObjects(this.targetList, true); // 신에 추가한 대상들이 포함되어 있는 애들
        if (intersects.length > 0) {

            {
                // if the closest object intersected is not the currently stored intersection object
                // @ts-ignore
                if (intersects[0].object != INTERSECTED) {
                    // restore previous intersection object (if it exists) to its original color
                    if (INTERSECTED)
                        INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
                    // store reference to closest object as current intersection object
                    // @ts-ignore
                    INTERSECTED = intersects[0].object;
                    // store color of closest object (for later restoration)
                    INTERSECTED.currentHex = INTERSECTED.material.color.getHex();
                    // set a new color for closest object
                    INTERSECTED.material.color.setHex(0xffff00);
                }
            }
        } else // there are no intersections
        {
            // restore previous intersection object (if it exists) to its original color
            if (INTERSECTED)
                INTERSECTED.material.color.setHex(INTERSECTED.currentHex);
            // remove previous intersection object reference
            //     by setting current intersection object to "nothing"
            INTERSECTED = null;
        }

    }

    /**
     * @description 해당 오브젝트를 클릭시 클릭이벤트가 진행(driildown으로 해당 맵 이동)
     * @param event
     */
    public onDocumentMouseDown(event: any) {
        let gapX: number = event.clientX - event.offsetX;
        let gapY: number = event.clientY - event.offsetY;


        this.mouse.x = ((event.clientX - gapX) / (window.innerWidth * 0.535)) * 2 - 1;
        this.mouse.y = -((event.clientY - gapY) / (window.innerHeight * 0.535)) * 2 + 1;

        const vector = new Vector3(this.mouse.x, this.mouse.y, 1);
        // @ts-ignore
        vector.unproject(this.camera);

        // @ts-ignore
        const ray = new Raycaster(this.camera.position, vector.sub(this.camera.position).normalize());

        // @ts-ignore
        const intersects: [] = ray.intersectObjects(this.targetList, true); // 신에 추가한 대상들이 포함되어 있는 애들
        if (intersects.length > 0) {
            this.props.SimulationStore.getMonitorModel.map((value: any) => {

                // @ts-ignore
                if (value.stencNm == intersects[0].object.parent.name) {

                    this.setState({
                        factoryName: value.modelItemNm
                    })

                    // @ts-ignore
                    this.props.SetAdress(value);
                    this.targetList = [];
                }
            })
        } else {
            console.log('해당객체 없음');
        }
    }

    @autobind
    stop() {
        cancelAnimationFrame(this.frameId)
    };

    renderScene() {
        if (this.renderer == null || this.scene == null || this.camera == null) {
            return;
        }
        if (this.controls != null) {
            this.controls.update();
        }
        this.renderer.render(this.scene, this.camera);
    }

    render() {
        return (
            <div style={{height: 480, width: '66%', float: "left"}} ref={this._threejsConvas}/>
        );
    }
}

export default MonitorModelMain;