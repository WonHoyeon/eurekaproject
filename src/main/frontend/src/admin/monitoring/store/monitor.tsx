import {action, computed, observable} from 'mobx';
import React from "react";
import {factoryEntire, factoryInfo, kpiInfo, map3DInfo, mapInfo, model3DInfo} from "../typescript/MonitorType";
import Searvice from "../../../common/Searvice";
import {GlobalParameter} from "../../DashBoard/typescript/DashBoardType";
import StringUtile from "../../../utile/StringUtile";

class monitor {
    @observable pickerDate: string = StringUtile.formatDate();

    @action setPickerDate(param: string) {
        this.pickerDate = param;
    }

    @computed get getPickerDate() {
        return this.pickerDate;
    }

// ===================================== 로딩셋팅
    @observable dashLoading: boolean = false;

    @action setDashLoading(param: boolean) {
        this.dashLoading = param;
    }

    @computed get getDashLoading() {
        return this.dashLoading;
    }

// ===================================== 로딩셋팅
    @observable dashLoadingM: boolean = false;

    @action setDashLoadingM(param: boolean) {
        this.dashLoadingM = param;
    }

    @computed get getDashLoadingM() {
        return this.dashLoadingM;
    }

// =============================================================Global Map--> Monitoring=======================================================
    // 공장 세부정보를 조회하기 위한 ComapnyID(match.param으로 받을것)
    @observable postId: string = '';

    @action setPostId(parameter: string) {
        this.postId = parameter;
    }

    @computed get getPostId() {
        return this.postId;
    }

    //
    @observable modelId: string = '';

    @action setModelId(parameter: string) {
        this.modelId = parameter;
    }

    @computed get getModelId() {
        return this.modelId;
    }


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    // FactoryAll (전체공장)
    @observable factoryData: factoryEntire[] = [];

    // 공장 kpi 데이터 정제▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @action
    async setFactory(parame: factoryEntire) {
        const test = [];

        test.push(parame.factoryInfo[0]);
        this.naviMenu = test;
        this.factoryInfo = [parame.factoryInfo[0]];

        this.factoryKpiInfo1 = parame.kpiInfo;
        this.factoryKpiInfo = parame.kpiInfo;
        this.factoryKpiData = parame.kpiData;

        const FactoryKpiData = Object.entries(parame.kpiData.reduce((p: any, c: any) => (p[c.kpiid] ? p[c.kpiid].push(c) : p[c.kpiid] = [c], p), {}))
        this.factoryKpiData = FactoryKpiData;
        this.factoryKpiData1 = FactoryKpiData;
    }


    @observable factoryInfo: Array<factoryInfo> = [];
    @observable factoryKpiInfo: Array<kpiInfo> = [];
    @observable factoryKpiInfo1: Array<kpiInfo> = [];
    @observable factoryKpiData: any = [];
    @observable factoryKpiData1: any = [];

    @computed get getFactoryInfo() {
        return this.factoryInfo
    }

    @computed get getFactoryKpiInfo() {
        return this.factoryKpiInfo
    }

    @computed get getFactoryKpiData() {
        return this.factoryKpiData
    }

// 공장 kpi 데이터 카피▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    // 공장 kpi 데이터 정제2▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @action
    async setFactory2(parame: factoryEntire) {

        const resultDate = this.naviMenu;
        if(resultDate.length == 1){
        resultDate.push(parame.factoryInfo[0])
        }

        this.naviMenu = resultDate;
        this.factoryInfo2 = resultDate;
        this.factoryKpiInfo2 = parame.kpiInfo;
        this.factoryKpiInfo = parame.kpiInfo;
        this.factoryKpiData2 = parame.kpiData;

        const FactoryKpiData2 = Object.entries(parame.kpiData.reduce((p: any, c: any) => (p[c.kpiid] ? p[c.kpiid].push(c) : p[c.kpiid] = [c], p), {}))
        this.factoryKpiData2 = FactoryKpiData2;
        this.factoryKpiData = FactoryKpiData2;
    }

    @computed get getFactory2() {
        return this.factoryKpiData2
    }

    @observable factoryInfo2: Array<factoryInfo> = [];
    @observable factoryKpiInfo2: Array<kpiInfo> = [];
    @observable factoryKpiData2: any = [];

    @computed get getFactoryInfo2() {
        return this.factoryInfo2
    }

    @computed get getFactoryKpiInfo2() {
        return this.factoryKpiInfo
    }

    @computed get getFactoryKpiData2() {
        return this.factoryKpiData
    }

    // 공장 kpi 데이터 정제3▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @action
    async setFactory3(parame: factoryEntire) {
        const resultDate = this.naviMenu;

        if(resultDate.length == 2){
            resultDate.push(parame.factoryInfo[0])
        }
        this.naviMenu = resultDate;
        this.factoryInfo3 = resultDate;
        this.factoryKpiInfo3 = parame.kpiInfo;
        this.factoryKpiInfo = parame.kpiInfo;
        this.factoryKpiData3 = parame.kpiData;

        const FactoryKpiData3 = Object.entries(parame.kpiData.reduce((p: any, c: any) => (p[c.kpiid] ? p[c.kpiid].push(c) : p[c.kpiid] = [c], p), {}))
        this.factoryKpiData3 = FactoryKpiData3;
        this.factoryKpiData = FactoryKpiData3;
    }

    @observable factoryInfo3: Array<factoryInfo> = [];
    @observable factoryKpiInfo3: Array<kpiInfo> = [];
    @observable factoryKpiData3: any = [];

    @computed get getFactoryKpiInfo3() {
        return this.factoryKpiInfo
    }

    @computed get getFactoryKpiData3() {
        return this.factoryKpiData
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    // 시뮬레이션 호출에 필요한 정보들(노드구조에서 자식까지 가져온 데이터임)을 분류하는 작업▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable factory3DInfo: Array<map3DInfo> = [];

    @action setFactory3DInfo(parame: Array<map3DInfo>) {

        this.depth = parame;
        parame.map((value: any) => {
            if (value.lvl == 0) {
                this.depth0 = value;
            } else if (value.lvl == 1) {
                const depth1 = this.depth1.concat(value);
                this.depth1 = depth1;
            } else if (value.lvl == 2) {
                const depth2 = this.depth2.concat(value);
                this.depth2 = depth2;
            } else if (value.lvl == 3) {
                const depth3 = this.depth3.concat(value);
                this.depth3 = depth3;
            }
        })
    }

    //depth 전체 데이터
    @observable depth: Array<map3DInfo> = [];

    @computed get getDepth() {
        return this.depth
    }


    //depth 0 데이터
    @observable depth0: any = null;

    @computed get getDepth0() {
        return this.depth0
    }

    //depth 1 데이터
    @observable depth1: Array<map3DInfo> = [];

    @computed get getDepth1() {
        return this.depth1
    }

    //depth 2 데이터
    @observable depth2: Array<map3DInfo> = [];

    @computed get getDepth2() {
        return this.depth2
    }

    //depth 3 데이터
    @observable depth3: Array<map3DInfo> = [];

    @computed get getDepth3() {
        return this.depth3
    }

    // 모니터링 맵, 모델 정보 담는 store▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable monitorMap: Array<mapInfo> = [];

    @action setMonitorMap(param: Array<mapInfo>) {
        this.monitorMap = param;
    }

    @computed get getMonitorMap() {

        return this.monitorMap;
    }

    @observable monitorModel: Array<model3DInfo> = [];

    @action setMonitorModel(param: Array<model3DInfo>) {
        this.monitorModel = param;
    }

    @computed get getMonitorModel() {

        return this.monitorModel;
    }


    @observable monitorMap2: Array<mapInfo> = [];
    @action setMonitorMap2(param: Array<mapInfo>) {
        this.monitorMap2 = param;
    }

    @computed get getMonitorMap2() {

        return this.monitorMap2;
    }

    @observable monitorModel2: Array<model3DInfo> = [];
    @action setMonitorModel2(param: Array<model3DInfo>) {
        this.monitorModel2 = param;
    }

    @computed get getMonitorModel2() {

        return this.monitorModel2;
    }


    @observable monitorMap3: Array<mapInfo> = [];
    @action setMonitorMap3(param: Array<mapInfo>) {
        this.monitorMap3 = param;
    }

    @computed get getMonitorMap3() {

        return this.monitorMap3;
    }

    @observable monitorModel3: Array<model3DInfo> = [];
    @action setMonitorModel3(param: Array<model3DInfo>) {
        this.monitorModel3 = param;
    }

    @computed get getMonitorModel3() {

        return this.monitorModel3;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    @observable relatedId: string = '';
    @action setRelatedId(param: string) {
        this.relatedId = param;
    }

    @computed get getRelatedId() {

        return this.relatedId;
    }


    @observable hirerachyName: Array<any> = [];
    @action setHirerachyName(param: string) {
        const hirerachy = param.split('>')
        this.hirerachyName = hirerachy;
    }

    @computed get getHirerachyName() {
        return this.hirerachyName
    }


    @observable hirerachyName2: Array<any> = [];
    @action setHirerachyName2(param: string) {
        const hirerachy = param.split('>')
        this.hirerachyName = hirerachy;
        this.hirerachyName2 = hirerachy;
    }


    @computed get getHirerachyName2() {
        return this.hirerachyName2
    }

    @observable hirerachyName3: Array<any> = [];
    @action setHirerachyName3(param: string) {
        const hirerachy = param.split('>')
        this.hirerachyName = hirerachy;
        this.hirerachyName3 = hirerachy;
    }

    @computed get getHirerachyName3() {
        return this.hirerachyName3
    }

    /**
     * @description 필터 조건에따른 데이터 정제
     */
    @action setRecordFilter(param: string) {
        // if (param == 'all') {
        //     this.kpiRecord = this.baseData;
        // } else {
        //     const filterData = this.baseData.filter((parame: KpiRecordData) => parame.relatedId == param && parame.day ==this.pickerDate);
        //     this.kpiRecord = filterData;
        // }
    }


    @observable kpiSearch: Array<any> = [];

    @action setKpiSearch(param: Array<any>) {
        this.kpiSearch = param;
    }

    @computed get getKpiSearch() {

        return this.kpiSearch;
    }


    @observable nowLevel: number = 0;

    @action setNowLevel(param: number) {
        this.nowLevel = param;
    }

    @computed get getNowLevel() {
        return this.nowLevel;
    }

    @action
    async settest(value: any) {
        console.log(value,'backway?');
        this.dashLoading = true;

        if (value.depth == '2' || value.depth == '3' ) {
            this.naviMenu = [this.factoryInfo[0]];
            let modelParam = {
                mapId: this.depth0.mapId,
            }
            sessionStorage.setItem('currentState', 'Factory');
            this.factoryKpiData = this.factoryKpiData1
            this.factoryKpiInfo = this.factoryKpiInfo1
            try {
                const resultData: any = await Searvice.getPost("dash/getSimulation.do", modelParam);
                this.setMonitorModel(resultData.data.simulationModel);
                this.setMonitorMap(resultData.data.simulationMap);
            } catch (e) {
                console.log(e);
            }


            let parameter:GlobalParameter = {
                factoryId: value.id,
                date: this.pickerDate,
                factoryType:'Factory',
                userId:sessionStorage.getItem('loggedInfo')
            }
            console.log(parameter,'result');

            try {
                const resultData:any = await Searvice.getPost("dash/selectFactorylMap.do", parameter);
                this.setHirerachyName(resultData.data.factoryInfo[0].hirerachyName);

                this.setHirerachyName2(resultData.data.factoryInfo[0].hirerachyName);
                console.log(resultData.data,'?????');
                this.setFactory(resultData.data)
            } catch (e) {
                console.log(e);
            }




        } else if (value.depth == '4') {
            const test = this.factoryInfo2.slice(0, 2);
            sessionStorage.setItem('currentState', 'Building');
            this.naviMenu = test;
            // this.factoryKpiInfo = this.factoryKpiInfo2;
            // this.factoryKpiData = this.factoryKpiData2;
            // this.hirerachyName = this.hirerachyName2;
            // console.log(this.hirerachyName2, 'this.hirerachyName2')
            // this.hirerachyName = this.hirerachyName2;
            // depth0 (공장전체) 의 3d 정보 가져오기

            this.factoryKpiData = this.factoryKpiData2
            let modelParam2 = {
                mapId: this.mapId2
            }
            try {
                const resultData: any = await Searvice.getPost("dash/getSimulation.do", modelParam2);
                this.setMonitorModel(resultData.data.simulationModel);
                this.setMonitorMap(resultData.data.simulationMap);
            } catch (e) {
                console.log(e);
            }
            // this.nowLevel = 1

        } else if (value.depth == '6') {
            const test = this.factoryInfo3.slice(0, 3);

            this.naviMenu = test;
            this.hirerachyName = this.hirerachyName3;
            // depth0 (공장전체) 의 3d 정보 가져오기

// this.nowLevel = 1
        } else {
            console.log('3');
        }
        //
        // // depth0 (공장전체) 의 3d 정보 가져오기
        // let modelParam = {
        //     mapId: this.depth0.mapId,
        // }
        // try {
        //     const resultData: any = await Searvice.getPost("dash/getSimulation.do", modelParam);
        //     this.setMonitorModel(resultData.data.simulationModel);
        //     this.setMonitorMap(resultData.data.simulationMap);
        // } catch (e) {
        //     console.log(e);
        // }
        // this.nowLevel = 1
        this.dashLoading = false;
    }

    @observable
    ppp: string = '';

    @action
    setPpp(param: string) {
        this.ppp = param;
    }

    @computed get getPpp() {
        return this.ppp;
    }

// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable
    naviMenu: Array<factoryInfo> = [];

    @action
    setNaviMenu(param: Array<factoryInfo>) {
        this.naviMenu = param;
    }

    @computed get getNaviMenu() {
        return this.naviMenu;
    }

    // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    @observable
    mapId: string = '';

    @action
    setMapId(param: string) {
        this.mapId = param;
    }

    @computed get getMapId() {
        return this.mapId;
    }

    @observable
    mapId2: string = '';

    @action
    setMapId2(param: string) {
        this.mapId2 = param;
    }

    @computed get getMapId2() {
        return this.mapId2;
    }


    // ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

}

export default monitor;