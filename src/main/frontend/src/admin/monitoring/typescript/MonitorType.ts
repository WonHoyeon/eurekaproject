/**
 * @description KPI 데이터 타입스크립트
 */
export interface KpiInfo{
    value:string,
    id:string,
    factoryID: string,
    companyID: string,
    unit: string,
    kpiname: string,
    kpiid: string,
    factoryName: string,
    componyName: string,
    text?:string
}


// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
export interface factoryEntire {
    factoryInfo: Array<factoryInfo>,
    kpiInfo: [],
    kpiData: []
}

/**
 * @description 시뮬레이션모델의 요소정보
 */
export interface model3DInfo{
    stencId: string,
    stenckey: string,
    stencNm: string,
    stencLenx: number,
    stencLeny: number,
    stencLenz: number,
    stencPosx: number,
    stencPosy:number,
    stencPosz: number,
    zIndex: number,
    mapId: string,
    rotAngle: number,
    modelScale:number,
    stenc3dTp: string,
    stenc3did: string,
    stenc3dinfoid: string,
    stenc3dfile:string,
    fillColor: string,
    lineColor: string,
    opacity: number
}

/**
 * @description 모델과 맵의 정보
 */
export interface map3DInfo{
    modelItemId: string,
    upModelItemId: string,
    modelItemNm: string,
    mapId: string,
    bgImgPath: string,
    lvl: number
}

/**
 * @description 시뮬레이션 맵의 구성요소정보
 */
export interface mapInfo{
    mapId: string,
    modelItemId: string,
    mapRangeLenx: number,
    mapRangeLeny: number,
    mapRangeLenz: number,
    mapGridx: number,
    mapGridy: number,
    mapGridz:number,
}

/**
 * @description 해당 맵의 공장정보
 */
export interface factoryInfo{
    name: string,
    location: string,
    type: string,
    id: string,
    description: string,
    depth:string,
    compnayID: string,
    factoryName: string,
    buildingName:string,
    buildingID:string,
    longitude: string,
    latitude: string,
    p_ID: null
    modelID:string,
    hirerachyID: string,
    companyName: string,
    factoryID: string,
    floorName: string,
    floorID: string,
    lineName: string,
    mapLocation:string,
    mapID:string,
    hirerachyName: string,
    lineID: string,
}


/**
 * @description kpiInfo 타입
 */
export interface kpiInfo{
    kpiid: string,
    kpiname:string,
    uitype: string,
    unit: string,
}

/**
 * @description kpiData 타입
 */
export interface kpiData{
    value: number,
    key: string,
    text: string,
    kpiid: string,
    unit: string
}

