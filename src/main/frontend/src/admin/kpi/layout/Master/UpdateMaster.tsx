import React, {Component} from 'react';
import {Button, Card, Input, message, PageHeader, Select} from "antd";
import {inject, observer} from "mobx-react";
import Searvice from "../../../../common/Searvice";
import autobind from "autobind-decorator";

const { Option } = Select;


@inject('kpi')
@observer
class UpdateMaster extends Component<any, any>{


    // Setter▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    private setKpiId = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setKpiId(e.target.value);
    };
    private setKpiName = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setKpiName(e.target.value);
    };
    private setUnit = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setUnit(e.target.value);
    };
    private setDesr = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setDesr(e.target.value);
    };
    private setScope = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setScope(e.target.value);
    };
    private setUiType = (value:string) => {
        this.props.kpi.setUiType(value);
    };
    private setFomula = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setFormula(e.target.value);
    };
    private setUnitOfM = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setUnitOfM(e.target.value);
    };
    private setRange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setRange(e.target.value);
    };
    private setTrend = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setTrend(e.target.value);
    };
    private setTiming = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setTiming(e.target.value);
    };
    private setAud = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setAud(e.target.value);
    };
    private setProdMeth = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setProdMeth(e.target.value);
    };
    private setNotes = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setNotes(e.target.value);
    };
    private setPrefer = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setPrefer(e.target.value);
    };
    private setDiagramFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setDiagramFile(e.target.value);
    };
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @description KPI INSERT 하기
     */
    @autobind
     private async updateMaster(event: React.MouseEvent<HTMLInputElement>){
        let update = this.props.kpi.getMasterParam;
       const resultData =  await Searvice.getPost("dash/updateKpiMaster.do", update);
       if(resultData.data.result == true){
           message.success('수정이 완료 되었습니다.');
       }else{
           message.error('수정을 실패하였습니다');
       }
     };
    render() {
        const {kpi} = this.props;
        return (
            <>
                <PageHeader style={{ backgroundColor: 'white' }}
                    title="KPI Master Registry"/>
                <br />
                <Card style={{ padding: 60 }}>
                    <h3>Content</h3>
                    <div style={{ width: 600, margin: "auto" }}>
                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>kpiId</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setKpiId} value={kpi.getKpiId}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>kpiName</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setKpiName}value={kpi.getKpiName}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>unit</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setUnit}value={kpi.getUnit}/>
                        </div>


                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>description</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setDesr}value={kpi.getDesr}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>uiType</div>
                            <Select value={kpi.getUiType} style={{ width: 300, float: "right" }}  onChange={this.setUiType}>
                                <Option value="N-Line">텍스트</Option>
                                <Option value="Line">꺽은쇠 그래프</Option>
                                <Option value="Bar">막대 그래프</Option>
                                <Option value="Pie">파이 그래프</Option>
                            </Select>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>scope</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setScope}value={kpi.getScope}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>fomula</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setFomula}value={kpi.getFormula}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>unit of measure</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setUnitOfM}value={kpi.getUnitOfM}/>
                        </div>


                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>range</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setRange}value={kpi.getRange}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>trend</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setTrend}value={kpi.getTrend}/>
                        </div>
                    </div>
                </Card>
                <br />
                <Card>
                    <h3>Context</h3>
                    <div style={{ width: 500, margin: "auto" }}>
                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>timing</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setTiming}value={kpi.getTiming}/>
                        </div>
                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>audience</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setAud}value={kpi.getAud}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>prodMeth</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setProdMeth}value={kpi.getProdMeth}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>notes</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setNotes}value={kpi.getNotes}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>preference</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setPrefer}value={kpi.getPrefer}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>Effect model diagram</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setDiagramFile}value={kpi.getDiagramFile}/>
                        </div>
                    </div>
                    <div style={{ width: 300, margin: "auto" }}>
                        <Button style={{ width: '45%', float: "left" }} type={'primary'} onClick={this.updateMaster}>수정</Button>
                        <Button style={{ width: '45%', float: "right" }} type={'danger'} onClick={() => history.go(-1)}>취소</Button>
                    </div>
                </Card>
            </>
        );
    };
}

export default UpdateMaster;