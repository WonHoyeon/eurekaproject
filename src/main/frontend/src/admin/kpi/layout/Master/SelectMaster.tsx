import React, {Component} from 'react';
import {Button, Card, Input, PageHeader, Select} from "antd";
import {inject, observer} from "mobx-react";
import {Link} from "react-router-dom";

const {Option} = Select;

@inject('kpi')
@observer
class SelectMaster extends Component<any, any> {

    state = {
        loading: false
    }

    componentDidMount() {
        const {kpi} = this.props;
        this.setState({
            loading: true
        })

        this.setState({
            loading: false
        })
    }

    render() {
        const {kpi} = this.props;
        const pk:string = kpi.getSelectKpi
        return (
            <>
                <PageHeader style={{backgroundColor: 'white'}}
                            title="Select Kpi Master"
                            subTitle="key perfomance indicator select "/>
                <br/>
                <Card style={{padding: 35}} loading={this.state.loading}>
                    <h3>Content</h3>
                    <div style={{width: 500, margin: "auto"}}>
                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>KPIID</div>
                            <div>{kpi.getSelectKpi.kpiId}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Name</div>
                            <div>{kpi.getSelectKpi.kpiName}</div>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Main category</div>
                            <Select
                                defaultValue={"B"}
                                style={{width: 300, float: "right"}}
                            >
                                <Option value="A">EXT</Option>
                                <Option value="B">유지보수</Option>
                                <Option value="C">EXT</Option>
                            </Select>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Preference</div>
                            <Select
                                defaultValue={"B"}
                                style={{width: 300, float: "right"}}
                            >
                                <Option value="A">EXT</Option>
                                <Option value="B">상</Option>
                                <Option value="C">EXT</Option>
                            </Select>
                        </div>


                        <div style={{height: 150}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Description</div>
                            <div style={{
                                width: 300,
                                height: 150,
                                overflowX: "auto",
                                overflowY: "auto"
                            }}>{kpi.getSelectKpi.description}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>uiType</div>
                            <div>{kpi.getSelectKpi.uiType}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Scope</div>
                            <div>{kpi.getSelectKpi.scope}</div>
                        </div>

                        <div style={{height: 150}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Fomula</div>
                            <div style={{width: 300, overflowX: "auto"}}>{kpi.getSelectKpi.formula}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Unit of Measure</div>
                            <div>%</div>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Range</div>
                            <div>{this.props.kpi.getSelectKpi.range}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Trend</div>
                            <div>{this.props.kpi.getSelectKpi.trend}</div>
                        </div>
                    </div>
                </Card>
                <br/>
                <Card>
                    <h3>Context</h3>
                    <div style={{width: 500, margin: "auto"}}>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Timing</div>
                            <div>{kpi.getSelectKpi.timing}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Audience</div>
                            <div>{kpi.getSelectKpi.audience}</div>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Production methology</div>
                            <div>{kpi.getSelectKpi.prodMeth}</div>
                        </div>

                        <div style={{height: 150}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Notes</div>
                            <div style={{height: 150, overflowY: 'auto'}}>{kpi.getSelectKpi.notes}</div>
                        </div>
                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding: 5}}>Effect model diagram</div>
                            <div>{kpi.getSelectKpi.diagramFile}</div>
                        </div>

                    </div>
                    <div style={{width: 300, margin: "auto"}}>
                        <Link to={'/Information/MasterUpdate/' + kpi.getSelectKpi.kpiId} onClick={()=>{kpi.masterUpdate(pk)}}>
                            <Button style={{width: '45%', float: "right"}} type={'danger'}>수정</Button>
                        </Link>
                        <Button style={{width: '45%', float: "left"}} type={'primary'}onClick={() => history.go(-1)}>뒤로가기</Button>

                    </div>
                </Card>
            </>
        );
    };
}

export default SelectMaster;