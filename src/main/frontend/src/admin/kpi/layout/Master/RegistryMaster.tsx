import React, {Component} from 'react';
import {Button, Card, Input, PageHeader, Select} from "antd";
import {inject, observer} from "mobx-react";
import Searvice from "../../../../common/Searvice";

const { Option } = Select;


@inject('kpi')
@observer
class RegistryMaster extends Component<any, any>{

    componentDidMount(){
        this.props.kpi.resetMasterKpi;
    }

// Setter▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    private setKpiId = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setKpiId(e.target.value);
    };
    private setKpiName = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setKpiName(e.target.value);
    };
    private setUnit = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setUnit(e.target.value);
    };
    private setDesr = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setDesr(e.target.value);
    };
    private setScope = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setScope(e.target.value);
    };
    private setUiType = (value:string) => {
        this.props.kpi.setUiType(value);
    };
    private setFomula = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setFormula(e.target.value);
    };
    private setUnitOfM = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setUnitOfM(e.target.value);
    };
    private setRange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setRange(e.target.value);
    };
    private setTrend = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setTrend(e.target.value);
    };
    private setTiming = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setTiming(e.target.value);
    };
    private setAud = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setAud(e.target.value);
    };
    private setProdMeth = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setProdMeth(e.target.value);
    };
    private setNotes = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setNotes(e.target.value);
    };
    private setPrefer = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setPrefer(e.target.value);
    };
    private setDiagramFile = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setDiagramFile(e.target.value);
    };
// ▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒

    /**
     * @description KPI INSERT 하기
     */
     private insertMaster = async (event: React.MouseEvent<HTMLInputElement>) => {
        let insertData = this.props.kpi.getMasterParam;
        await Searvice.getPost("dash/insertKpiMaster.do", insertData);
    };
    render() {
        const {kpi} = this.props;
        return (
            <>
                <PageHeader style={{ backgroundColor: 'white' }}
                    title="KPI Master Update"     subTitle="key perfomance indicator update "/>
                <br />
                <Card style={{ padding: 60 }}>
                    <h3>Content</h3>
                    <div style={{ width: 600, margin: "auto" }}>
                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>kpiId</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setKpiId} value={kpi.getKpiId}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>kpiName</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setKpiName}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>unit</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setUnit}/>
                        </div>


                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>description</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setDesr}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>uiType</div>
                            <Select value={kpi.getUiType} style={{ width: 300, float: "right" }}  onChange={this.setUiType}>
                                <Option value="N-Line">텍스트</Option>
                                <Option value="Line">꺽은쇠 그래프</Option>
                                <Option value="Bar">막대 그래프</Option>
                                <Option value="Pie">파이 그래프</Option>
                            </Select>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>scope</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setScope}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>fomula</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setFomula}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>unit of measure</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setUnitOfM}/>
                        </div>


                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>range</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setRange}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>trend</div>
                            <Input style={{ width: 300, float: "right" }} onChange={this.setTrend}/>
                        </div>
                    </div>
                </Card>
                <br />
                <Card>
                    <h3>Context</h3>
                    <div style={{ width: 500, margin: "auto" }}>
                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>timing</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setTiming}/>
                        </div>
                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>audience</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setAud}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>prodMeth</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setProdMeth}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>notes</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setNotes}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>preference</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setPrefer}/>
                        </div>

                        <div style={{ height: 50 }}>
                            <div style={{ float: "left", width: 200, padding: 5 }}>Effect model diagram</div>
                            <Input style={{ width: 300, float: "right" }}  onChange={this.setDiagramFile}/>
                        </div>
                    </div>
                    <div style={{ width: 300, margin: "auto" }}>
                        <Button style={{ width: '45%', float: "left" }} type={'primary'} onClick={this.insertMaster}>등록</Button>
                        <Button style={{ width: '45%', float: "right" }} type={'danger'}>취소</Button>
                    </div>
                </Card>
            </>
        );
    };
}

export default RegistryMaster;