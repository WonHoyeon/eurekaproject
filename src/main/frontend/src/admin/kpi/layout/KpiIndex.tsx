import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { Avatar, Layout, Menu, Icon } from 'antd';
import {
    MenuFoldOutlined,
    MenuUnfoldOutlined,
    UploadOutlined,
    VideoCameraOutlined,
} from '@ant-design/icons';
import EmailIcon from '@material-ui/icons/Email';
import InfoIcon from '@material-ui/icons/Info';
import { inject, observer } from "mobx-react";
import Information from "./Information/Information";
import { BrowserRouter, Link, Route, Switch } from "react-router-dom";
import Master from "./Master/Master";
import Record from "./record/Record";
import RegistryRecord from "./record/RegistryRecord";
import RegistryMaster from "./Master/RegistryMaster";
import SelectMaster from "./Master/SelectMaster";
import { MenuItemList } from '../../../typescript/IndexType';
import UpdateMaster from "./Master/UpdateMaster";
import RegistrySelect from "./record/RegistrySelect";
import UpdateRecord from "./record/UpdateRecord";

const { SubMenu } = Menu;
const { Header, Content, Footer, Sider } = Layout;

@inject('IndexStore')
@observer
class KpiIndex extends Component<any, any> {

    /** @description  */
    state = {
        collapsed: false,
    };


    toggle = () => {
        this.setState({
            collapsed: !this.state.collapsed,
        });
    };


    private setTree(menuItem: MenuItemList[]): any {
        let jsxResult: any = menuItem.map((value: MenuItemList) => {
            // 제일 마지막 자식 (더이상 자식이 없음.)
            if (value.child.length === 0) {
                return <Menu.Item key={value.id} style={{ paddingLeft: 0, paddingRight: 0 }}>

                    <Link to={value.url}><Icon type={value.icon}/>{value.text}</Link>
                </Menu.Item>;
            } else {
                let textTag: any = value.text;
                if (value.icon != undefined) {
                    // 안전관제 관리 | 작업자 안전관제 | 분석 레보트
                    textTag = <span><Icon type={value.icon} /><span>{value.text}</span></span>
                }
                return <SubMenu key={value.id} title={textTag} style={{ paddingLeft: 0, paddingRight: 0 }}>
                    {this.setTree(value.child)}
                </SubMenu>;
            }
        });
        return jsxResult;
    }


    render() {
        const { IndexStore } = this.props;
        let jsxResult: any = this.setTree(IndexStore.getKpiLeftMenuList);
        return (
            <Layout style={{ height: '100vh' }}>
                    <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
                        <div style={{ textAlign: 'center', color: 'white', fontSize: 20, padding: 10 }}>
                            <img src={require('../../../store/logo.png')} width="60" height="40" />
                        </div>
                        <Menu
                            defaultSelectedKeys={['1']}
                            mode="inline"
                            theme="dark">
                            {/* 1 Depth 처리 */}
                            {jsxResult}
                        </Menu>
                    </Sider>
                    <Layout className="site-layout">
                        <Header className="site-layout-background">
                            {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                                className: 'trigger',
                                onClick: this.toggle,
                            })}
                        </Header>
                        <Content
                            style={{
                                margin: 'auto',
                                padding: 10,
                                // minHeight: 280,
                            }}
                        >
                            <Switch>
                                <Route path="/Information/" component={Information} exact={true}/>
                                <Route path="/Information/Record" component={Record} />
                                <Route path="/Information/RecordRegistry" component={RegistryRecord} />
                                <Route path="/Information/RecordSelect" component={RegistrySelect} />
                                <Route path="/Information/recordUpdate/:kpiid" component={UpdateRecord} />
                                <Route path="/Information/Master" component={Master} />
                                <Route path="/Information/MasterRegistry" component={RegistryMaster} />
                                <Route path="/Information/MasterUpdate/:kpiid" component={UpdateMaster} />
                                <Route path="/Information/MasterSelect/:id" component={SelectMaster} />
                            </Switch>
                        </Content>
                    </Layout>
            </Layout>
        );
    };
}

export default KpiIndex;
