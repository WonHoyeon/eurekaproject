import React, {Component} from 'react';
import {Button, Card, Input, PageHeader, Select} from "antd";
import autobind from "autobind-decorator";

const {Option} = Select;


class RegistryRecord extends Component<any, any> {

    render() {

        return (
            <>

                <PageHeader style={{backgroundColor: 'white'}}
                            title="KPI Registry"
                            subTitle="key perfomance indicator registry "/>

                <Card>
                    <div style={{width: 500, margin: "auto"}}>
                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>조직</div>
                            <Select
                                defaultValue={"B"}
                                style={{width: 300, float: "right"}}
                            >
                                <Option value="A">A공장 조립라인</Option>
                                <Option value="B">B공장 조립라인</Option>
                                <Option value="C">C공장 조립라인</Option>
                            </Select>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>KPI명</div>
                            <Select
                                defaultValue={"B"}
                                style={{width: 300, float: "right"}}
                            >
                                <Option value="A">감소 비율</Option>
                                <Option value="B">EXT</Option>
                                <Option value="C">EXT</Option>
                            </Select>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>KPI타입 ID</div>
                            <Input style={{width: 300, float: "right"}} placeholder={'APWT'}/>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>KPI타입 aud</div>
                            <Input style={{width: 300, float: "right"}} placeholder={'개인근무시간'}/>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>값</div>
                            <Input style={{width: 300, float: "right"}} placeholder={'10'}/>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Day</div>
                            <Input style={{width: 300, float: "right"}} placeholder={'10'}/>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Time</div>
                            <Input style={{width: 300, float: "right"}} placeholder={'10'}/>
                        </div>
                    </div>

                    <div style={{width: 300, margin: "auto"}}>
                        <Button style={{width:'45%', float:"left"}} type={'primary'}>등록</Button>
                        <Button style={{width:'45%', float:"right"}} type={'danger'}>취소</Button>
                    </div>
                </Card>
            </>
        );
    };


}

export default RegistryRecord;