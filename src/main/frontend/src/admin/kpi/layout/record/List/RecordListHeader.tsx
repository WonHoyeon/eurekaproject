import React, {Component} from 'react';
import {PageHeader, Select, Input, Button, DatePicker, Card, message} from 'antd';
import moment from "moment";
import {inject, observer} from "mobx-react";
import autobind from "autobind-decorator";
import {KpiRecordSearch} from "../../../typescript/KpiType";
import Searvice from "../../../../../common/Searvice";
import StringUtile from "../../../../../utile/StringUtile";

const {Option} = Select;

@inject('kpi')
@observer
class RecordListHeader extends Component<any, any> {


    @autobind
    public dateSelect(date: moment.Moment | null, selectDate: string) {
        this.props.kpi.setPickerDate(selectDate);
    }


    /**
     * @description 국가 검색
     * @param value
     */
    @autobind
    private async hirerachy1(value: string) {
        const {kpi} = this.props;
        let withHirerachy = value.split('|');
        kpi.setHirerachyName(withHirerachy[1]);
        kpi.setRecordFilter(withHirerachy[0]);
        kpi.setRelatedId(value);
        let param = {
            id: withHirerachy[0]
        }
        const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
        kpi.setKpiSearch2(resultData.data.kpiSearch);
        kpi.setSearchFactory(param);
        kpi.setRelatedId2('all');
        kpi.setRelatedId3('all');
        kpi.setRelatedId4('all');
        kpi.setRelatedId5('all');
        kpi.setRelatedId6('all');
        kpi.setKpiSearch3([]);
        kpi.setKpiSearch4([]);
        kpi.setKpiSearch5([]);
        kpi.setKpiSearch6([]);

    }

    @autobind
    private async hirerachy2(value: string) {
        const {kpi} = this.props;

        let withHirerachy = value.split('|');

        kpi.setHirerachyName(withHirerachy[1]);
        kpi.setRecordFilter(withHirerachy[0]);
        kpi.setRelatedId2(value);

        let param = {
            id: withHirerachy[0]
        }
        const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
        kpi.setKpiSearch3(resultData.data.kpiSearch);
        kpi.setSearchFactory(param);
        kpi.setRelatedId3('all');
        kpi.setRelatedId4('all');
        kpi.setRelatedId5('all');
        kpi.setRelatedId6('all');
        kpi.setKpiSearch4([]);
        kpi.setKpiSearch5([]);
        kpi.setKpiSearch6([]);
    }

    @autobind
    private async hirerachy3(value: string) {
        const {kpi} = this.props;
        let withHirerachy = value.split('|');

        kpi.setHirerachyName(withHirerachy[1]);
        kpi.setRecordFilter(withHirerachy[0]);
        kpi.setRelatedId3(value);

        let param = {
            id: withHirerachy[0]
        }

        const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
        kpi.setKpiSearch4(resultData.data.kpiSearch);
        kpi.setSearchFactory(param);
        kpi.setRelatedId4('all');
        kpi.setRelatedId5('all');
        kpi.setRelatedId6('all');
        kpi.setKpiSearch5([]);
        kpi.setKpiSearch6([]);
    }

    @autobind
    private async hirerachy4(value: string) {
        const {kpi} = this.props;
        let withHirerachy = value.split('|');

        kpi.setHirerachyName(withHirerachy[1]);
        kpi.setRecordFilter(withHirerachy[0]);
        kpi.setRelatedId4(value);

        let param = {
            id: withHirerachy[0]
        }

        const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
        kpi.setKpiSearch5(resultData.data.kpiSearch);
        kpi.setSearchFactory(param);
        kpi.setRelatedId5('all');
        kpi.setKpiSearch6([]);
    }

    @autobind
    private async hirerachy5(value: string) {
        const {kpi} = this.props;
        let withHirerachy = value.split('|');

        kpi.setHirerachyName(withHirerachy[1]);
        kpi.setRecordFilter(withHirerachy[0]);
        kpi.setRelatedId5(value);

        let param = {
            id: withHirerachy[0]
        }

        const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
        kpi.setKpiSearch6(resultData.data.kpiSearch);
        kpi.setSearchFactory(param);
    }

    @autobind
    private async hirerachy6(value: string) {
        const {kpi} = this.props;
        let withHirerachy = value.split('|');

        kpi.setHirerachyName(withHirerachy[1]);
        kpi.setRecordFilter(withHirerachy[0]);
        kpi.setRelatedId6(value);

        let param = {
            id: withHirerachy[0]
        }

        const resultData: any = await Searvice.getPost("dash/getSearch.do", param);
        kpi.setKpiSearch7(resultData.data.kpiSearch);
        kpi.setSearchFactory(param);
    }

    @autobind
    private async recordSearch(){
        const {kpi} = this.props;
        let parameter = {
            day: kpi.getPickerDate
        }
        if(kpi.getRelatedId3 == 'all'){
            message.warning('세부 공장을 선택해주세요');
        } else {
            const responseData: any = await Searvice.getPost("dash/getKpiRecord.do", parameter);

            const resultData = responseData.data.kpiRecord.filter(function(store:any){
                return store.relatedId == kpi.getSearchFactory.id
            })


            kpi.setKpiRecord(resultData);

        }
    }

    @autobind
    public reset(){
        const {kpi} = this.props;
        kpi.setRelatedId('all');
        kpi.setRelatedId2('all');
        kpi.setRelatedId3('all');
        kpi.setRelatedId4('all');
        kpi.setRelatedId5('all');
        kpi.setRelatedId6('all');


        kpi.setKpiSearch2([]);
        kpi.setKpiSearch3([]);
        kpi.setKpiSearch4([]);
        kpi.setKpiSearch5([]);
        kpi.setKpiSearch6([]);
        kpi.setPickerDate(StringUtile.formatDate());
    }

    render() {
        const {kpi} = this.props;
        const dateFormat = 'YYYY-MM-DD';
        return (
            <>
                <PageHeader style={{backgroundColor: 'white'}}
                            title="KPI Record List"
                            subTitle="key perfomance indicator registry ">
                    <div style={{width: '100%', float: 'left', marginBottom:5}}>
                        <span style={{float:'left', margin:5}}>검색 일자 : </span>  <DatePicker style={{width: '30%'}} value={moment(kpi.getPickerDate)}
                                    format={dateFormat}
                                    onChange={this.dateSelect}/>
                                    <Button style={{float:"right", width:'20%'}} type={'primary'} onClick={this.recordSearch}>조회</Button>
                    </div>
                    <div>
                        <Button style={{float:"right", width:'20%'}} type={'danger'} onClick={this.reset}>초기화</Button>
                        <span style={{float:'left', margin:5}}>공장 선택 : </span>
                        <Select
                            value={kpi.getRelatedId}
                            style={{float: 'left', width: 100}}
                            onChange={this.hirerachy1}
                        >
                            {kpi.getKpiSearch.map((store: KpiRecordSearch) => {
                                return <Option value={store.id + '|' + store.hirerachyName}>{store.name}</Option>
                            })
                            }
                            <Option value={'all'}>선택</Option>
                        </Select>

                        {(this.props.kpi.getKpiSearch2.length !== 0) ?
                            <Select value={kpi.getRelatedId2} style={{float: 'left', width: 100}}
                                    onChange={this.hirerachy2}
                            >
                                {kpi.getKpiSearch2.map((store: KpiRecordSearch) => {
                                    return <Option value={store.id + '|' + store.hirerachyName}>{store.name}</Option>
                                })
                                }
                                <Option value={'all'}>선택</Option>
                            </Select>
                            :
                            null
                        }


                        {(this.props.kpi.getKpiSearch3.length !== 0) ?
                            <Select value={kpi.getRelatedId3} style={{float: 'left', width: 100}}
                                    onChange={this.hirerachy3}
                            >
                                {kpi.getKpiSearch3.map((store: KpiRecordSearch) => {
                                    return <Option value={store.id + '|' + store.hirerachyName}>{store.name}</Option>
                                })
                                }
                                <Option value={'all'}>선택</Option>
                            </Select>
                            :
                            null
                        }

                        {(this.props.kpi.getKpiSearch4.length !== 0) ?
                            <Select value={kpi.getRelatedId4} style={{float: 'left', width: 100}}
                                    onChange={this.hirerachy4}
                            >
                                {kpi.getKpiSearch4.map((store: KpiRecordSearch) => {
                                    return <Option value={store.id + '|' + store.hirerachyName}>{store.name}</Option>
                                })
                                }
                                <Option value={'all'}>선택</Option>
                            </Select>
                            :
                            null
                        }
                        {(this.props.kpi.getKpiSearch5.length !== 0) ?
                            <Select value={kpi.getRelatedId5} style={{float: 'left', width: 100}}
                                    onChange={this.hirerachy5}
                            >
                                {kpi.getKpiSearch5.map((store: KpiRecordSearch) => {
                                    return <Option value={store.id + '|' + store.hirerachyName}>{store.name}</Option>
                                })
                                }
                                <Option value={'all'}>선택</Option>
                            </Select>
                            :
                            null
                        }
                        {(this.props.kpi.getKpiSearch6.length !== 0) ?
                            <Select value={kpi.getRelatedId6} style={{float: 'left', width: 100}}
                                    onChange={this.hirerachy6}
                            >
                                {kpi.getKpiSearch6.map((store: KpiRecordSearch) => {
                                    return <Option value={store.id + '|' + store.hirerachyName}>{store.name}</Option>
                                })
                                }
                                <Option value={'all'}>선택</Option>
                            </Select>
                            :
                            null
                        }
                    </div>

                </PageHeader>

            </>
        );
    };
}

export default RecordListHeader;