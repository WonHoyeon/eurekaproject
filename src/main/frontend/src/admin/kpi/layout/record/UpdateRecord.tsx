
import React, {Component} from 'react';
import {Button, Card, Input, PageHeader, Select} from "antd";
import autobind from "autobind-decorator";
import {inject, observer} from "mobx-react";
import Searvice from "../../../../common/Searvice";

const {Option} = Select;

@inject('kpi')
@observer
class UpdateRecord extends Component<any, any> {


    /**
     * @description KPI INSERT 하기
     */
    private updateRecord = async (event: React.MouseEvent<HTMLInputElement>) => {
        let update = this.props.kpi.getRecordParame;
        await Searvice.getPost("dash/updateKpiRecord.do", update);
    };

    // Setter▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒
    private setRelatedId = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setReRelatedId(e.target.value);
    };
    private setRecordKpiId = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setRecordKpiId(e.target.value);
    };
    private setProduct = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setProduct(e.target.value);
    };
    private setRecordValue = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setRecordValue(e.target.value);
    };
    private setRecordUnit = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setRecordUnit(e.target.value);
    };
    private setRecordDay = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.props.kpi.setRecordDay(e.target.value);
    };
    render() {
        const {kpi} = this.props;

        return (
            <>

                <PageHeader style={{backgroundColor: 'white'}}
                            title="KPI Record Update"
                            subTitle="key perfomance indicator update "/>

                <Card>
                    <div style={{width: 500, margin: "auto"}}>
                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>name</div>
                            <Input style={{width: 300, float: "right"}} onChange={this.setRelatedId} value={kpi.getReRelatedId}/>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>KPI ID</div>
                            <Input style={{width: 300, float: "right"}} onChange={this.setRecordKpiId} value={kpi.getRecordKpiId}/>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Product</div>
                            <Input style={{width: 300, float: "right"}} onChange={this.setProduct} value={kpi.getProduct} readOnly={true}/>
                        </div>


                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>값</div>
                            <Input style={{width: 300, float: "right"}} onChange={this.setRecordValue} value={kpi.getRecordValue}/>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Unit</div>
                            <Input style={{width: 300, float: "right"}} onChange={this.setRecordUnit} value={kpi.getRecordUnit}/>
                        </div>

                        <div style={{height: 50}}>
                            <div style={{float: "left", width: 200, padding:5}}>Day</div>
                            <Input style={{width: 300, float: "right"}} onChange={this.setRecordDay} value={kpi.getRecordDay}/>
                        </div>

                    </div>

                    <div style={{width: 300, margin: "auto"}}>
                        <Button style={{width:'45%', float:"left"}} type={'primary'} onClick={this.updateRecord}>수정</Button>
                        <Button style={{width:'45%', float:"right"}} type={'danger'}onClick={() => history.go(-1)}>취소</Button>
                    </div>
                </Card>
            </>
        );
    };


}

export default UpdateRecord;