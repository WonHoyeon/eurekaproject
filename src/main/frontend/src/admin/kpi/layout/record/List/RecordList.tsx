import React, {Component} from 'react';
import RecordListHeader from './RecordListHeader';
import {Button, Card, Select, Table} from 'antd';
import {Link} from 'react-router-dom';
import Searvice from "../../../../../common/Searvice";
import {inject, observer} from "mobx-react";
import {KpiRecordData, KpiRecordSearch} from "../../../typescript/KpiType";
import autobind from "autobind-decorator";
import moment from "moment";

const {Option} = Select;

@inject('kpi')
@observer
class RecordList extends Component<any, any> {

    constructor(props: any) {
        super(props);
        this.state = {
            selectedRowKeys: [], //테이블 체크박스 key 정보를 담는 state
            selectedRows: [], //테이블 체크박스 row 정보를 담는 state
        };
    }

    async componentDidMount() {
        const {kpi} = this.props;
        let parame2 = {
            id: ''
        }
        const responseData2: any = await Searvice.getPost("dash/getSearch.do", parame2);
        kpi.setKpiSearch(responseData2.data.kpiSearch)
    }



    onSelectChange = (selectedRowKeys: any, selectedRows: any) => {
        this.setState({selectedRowKeys, selectedRows});
    };

    @autobind
    public dateSelect(date: moment.Moment | null, selectDate: string) {
        this.props.kpi.setPickerDate(selectDate);
    }

    render() {
        const {kpi} = this.props;
        const {selectedRowKeys} = this.state;
        const rowSelection = {
            selectedRowKeys,
            onChange: this.onSelectChange,
        };
        const columns = [
            {
                title: 'KPI ID',
                dataIndex: 'kpiId',
            },
            {
                title: 'RELATED ID',
                dataIndex: 'relatedId',
            },
            {
                title: 'NAME',
                dataIndex: 'name',
            },
            {
                title: 'Product',
                dataIndex: 'text',
                render: function (text: string, record: KpiRecordData) {
                    const item: KpiRecordData = record;
                    return <Link to={"/Information/RecordSelect/" + item.relatedId + item.text}>{text}</Link>
                },
                onCell: (recode: KpiRecordData, rowIndex: number) => {
                    const data: KpiRecordData = recode;
                    return {
                        onClick: () => {
                            this.props.kpi.setSelectRecord(data);
                        }
                    }
                }
            },
            {
                title: 'VALUE',
                dataIndex: 'value',
            },
            {
                title: 'UNIT',
                dataIndex: 'unit',
                width:'10%'
            },
            {
                title: 'DAY',
                dataIndex: 'day',
            },
            {
                title: 'TIME',
                dataIndex: 'time',
            },
        ];

        //pickerdate 데이터 형식 설정 (ex- 'YYYY/MM/DD')
        const dateFormat = 'YYYY-MM-DD';

        return (
            <>
                <RecordListHeader/>
                <br/>
                <Card>

                    <div>

                        <div style={{width: '20%', margin: 'auto', float: 'right', textAlign: 'center'}}>
                            <Link to={'/Information/RecordRegistry'}>
                                <Button style={{width: '48%', float: 'left'}} type={'primary'}
                                        size={'small'}>추가</Button>
                            </Link>
                            <Button style={{width: '48%'}} type={'danger'} size={'small'}>삭제</Button>
                        </div>
                    </div>
                    <br/>
                    {(kpi.getHirerachyName !== '' ?
                        <div style={{float: "left", width: 800}}>{kpi.getHirerachyName}</div> : null)}
                    <Table
                        style={{width: '100%'}}
                        columns={columns}
                        dataSource={kpi.getKpiRecord}
                        rowKey={((record: any, index: any) => index)}
                        rowSelection={rowSelection}
                        bordered
                    />
                </Card>

            </>
        );
    };
}

export default RecordList;