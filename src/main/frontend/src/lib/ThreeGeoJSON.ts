import {
    LineBasicMaterial,
    Vector3,
    ShaderMaterial,
    Points,
    Line,
    Scene,
    Geometry,
    Group,
    Math as TMath,
} from 'three';

export enum GEOM_TYPE{
    POINT           = 'Point',
    MULTIPOINT      = 'MultiPoint',
    LINESTRING      = 'LineString',
    POLYGON         = 'Polygon',
    MULTILINESTRING = 'MultiLineString',
    MULTIPOLYGON    = 'MultiPolygon'
}

export enum SHAPE_TYPE{
    SPHERE  = 'sphere',
    PLANE   = 'plane',
}

export enum JSON_TYPE{
    FEATURE  = 'Feature',
    FEATURE_COLLECTION   = 'FeatureCollection',
    GEOMETRY_COLLECTION   = 'GeometryCollection',
}

/**
 * @description Eberhard Graether / http://egraether.com/
 */
export default class ThreeGeoJSON{


    private _geoGroup = new Group(); 

    private _container:Scene;
    private _x_values:Array<number> = [];
    private _y_values:Array<number> = [];
    private _z_values:Array<number> = [];

    constructor(container:Scene){
        this._container = container;
        this.drawThreeGeo.bind(this);
    }


    private _createGeometryArray(json:{type:JSON_TYPE , geometry:any , features:any , geometries:any}):Array<any>{
        let geometry_array:Array<any> = [];
        let type:string = json.type;
        if (type === JSON_TYPE.FEATURE ) {
            geometry_array.push(json.geometry);
        } else if (type === JSON_TYPE.FEATURE_COLLECTION ) {
            for (let feature_num:number = 0; feature_num < json.features.length; feature_num++) {
                geometry_array.push(json.features[feature_num].geometry);
            }
        } else if (type === JSON_TYPE.GEOMETRY_COLLECTION ) {
            for (let geom_num:number = 0; geom_num < json.geometries.length; geom_num++) {
                geometry_array.push(json.geometries[geom_num]);
            }
        } else {
            throw new Error('The geoJSON is not valid.');
        }
        //alert(geometry_array.length);
        return geometry_array;
    }

    /**
     * 
     * @param json JSON Object
     * @param radius 
     * @param shape 
     * @param materalOptions 
     */
    public drawThreeGeo(json:any, radius:number, shape:SHAPE_TYPE, materalOptions?:any):void {
    
        this._x_values = [];
        this._y_values = [];
        this._z_values = [];
    
        let json_geom:Array<any> = this._createGeometryArray(json);
        //An array to hold the feature geometries.
        let convertCoordinates:Function = this._getConversionFunctionName(shape);
        //Whether you want to convert to spherical or planar coordinates.
        let coordinate_array:Array<Array<number>> = [];
        //Re-usable array to hold coordinate values. This is necessary so that you can add
        //interpolated coordinates. Otherwise, lines go through the sphere instead of wrapping around.
        for (let geom_num:number = 0; geom_num < json_geom.length; geom_num++) {
            const type:string = json_geom[geom_num].type;
    
            if (type === GEOM_TYPE.POINT ) {
                convertCoordinates(json_geom[geom_num].coordinates, radius);
                this._drawParticle(this._x_values[0], this._y_values[0], this._z_values[0], materalOptions);
    
            } else if (type === GEOM_TYPE.MULTIPOINT ) {
                for (let point_num:number = 0; point_num < json_geom[geom_num].coordinates.length; point_num++) {
                    convertCoordinates(json_geom[geom_num].coordinates[point_num], radius);
                    this._drawParticle(this._x_values[0], this._y_values[0], this._z_values[0], materalOptions);
                }
    
            } else if (type === GEOM_TYPE.LINESTRING ) {
                coordinate_array = this._createCoordinateArray(json_geom[geom_num].coordinates);
    
                for (let point_num:number = 0; point_num < coordinate_array.length; point_num++) {
                    convertCoordinates(coordinate_array[point_num], radius);
                }
                this._drawLine(this._x_values, this._y_values, this._z_values, materalOptions);
    
            } else if (type === GEOM_TYPE.POLYGON ) {
                for (let segment_num:number = 0; segment_num < json_geom[geom_num].coordinates.length; segment_num++) {
                    coordinate_array = this._createCoordinateArray(json_geom[geom_num].coordinates[segment_num]);
    
                    for (let point_num:number = 0; point_num < coordinate_array.length; point_num++) {
                        convertCoordinates(coordinate_array[point_num], radius);
                    }
                    this._drawLine(this._x_values, this._y_values, this._z_values, materalOptions);
                }
    
            } else if (type === GEOM_TYPE.MULTILINESTRING ) {
                for (let segment_num:number = 0; segment_num < json_geom[geom_num].coordinates.length; segment_num++) {
                    coordinate_array = this._createCoordinateArray(json_geom[geom_num].coordinates[segment_num]);
    
                    for (let point_num:number = 0; point_num < coordinate_array.length; point_num++) {
                        convertCoordinates(coordinate_array[point_num], radius);
                    }
                    this._drawLine(this._x_values, this._y_values, this._z_values, materalOptions);
                }
    
            } else if (type === GEOM_TYPE.MULTIPOLYGON ) {
                for (let polygon_num:number = 0; polygon_num < json_geom[geom_num].coordinates.length; polygon_num++) {
                    for (let segment_num:number = 0; segment_num < json_geom[geom_num].coordinates[polygon_num].length; segment_num++) {
                        coordinate_array = this._createCoordinateArray(json_geom[geom_num].coordinates[polygon_num][segment_num]);
    
                        for (let point_num:number = 0; point_num < coordinate_array.length; point_num++) {
                            convertCoordinates(coordinate_array[point_num], radius);
                        }
                        this._drawLine(this._x_values, this._y_values, this._z_values, materalOptions);
                    }
                }
            } else {
                throw new Error('The geoJSON is not valid.');
            }
        }

        this._geoGroup.rotateX(TMath.degToRad(-90));
        this._container.add(this._geoGroup);
    }


    private _getConversionFunctionName(shape:SHAPE_TYPE):Function{
        let conversionFunctionName:Function;

        if (shape === SHAPE_TYPE.SPHERE) {
            conversionFunctionName = this._convertToSphereCoords.bind(this);
        } else if (shape === SHAPE_TYPE.PLANE ) {
            conversionFunctionName = this._convertToPlaneCoords.bind(this);
        } else {
            throw new Error('The shape that you specified is not valid.');
        }
        return conversionFunctionName;
    }

    private _createCoordinateArray(feature:Array<Array<number>>):Array<Array<number>> {
        //Loop through the coordinates and figure out if the points need interpolation.
        let temp_array:Array<Array<number>> = [];
        let interpolation_array:Array<Array<number>> = [];

        for (let point_num:number = 0; point_num < feature.length; point_num++) {
            let point1:Array<number> = feature[point_num];
            let point2:Array<number> = feature[point_num - 1];

            if (point_num > 0) {
                if (this._needsInterpolation(point2, point1)) {
                    interpolation_array = [point2, point1];
                    interpolation_array = this._interpolatePoints(interpolation_array);

                    for (let inter_point_num:number = 0; inter_point_num < interpolation_array.length; inter_point_num++) {
                        temp_array.push(interpolation_array[inter_point_num]);
                    }
                } else {
                    temp_array.push(point1);
                }
            } else {
                temp_array.push(point1);
            }
        }
        return temp_array;
    }

    private _needsInterpolation(point2:Array<number>, point1:Array<number>):Boolean {
        //If the distance between two latitude and longitude values is
        //greater than five degrees, return true.
        let lon1:number = point1[0];
        let lat1:number = point1[1];
        let lon2:number = point2[0];
        let lat2:number = point2[1];
        let lon_distance:number = Math.abs(lon1 - lon2);
        let lat_distance:number = Math.abs(lat1 - lat2);

        if (lon_distance > 5 || lat_distance > 5) {
            return true;
        } else {
            return false;
        }
    }

    private _interpolatePoints(interpolation_array:Array<Array<number>>) {
        //This function is recursive. It will continue to add midpoints to the
        //interpolation array until needsInterpolation() returns false.
        let temp_array:Array<Array<number>> = [];
        let point1:Array<number>=[];
        let point2:Array<number>=[];

        for (let point_num = 0; point_num < interpolation_array.length - 1; point_num++) {
            point1 = interpolation_array[point_num];
            point2 = interpolation_array[point_num + 1];

            if (this._needsInterpolation(point2, point1)) {
                temp_array.push(point1);
                temp_array.push(this._getMidpoint(point1, point2));
            } else {
                temp_array.push(point1);
            }
        }

        temp_array.push(interpolation_array[interpolation_array.length - 1]);

        if (temp_array.length > interpolation_array.length) {
            temp_array = this._interpolatePoints(temp_array);
        } else {
            return temp_array;
        }
        return temp_array;
    }

    private _getMidpoint(point1:Array<number>, point2:Array<number> ):number[]{
        let midpoint_lon:number = (point1[0] + point2[0]) / 2;
        let midpoint_lat:number = (point1[1] + point2[1]) / 2;
        var midpoint:number[] = [midpoint_lon, midpoint_lat];
        return midpoint;
    }

    private _convertToSphereCoords(coordinates_array:Array<number>, sphere_radius:number):void{
        let lon:number = coordinates_array[0];
        let lat:number = coordinates_array[1];

        this._x_values.push(Math.cos(lat * Math.PI / 180) * Math.cos(lon * Math.PI / 180) * sphere_radius);
        this._y_values.push(Math.cos(lat * Math.PI / 180) * Math.sin(lon * Math.PI / 180) * sphere_radius);
        this._z_values.push(Math.sin(lat * Math.PI / 180) * sphere_radius);
    }

    private _convertToPlaneCoords(coordinates_array:Array<number>, radius:number):void{
        let lon:number = coordinates_array[0];
        let lat:number = coordinates_array[1];

        this._z_values.push((lat / 180) * radius);
        this._y_values.push((lon / 180) * radius);
    }

    private _drawParticle(x:number, y:number, z:number, options:any):void{
        let particle_geom:Geometry = new Geometry();
        particle_geom.vertices.push(new Vector3(x, y, z));

        /* ParticleSystemMaterial -> ShaderMaterial */
        let particle_material:ShaderMaterial = new ShaderMaterial(options);
        /* ParticleSystem -> Points */
        let particle:Points = new Points(particle_geom, particle_material);
        // this._container.add(particle);

        this._geoGroup.add(particle);
        
        this._clearArrays();
    }

    /**
     * 라인 그리는 함수
     * @param x_values 
     * @param y_values 
     * @param z_values 
     * @param options 
     */
    private _drawLine(x_values:Array<number>, y_values:Array<number>, z_values:Array<number>, options:any):void {
        let line_geom:Geometry = new Geometry();
        this._createVertexForEachPoint(line_geom, x_values, y_values, z_values);

        let line_material:LineBasicMaterial = new LineBasicMaterial(options);
        let line:Line = new Line(line_geom, line_material);
        this._geoGroup.add(line);

        this._clearArrays();
    }


    private _createVertexForEachPoint(object_geometry:Geometry, values_axis1:Array<number>, values_axis2:Array<number>, values_axis3:Array<number>) {
        for (let i = 0; i < values_axis1.length; i++) {
            object_geometry.vertices.push( new Vector3(values_axis1[i], values_axis2[i], values_axis3[i]) );
        }
    }

    private _clearArrays(){
        this._x_values = [];
        this._y_values= [];
        this._z_values= [];
    }

}